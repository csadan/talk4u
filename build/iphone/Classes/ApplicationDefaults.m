/**
 * Appcelerator Titanium Mobile
 * Copyright (c) 2009-2013 by Appcelerator, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 *
 * WARNING: This is generated code. Do not modify. Your changes *will* be lost.
 */

#import <Foundation/Foundation.h>
#import "TiUtils.h"
#import "ApplicationDefaults.h"

@implementation ApplicationDefaults

+ (NSMutableDictionary*) copyDefaults
{
	NSMutableDictionary * _property = [[NSMutableDictionary alloc] init];
	
	[_property setObject:[TiUtils stringValue:@"LdxwCDtYVdRJBUbCzIIrxBXLwHCIO23j"] forKey:@"acs-oauth-secret-production"];
	[_property setObject:[TiUtils stringValue:@"RTI4Mr8cwrA50drNhZi2Tenn948tIdWK"] forKey:@"acs-oauth-key-production"];
	[_property setObject:[TiUtils stringValue:@"MsJxzD8mGssQNlESUNTGsg7K7pAxoaVw"] forKey:@"acs-api-key-production"];
	[_property setObject:[TiUtils stringValue:@"Mmw0dfQPjSRx5A2YlrgvBcB7aDHHqNlq"] forKey:@"acs-oauth-secret-development"];
	[_property setObject:[TiUtils stringValue:@"abKl2QXByRJ1SfQaB1FCASTdXYdqshb3"] forKey:@"acs-oauth-key-development"];
	[_property setObject:[TiUtils stringValue:@"HqGw1Le9fghBh0lDmNxWlRXUfcm4sy2D"] forKey:@"acs-api-key-development"];
	[_property setObject:[TiUtils stringValue:@"system"] forKey:@"ti.ui.defaultunit"];
	return _property;
}

@end