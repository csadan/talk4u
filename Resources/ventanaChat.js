
var ventanaChat = function(conn,xmpp) {


    // ----------------------------------
    // SEND MESSAGE
    // ----------------------------------
    function send_a_message(message) {
			conn.send(xmpp.message({
						to :"pedro@darwin/Gajim",
						from : "carlos@darwin",
						type : "chat"
			}).c("body").t(message));
			append_chat_message(message,"blue");
    }

	fn_open= function() {
		//Añado las funciones que van a responder a los eventos
   			conn.addHandler(onMessage, null, 'message', null, null, null);
   			
	};

	function onMessage(message) {	
		   append_chat_message(message.getChild("body").getText());
	}

    // ----------------------------------
    // CREATE BASE UI TAB AND ROOT WINDOW
    // ----------------------------------
    var chat_window = Ti.UI.createWindow();
    chat_window.addEventListener("open",fn_open);
    var textfield   = Ti.UI.createTextField({
        width       : '75%',
        height      : '50dp',
        left        : 4,
        top         : 4,
        color       : "#111",
        value       : "",
        border      : 1,
        borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        borderRadius : 4,
        font        : {
            fontSize   : '18dp',
            fontWeight : 'bold'
        }
    });

	var windowView = Ti.UI.createView({
		layout: "vertical"
	});

	//heder view
	var headerView = Ti.UI.createView({
		height: 100
	});
	headerView.add(textfield);

    // Text Chat History
    var table = Ti.UI.createTableView({

	    backgroundColor:'white'
    });


    // Send Button
    var button = Ti.UI.createButton({
        title         : 'Send',
        top           : 4,
        right         : 4,
        width         : '60dp',
        height        : '50dp',
        borderRadius  : 6,
        shadowColor   : "#001",
        shadowOffset  : { x : 1, y : 1 },
        style         : Ti.UI.iPhone.SystemButtonStyle.PLAIN,
        font          : {
            fontSize   : '18dp',
            fontWeight : 'bold'
        },
        backgroundGradient : {
            type          : 'linear',
            colors        : [ '#058cf5', '#015fe6' ],
            startPoint    : { x : 0, y : 0 },
            endPoint      : { x : 2, y : 50 },
            backFillStart : false
        }
    });
	headerView.add(button);

    // Append New Chat Message
    function append_chat_message( message, color ) {
        var row = Ti.UI.createTableViewRow({
            className          : "pubnub_chat",
            backgroundGradient : {
                type          : 'linear',
                colors        : [ "#fff", '#eeeeed' ],
                startPoint    : { x : 0, y : 0 },
                endPoint      : { x : 0, y : 70 },
                backFillStart : false
            }
        });

        var label = Ti.UI.createLabel({
            text   : message || "no-message",
            height : '50dp',
            width  : 'auto',
            color  : color || "#111",
            left   : 10,
            font   : { 
            	fontSize :'19dp',
            	fontWeight:'bold'
            }
        });

        row.add(label);
        table.insertRowBefore( 0, row );
    }

    // Listen for Send Button Touch
    button.addEventListener( 'touchstart', function(e) {
        send_a_message(textfield.value);
        textfield.value = "";
        textfield.focus();
    });

    // Listen for Return Key Press
    textfield.addEventListener( 'return', function(e) {
        send_a_message(textfield.value);
        textfield.value = "";
        textfield.focus();
    });

    // Listen for Return Key Press
    chat_window.addEventListener( 'open', function(e) {
        textfield.focus();
    });
    
    windowView.add(headerView);
    windowView.add(table);

	chat_window.add(windowView);
	
    append_chat_message(" ");
    append_chat_message(" ");
    append_chat_message(" ");
    append_chat_message("Connecting...");

    return chat_window;
};
module.exports = ventanaChat;
