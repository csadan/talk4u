/*
 * A tabbed application, consisting of multiple stacks of windows associated with tabs in a tab group.  
 * A starting point for tab-based application with multiple top-level windows. 
 * Requires Titanium Mobile SDK 1.8.0+.
 * 
 * In app.js, we generally take care of a few things:
 * - Bootstrap the application with any data we need
 * - Check for dependencies like device type, platform version or network connection
 * - Require and open our top-level UI component
 *  
 */

//bootstrap and check dependencies
if (Ti.version < 1.8 ) {
	alert('Sorry - this application template requires Titanium Mobile SDK 1.8 or later');
}

lib = {};
glb = {};
talk4u = {};
navigation = {};
xmpp = require("lib/xmppjs/xmpp");
_xmppservice= require("classes/service/talk4u/xmpp/xmppservice");
listaChats = [];
/*
 listaChats.['id'].messages[0]
 listaChats.['id'].participants[0]
 listaChats.['id'].nMessagesNotRead= 0
*/

// This is a single context application with mutliple windows in a stack
(function() {
	// namespace of the app
	glb = {
		'lang' : Ti.Platform.locale,
		'osname' : Ti.Platform.osname,
		'model' : Ti.Platform.model,
		'version' : Ti.Platform.version,
		'macaddress' : Ti.Platform.macaddress,
		'density' : Ti.Platform.displayCaps.density,
		'platformWidth' : Ti.Platform.displayCaps.platformWidth,
		'platformHeight' : Ti.Platform.displayCaps.platformHeight,
		'appversion' : '1.0',
		'debug' : true
	};
	
	if(glb.osname === 'ipad'){
		glb.device='ipad';
	}else if(glb.osname === 'iphone'){
		glb.device='iphone';
	}else if(glb.osname === 'android' && (width > 899 || height > 899)){
		glb.device='androidTablet';
	}else /*if(osname === 'android')*/{
		glb.device='android';
	}
	// include third-party modules
	//glb.Cloud = require('ti.cloud');
	//glb.tielcimagepicker = require('jp.kray.ti.ELCImagePicker');
	// glb.assetlibrary = require('de.marcelpociot.assetlibrary');
	Titanium.SMSView = require('ti.smsview');
	
	var NavigationController = require('navigation/NavigationController').NavigationController;
	navigation = new NavigationController();
	
	Ti.include('/lib/library.js'); // include some useful functions
	Ti.include('lib/moment.js');   //include useful function to format dates and moments
	//moment.lang(glb.lang);
	moment.lang('es');
	var appConfig = require('config'); // add config parameters to the namespace	
	glb.config = new appConfig();
	
	// Crea la instancia de aplicación y enlaza los componentes necesarios
	var _I4UAppMaker = require('classes/I4UAppMaker');
	var I4UAppMaker = new _I4UAppMaker();
	talk4u = I4UAppMaker.getApp('talk');
	//Ti.App.addEventListener("resume",talk4u.startApp);
	talk4u.startApp();
	
})();