// this file contains some configuration parameters
// for adapting the app to another company
function config() {
	// general

	this.appname = 'talk4u';
	// Data Base
	this.dbpath = '/ddbb/4ikim.db';
	this.dbname  = '4ikim';
	// server
	// this.xmppServerPath ='10.128.3.190'
	// this.xmppDomain = '4ikim'
	
	this.xmppServerPath ='10.128.3.190'
	this.xmppDomain = '4ikim'
	
	//this.xmppServerPath ='192.168.56.101'
	//this.xmppDomain = 'adminuser'
	
	this.xmppSource = 'talk4u'
	this.apiServerPath = 'http://securelog.dyndns-work.com/n4ikimserver/api/v1/rest/';
	//this.apiServerPath = 'https://api-4ikim.4ikim.com/api/v1/rest/';
	//this.apiServerPath = 'http://192.168.13.12/n4ikimserver/api/v1/rest/';
	//this.apiServerPath = 'http://46.17.140.251/api/v1/rest/';
	//this.apiServerPath = 'http://46.17.141.8/api/v1/rest/';
	
	var iberboxTheme = {
		color1: '#4a87b0',
		color2: '#336e95',
		toolsColor: 'white', //white or gray depending the file iphone/images/talk4u/icoToolbar/    icons-gray or icons-white
		buttonLoginColor:'blue',
		mainWindowBackgroundImage:'images/talk4u/fondo/rough_diagonal.png',
		windowBackgroundImage:'images/talk4u/fondo/rough_diagonal.png'
	}
	var talk4uTheme = {
		color1: '#303030', //'#40a800',
		color2: '#2B2B2B',//'#5eab1f',
		toolsColor: 'white', //white or gray depending the file iphone/images/talk4u/icoToolbar/    icons-gray or icons-white
		buttonLoginColor:'green',
	    mainWindowBackgroundImage:'images/talk4u/fondo/rough_diagonal.png',
		windowBackgroundImage:'images/talk4u/fondo/rough_diagonal.png'
	}
	this.theme= talk4uTheme;
	
	//feature tooggle
	this.userOrGroupProfileAccess = false;
	
	//mail for send comments form
	this.mail = 'info@4ikim.com';
	
	// xhr timeout
	this.xhrTimeout = 10000;
	this.xhrSecurityTimeout = 30000;
	this.xhrFilesTimeout = 1800000;
	// tableView dinamically scroll params
	this.limit = 29;
	this.sortType = 'ASC';
	// photo and list image params
	this.photoExtension = (glb.density === 'hight' && glb.osname === 'iphone')? '@2x.png' : '.png';
	this.thumbImageSize = (glb.density === 'hight')?80:80;
	this.photoSize = (glb.density === 'hight')?200:100;
	// Save ids of the context list elements
	this.contextListIds = [];
	//initialize folder navigation
	glb.contextsNavigation=[];
	// Save the ids of changed folders
	glb.changedItemIds = [];
	// Available types to show
	
	this.types=[
	//email,pdf, office, fotos, videos	
		{id: 38,  name: L('mensaje'), icoListas: 'images/talk4u/icoListas/email.png', preview: 'images/talk4u/preview/email.png', inHome: true, download: {type: 'file'} },
		{id: 40,  name: L('received'), icoListas: 'images/talk4u/icoListas/email.png', preview: 'images/talk4u/preview/email.png', inHome: false, download: {type: 'file'}},
		{id: 41,  name: L('sent'), icoListas: 'images/talk4u/icoListas/email.png', preview: 'images/talk4u/preview/email.png', inHome: false, download: {type: 'file'}},
		{id: 42,  name: L('receivedAttach'), icoListas: 'images/talk4u/icoListas/email.png', preview: 'images/talk4u/preview/email.png', inHome: false, download: {type: 'file'}},
		{id: 29,  name: L('pdf'), icoListas: 'images/talk4u/icoListas/pdf.png', preview: 'images/talk4u/preview/pdf.png', inHome: true, download: {type: 'file'}},
		{id: 30,  name: L('office'), icoListas: 'images/talk4u/icoListas/office.png', preview: 'images/talk4u/preview/office.png', inHome: true, download: {type: 'file', only:['dot']}},
		{id: 607, name: L('word'), icoListas: 'images/talk4u/icoListas/word.png', preview: 'images/talk4u/preview/word.png', inHome: false, download: {type: 'file'}},
		{id: 608, name: L('excel'), icoListas: 'images/talk4u/icoListas/excel.png', preview: 'images/talk4u/preview/excel.png', inHome: false, download: {type: 'file'}},		
		{id: 609, name: L('powerPoint'), icoListas: 'images/talk4u/icoListas/powerpoint.png', preview: 'images/talk4u/preview/powerpoint.png', inHome: false, download: {type: 'file'}},
		{id: 32,  name: L('openOffice'), icoListas: 'images/talk4u/icoListas/openoffice.png', preview: 'images/talk4u/preview/openoffice.png', inHome: false },
		{id: 718, name: L('writer'), icoListas: 'images/talk4u/icoListas/writer.png', preview: 'images/talk4u/preview/writer.png', inHome: false },
		{id: 719, name: L('calc'), icoListas: 'images/talk4u/icoListas/calc.png', preview: 'images/talk4u/preview/calc.png', inHome: false },
		{id: 720, name: L('impress'), icoListas: 'images/talk4u/icoListas/impress.png', preview: 'images/talk4u/preview/impress.png', inHome: false },
		{id: 27,  name: L('image'), icoListas: 'images/talk4u/icoListas/image.png', preview: 'images/talk4u/preview/image.png', inHome: true, download: {type: 'file'}},
		{id: 610, name: L('videoChat'), icoListas: 'images/talk4u/icoListas/video.png', preview: 'images/talk4u/preview/video.png', inHome: false, download: {type: 'file'}},
		{id: 2,   name: L('folder'), icoListas: 'images/talk4u/icoListas/carpeta.png', preview: 'images/talk4u/preview/carpeta.png', inHome: false },
		{id: 19,  name: L('fichero'), icoListas: 'images/talk4u/icoListas/fichero.png', preview: 'images/talk4u/preview/fichero.png', inHome: false, download: {type: 'file', only: ['txt','js','html','htm','css', 'xml']}},
		{id: 53,  name: L('video'), icoListas: 'images/talk4u/icoListas/video.png', preview: 'images/talk4u/preview/video.png', inHome: true, download: {type: 'file'}},
		{id: 51,  name: L('autocad'), icoListas: 'images/talk4u/icoListas/autocad.png', preview: 'images/talk4u/preview/autocad.png', inHome: false },
		{id: 52,  name: L('comprimido'), icoListas: 'images/talk4u/icoListas/comprimido.png', preview: 'images/talk4u/preview/comprimido.png', inHome: false },//comprimido
		//{id: 400, name: L('google_doc'), icoListas: 'images/talk4u/icoListas/gdrive.png', preview: 'images/talk4u/preview/gdrive.png', inHome: false },//
		//{id: 401, name: L('gdoc'), icoListas: 'images/talk4u/icoListas/gdoc.png', preview: 'images/talk4u/preview/gdoc.png', inHome: false },
		//{id: 402, name: L('gsheet'), icoListas: 'images/talk4u/icoListas/gsheet.png', preview: 'images/talk4u/preview/gsheet.png', inHome: false },
		//{id: 403, name: L('gpoint'), icoListas: 'images/talk4u/icoListas/gslides.png', preview: 'images/talk4u/preview/gslides.png', inHome: false },
		{id: 50,  name: L('website'), icoListas: 'images/talk4u/icoListas/website.png', preview: 'images/talk4u/preview/website.png', inHome: false, download: {type: 'link'}},
		{id: 3, name: L('note'), icoListas: 'images/talk4u/icoListas/nota.png', preview: 'images/talk4u/preview/nota.png', inHome: false, download: {type: 'file'}}		
	];
	this.talk4u_dir='talk4u';
	this.preview_dir='preview';
	this.list_dir='list';
	this.talk4u_path=Ti.Filesystem.applicationCacheDirectory+this.talk4u_dir;
	this.preview_path=this.talk4u_path+Ti.Filesystem.separator+this.preview_dir;
	this.list_path=this.talk4u_path+Ti.Filesystem.separator+this.list_dir;	
};
config.prototype.getApiServerPath=function(type, id){
	if(type){
		var subpath = {
			TalkChat : 'talk/chats/'+id,
			Properties : 'elements/'+id+'/properties',
			Elements : 'elements',
			TalkElements : 'talk/talkelements',
			TalkElementsActions : 'talk/talkelements/'+id,
			TalkParents : 'talk/talkelements/'+id+'/parents',
			TalkChildren : 'talk/talkelements/'+id+'/childrens',
			TalkUserPersonalizations : 'talk/userpersonalization',
			TalkUsers : 'talk/users',
			TalkUsersActions : 'talk/users/'+id,
			TalkGroups : 'talk/groups',
			TalkVersions : 'talk/talkelements/'+id+'/versions',
			TalkPermissions : 'talk/talkelements/'+id+'/permissions',
			TalkEffectivePermissions : 'talk/talkelements/'+id+'/effectivepermissions',
			TalkDiscovery : 'talk/discovery/types',
			TalkAccount : 'talk/account'
		}
		return this.apiServerPath + subpath[type];
	}else
		return this.apiServerPath;
}
config.prototype.getImageFromTypeId=function(typeId, imageType){
// returns the image url of the element id given in parameter
	for(var i=0;i<this.types.length;i++){
		if(this.types[i].id===typeId){
			return (imageType)?this.types[i][imageType]:this.types[i].icoListas;
		}
	}
	return (imageType)?'images/talk4u/'+imageType+'/fichero.png':'images/talk4u/icoListas/fichero.png';
}
module.exports = config;


