function I4UForm(){};
I4UForm.prototype.createWindow=function(params){
	var win = Ti.UI.createWindow({
		title: (params.title)? params.title : '',
		backgroundColor:(params.backgroundColor)? params.backgroundColor : '#fff',
		modal: (params.modal)? true : false,
		fullscreen: (params.fullscreen)? true : false,
		openStyle: (params.openStyle)? params.openStyle : null,
		formName: (params.formName)? params.formName : null,
		navBarHidden: (params.navBarHidden)? params.navBarHidden : undefined,
		url: (params.url)? params.url : undefined,
		tabBarHidden:(params.tabBarHidden)? params.tabBarHidden : undefined,
	});
	switch(glb.device){
		case 'iphone':
			win.backgroundImage = (params.backgroundImage)? params.backgroundImage : glb.config.theme.windowBackgroundImage;
			win.barColor = (glb.config.theme.color1)? glb.config.theme.color1 : null;
			win.barImage = (glb.config.theme.navBarImage)? glb.config.theme.navBarImage : null;

			if(win.openStyle === 'modal'){
				// Add the close button
				var closeButton = Titanium.UI.createButton({
					title:(params.backButtonTitle)?params.backButtonTitle:L('close'),
					style:Titanium.UI.iPhone.SystemButtonStyle.BAR
				});
				closeButton.addEventListener('click',function(){
					win.close();
				});
				if(params.leftNavButton){
					win.setLeftNavButton(params.leftNavButton);
					win.setRightNavButton(closeButton);
				}else{
					win.setLeftNavButton(closeButton);
					win.rightNavButton = (params.rightNavButton) ? params.rightNavButton : undefined;
				}
			}else {
				win.backButtonTitle = (params.backButtonTitle) ? params.backButtonTitle : undefined;
				win.rightNavButton = (params.rightNavButton) ? params.rightNavButton : undefined;
				win.leftNavButton = (params.leftNavButton) ? params.leftNavButton : undefined;
			}
		break;
		case 'ipad':
			win.barColor = (glb.config.theme.color1)?glb.config.theme.color1 : null;
			if(params.inDetail && win.openStyle === 'inTab')
				win.openStyle = 'inDetail';
			else if(win.openStyle === 'modal'){
				// Add the close button
				var closeButton = Titanium.UI.createButton({
					title:(params.backButtonTitle)?params.backButtonTitle:L('close'),
					style:Titanium.UI.iPhone.SystemButtonStyle.BAR
				});
				closeButton.addEventListener('click',function(){
					win.close();
				});
				if(params.leftNavButton){
					win.setLeftNavButton(params.leftNavButton);
					win.setRightNavButton(closeButton);
				}else{
					win.setLeftNavButton(closeButton);
					win.rightNavButton = (params.rightNavButton) ? params.rightNavButton : undefined;
				}
			}else {
				win.backButtonTitle = (params.backButtonTitle) ? params.backButtonTitle : undefined;
				win.rightNavButton = (params.rightNavButton) ? params.rightNavButton : undefined;
				win.leftNavButton = (params.leftNavButton) ? params.leftNavButton : undefined;
			}
			if (params.backgroundImageOrientation && params.backgroundImage) {
				// to set the correct backgroundImage
				var a_bgImageName = params.backgroundImage.split(".");
				function orientationChanged(e) {
					if (e.orientation === Ti.UI.PORTRAIT || e.orientation === Ti.UI.UPSIDE_PORTRAIT){
						win.backgroundImage = a_bgImageName[0] +'-Portrait.'+ a_bgImageName[1];
					} else {
						win.backgroundImage =  a_bgImageName[0] +'-Landscape.'+ a_bgImageName[1];
					}
				}
				var orientation = Ti.Gesture.getOrientation();
				if (orientation === Ti.UI.PORTRAIT || orientation === Ti.UI.UPSIDE_PORTRAIT){
					win.backgroundImage = a_bgImageName[0] +'-Portrait.'+ a_bgImageName[1];
				} else {
					win.backgroundImage = a_bgImageName[0] +'-Landscape.'+ a_bgImageName[1];
				}
				Ti.Gesture.addEventListener('orientationchange', orientationChanged);
			} else {
				win.backgroundImage = (params.backgroundImage)? params.backgroundImage : glb.config.theme.windowBackgroundImage;
			}
		break;
		case 'androidTablet':
			
		break;
		default:
			
	}
	return win;
};
module.exports=I4UForm;