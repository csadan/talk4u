var I4UFormAbstractFactory=require('classes/I4UFormAbstractFactory');
function Talk4UFormFactory(){
	I4UFormAbstractFactory.apply(this);
};
Talk4UFormFactory.prototype=new I4UFormAbstractFactory();
Talk4UFormFactory.prototype.createForm=function(type, params){
	var form = require('classes/forms/talk4u/'+ type);
	return new form(params);
	
};
Talk4UFormFactory.prototype.openForm = function(form){
	if (form.openStyle === 'inTab'){
		glb.tabgroup.activeTab.open(form);
	} else if (form.openStyle === 'inDetail'){
		glb.mainWindow.detailView.fn_close();
		glb.mainWindow.detailView.fn_open(form);
	} else if (form.openStyle === 'inNavGroup'){
		navigation.open(form);
	} else if (form.openStyle === 'modal'){
		form.open({modal: true});
	} else {
		form.open();
	}
};

module.exports=Talk4UFormFactory;