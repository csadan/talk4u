// API
// method createVersion() 
exports.createVersion = function(itemId, version, candidateId, fn_callback) {
	// xhr
    var xhr = Titanium.Network.createHTTPClient({
		timeout: glb.config.xhrTimeout
	});
	
	var data = []; //response object
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 200) {
			var errorCode = null;
			var response = null;
			try {
				response = JSON.parse(this.responseText);
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
			
			fn_callback(data);
			// fn_callback(response);
		}else {
			data.push({
				'errorCode': this.status, // status error
				'response': false,
			});
			
			fn_callback(data);
			// fn_callback(null);
		}
	};
	xhr.onerror = function(e) {
		trace('createVersion :: error','error');
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': null,
		});
		
		fn_callback(data);
	};
	
	if (candidateId){
		trace(glb.config.getApiServerPath('BoxVersions', candidateId)+'?candidateId='+itemId);
		xhr.open('POST',glb.config.getApiServerPath('BoxVersions', itemId)+'?candidateId='+candidateId);
	} else {
		trace(glb.config.getApiServerPath('BoxVersions', itemId));
		xhr.open('POST',glb.config.getApiServerPath('BoxVersions', itemId));
	}
	
	var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
	xhr_oAuth.send(xhr, version);
};