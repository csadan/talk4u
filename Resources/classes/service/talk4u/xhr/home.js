// this function will call the 3 methods necessary
// at the launch of the app
// 1. getContexts()
// 2. getFeaturedLists()
// 3. getTypes()
exports.home = function(fn_callback) {
	
	function processXhrList(cont){
		
		trace('GET = '+webserver_api[cont]);
        xhr.open('GET', webserver_api[cont]);
        
        var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
		xhr_oAuth.send(xhr);
	}
	
	var data = []; //response object
	var index=0;
	var webserver_api = [
		glb.config.getApiServerPath('BoxUserPersonalizations')+'/Contexts', //It will be data[0]
		glb.config.getApiServerPath('BoxUserPersonalizations')+'/FeaturedLists',//It will be data[1]
		glb.config.getApiServerPath('BoxDiscovery'),//It will be data[2]
	];
	
	var xhr = Ti.Network.createHTTPClient();
	
	xhr.timeout = glb.config.xhrTimeout;
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 200) {
			var errorCode = null;
			var response = null;
			try {
				response = JSON.parse(this.responseText);
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
			
			if (index < webserver_api.length-1) {
				// launch next service
				index++;
				processXhrList(index);
			} else {
				clearTimeout(timeoutId);
				// content load finished
				fn_callback(data);
			}
		}else {
			if (index < webserver_api.length) {
				// launch same service
				processXhrList(index);
			}
		}
	};
	
	xhr.onerror = function(e) {
		trace('home :: error','error');
		clearTimeout(timeoutId);
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': null,
		});
		
		fn_callback(data);
	};
	
	processXhrList(index);
	
	var timeoutId = setTimeout(function(){
		trace('home :: xhr Timeout','error');
		xhr=null;
		data.push({
			'errorCode': 2, // security timeout error
			'response': null,
		});
		fn_callback(data);
	}, glb.config.xhrSecurityTimeout);
};