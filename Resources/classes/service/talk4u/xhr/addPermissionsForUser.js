// API
// method addPermissionsForUser() 
exports.addPermissionsForUser = function(itemId, userOrGroupIds, itemPermissions, fn_callback) {
	function processXhrList(cont){
        xhr.open('POST', webserver_api[cont]);
        trace('POST = webserver_api['+cont+']='+webserver_api[cont]);
        		
        var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
		xhr_oAuth.send(xhr, itemPermissions);
	}
	
	var data = []; //response object
	var index=0;
	var webserver_api = [];
	for (var i in userOrGroupIds){
		webserver_api.push(glb.config.getApiServerPath('BoxPermissions',itemId)+'/'+userOrGroupIds[i]);
	}
	
	var xhr = Ti.Network.createHTTPClient();
	
	xhr.timeout = glb.config.xhrTimeout;
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 200) {
			var errorCode = null;
			var response = null;
			try {
				response = this.responseText;
				if(!response)
					response = userOrGroupIds[index];
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
									
			// if(!response)
				// data.push(userOrGroupIds[index]);
			
			if(index < webserver_api.length-1) {
				// launch next service
				index++;
				processXhrList(index);
			}else {
				clearTimeout(timeoutId);
				// content load finished
				fn_callback(data);
			}
		}else {
			
			data.push({
				'errorCode': this.status, // status error
				'response': userOrGroupIds[index],
			});
			
			// data.push(userOrGroupIds[index]);
			
			if (index < webserver_api.length-1) {
				// launch next service
				index++;
				processXhrList(index);
			}else {
				clearTimeout(timeoutId);
				// content load finished
				fn_callback(data);
			}
		}
	};
	
	xhr.onerror = function(e) {
		trace('addPermissionsForUser :: error','error');
		
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': userOrGroupIds[index],
		});
			
		// data.push(userOrGroupIds[index]);
		
		if (index < webserver_api.length-1) {
			// launch next service
			index++;
			processXhrList(index);
		}else {
			clearTimeout(timeoutId);
			// content load finished
			fn_callback(data);
		}
	};
	
	processXhrList(index);
	
	var timeoutId = setTimeout(function(){
		trace('addPermissionsForUser :: timeout','error');
		xhr=null;
		data.push({
			'errorCode': 2,
			'response': null,
		});
		fn_callback(data);
	}, glb.config.xhrSecurityTimeout);
};