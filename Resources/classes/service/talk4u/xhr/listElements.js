// API
// method listElements() 
exports.listElements = function(listType, levels, start, limit, sort, dir, filters, propertyNames, referenceId, fn_callback) {
	// xhr
    var xhr = Titanium.Network.createHTTPClient({
		timeout: glb.config.xhrTimeout
	});
	
	data = [];
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 200) {
			var errorCode = null;
			var response = null;
			try {
				response = JSON.parse(this.responseText);
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
			
			fn_callback(data);
		}else {
			data.push({
				'errorCode': this.status, // status error
				'response': false,
			});
			
			fn_callback(data);
		}
	};
	xhr.onerror = function(e) {
		trace('listElements :: error','error');
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': null,
		});
		
		fn_callback(data);
	};
	
	var q_referenceId = (referenceId && referenceId != '') ? '&referenceId='+referenceId : '';
	var q_filters = (filters && filters !== '') ? '&'+filters : '&filters=null';
	trace(glb.config.getApiServerPath('BoxElements')+'?listType='+listType+'&levels='+levels+'&start='+start+'&limit='+limit+'&sort='+sort+'&dir='+dir+q_filters+'&propertyNames='+propertyNames+q_referenceId);
	
	xhr.open('GET',glb.config.getApiServerPath('BoxElements')+'?listType='+listType+'&levels='+levels+'&start='+start+'&limit='+limit+'&sort='+sort+'&dir='+dir+q_filters+'&propertyNames='+propertyNames+q_referenceId);
	
	var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
	xhr_oAuth.send(xhr);
};