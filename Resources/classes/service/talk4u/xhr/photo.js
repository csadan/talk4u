// API
// method photo() 
exports.photo = function(userId, width, height, localFilePath, fn_callback) {
	// xhr
    var xhr = Titanium.Network.createHTTPClient({
		timeout: glb.config.xhrFilesTimeout
	});
	
	var data = []; //response object
	
	xhr.onload = function(){
		if(this.readyState == xhr.DONE && this.status == 200) {
			// var response=this.responseData;
			// response.localFilePath=localFilePath;
			// fn_callback(response);
			var errorCode = null;
			var response = null;
			try {
				var response=this.responseData;
				response.localFilePath=localFilePath;
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
			
			fn_callback(data);
		}else {
			data.push({
				'errorCode': this.status, // status error
				'response': false,
			});
			
			fn_callback(data);
			// fn_callback(false);
		}
	};
	xhr.onerror = function(e){
		trace('photo :: error','error');
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': null,
		});
		
		fn_callback(data);
	};
	
	if(width && !height)
		height=width;
	else if(!width && height)
		width=height;
	else if(!width && !height){
		width=100;
		height=100;
	}

	trace(glb.config.getApiServerPath('TalkUsersActions', userId)+'/Photo?width='+width+'&height='+height);
	xhr.open('GET',glb.config.getApiServerPath('TalkUsersActions', userId)+'/Photo?width='+width+'&height='+height);
	
	if(localFilePath && (glb.osname == 'iphone' || glb.osname == 'ipad')) {
		xhr.file = Titanium.Filesystem.getFile(localFilePath);
	}

	var xhr_oAuth = require('classes/service/talk4u/xhr/oAuth');
	xhr_oAuth.send(xhr);
};