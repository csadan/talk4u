// API
// method getUser() 
exports.getUser = function(id, fn_callback) {
		
	//xhr
    var xhr = Titanium.Network.createHTTPClient({
		timeout: glb.config.xhrTimeout
	});
	
	var data = []; //response object
		
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 200) {
			var errorCode = null;
			var response = null;
			try {
				
				response = JSON.parse(this.responseText);

			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
			
			fn_callback(data);

		}else {
			data.push({
				'errorCode': this.status, // status error
				'response': false,
			});
			
			fn_callback(data);

		}
	};
	xhr.onerror = function(e) {
		trace('getUser :: error','error');
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': null,
		});
		
		fn_callback(data);
	};
	
	trace(glb.config.getApiServerPath('TalkUsersActions',id));//TODO: quitar 1
	xhr.open('GET',glb.config.getApiServerPath('TalkUsersActions',id));
	
	var xhr_oAuth = require('classes/service/talk4u/xhr/oAuth');
	xhr_oAuth.send(xhr);


};


/*
{
    "id": 1,
    "userName": "administrador",
    "email": "47928",
    "name": "Administrador",
    "favourite": true,
    "photo": {
        "photoId": 97355,
        "photoUrl": null
    }
}  
*/



