// API
// method deleteVersion() 
exports.deleteVersion = function(itemId, versionId, fn_callback) {
	// xhr
    var xhr = Titanium.Network.createHTTPClient({
		timeout: glb.config.xhrTimeout
	});
	
	var data = []; //response object
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 204) {
			var errorCode = null;
			var response = null;
			try {
				response = this.responseText;
				response = true;
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
			
			fn_callback(data);
			// fn_callback(true);
		}else {
			data.push({
				'errorCode': this.status, // status error
				'response': false,
			});
			
			fn_callback(data);
			// fn_callback(null);
		}
	};
	xhr.onerror = function(e) {
		trace('deleteVersion :: error','error');
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': null,
		});
		
		fn_callback(data);
	};
	
	trace(glb.config.getApiServerPath('BoxVersions', itemId)+'/'+versionId);
	xhr.open('DELETE', glb.config.getApiServerPath('BoxVersions', itemId)+'/'+versionId);

	var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
	xhr_oAuth.send(xhr);
};