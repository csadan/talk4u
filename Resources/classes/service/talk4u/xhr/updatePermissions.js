// API
// method updatePermissions() 
exports.updatePermissions = function(itemId, permissions, fn_callback) {
	// xhr
    var xhr = Titanium.Network.createHTTPClient({
		timeout: glb.config.xhrTimeout
	});
	
	var data = []; //response object
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 200) {
			var errorCode = null;
			var response = null;
			try {
				response = JSON.parse(this.responseText);
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
			
			fn_callback(data);
			// fn_callback(response);
		}else {
			data.push({
				'errorCode': this.status, // status error
				'response': false,
			});
			
			fn_callback(data);
			// fn_callback(null);
		}
	};
	xhr.onerror = function(e) {
		trace('updatePermissions :: error :: '+e.error,'error');
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': null,
		});
		
		fn_callback(data);
	};
	
	trace('PUT = '+glb.config.getApiServerPath('BoxPermissions', itemId)+'?inherit='+permissions.inherit+'&propagate='+permissions.propagate);
	xhr.open('PUT', glb.config.getApiServerPath('BoxPermissions', itemId)+'?inherit='+permissions.inherit+'&propagate='+permissions.propagate);
	
	var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
	xhr_oAuth.send(xhr);
};