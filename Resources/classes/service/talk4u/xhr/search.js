// API
// method search() 
exports.search = function(searchText, start, limit, sort, dir, filters, fn_callback) {
	// xhr
    var xhr = Titanium.Network.createHTTPClient({
		timeout: glb.config.xhrTimeout
	});
	
	var data = []; //response object
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 200) {
			var errorCode = null;
			var response = null;
			try {
				response = JSON.parse(this.responseText);
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
			
			fn_callback(data);
			// fn_callback(response);
		}else {
			data.push({
				'errorCode': this.status, // status error
				'response': false,
			});
			
			fn_callback(data);
			// fn_callback(null);
		}
	};
	xhr.onerror = function(e) {
		trace('search :: error','error');
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': null,
		});
		
		fn_callback(data);
	};
	
	var q_filters = (filters && filters !== '') ? '&'+filters : '';
	trace(glb.config.getApiServerPath('BoxElements')+'?searchText='+searchText+'&start='+start+'&limit='+limit+'&sort='+sort+'&dir='+dir + q_filters);
	xhr.open('GET',glb.config.getApiServerPath('BoxElements')+'?searchText='+searchText+'&start='+start+'&limit='+limit+'&sort='+sort+'&dir='+dir + q_filters);
	
	var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
	xhr_oAuth.send(xhr);
};