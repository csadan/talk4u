// API
// method geolocate() 
exports.geolocate = function(itemIds, long, lat, fn_callback) {
	function processXhrList(cont){		
        xhr.open('POST', webserver_api[cont]);
        
        trace('webserver_api['+cont+']='+webserver_api[cont]);
        
        var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
		xhr_oAuth.send(xhr, params);
	}
	
	var data = []; //response object
	var index=0;
	var webserver_api = [];
	var users=[];
	var params = {
		'long': long,
		'lat': lat
	};
	for (var i in itemIds){
		webserver_api.push(glb.config.getApiServerPath('BoxElementsActions',itemIds[i])+'/Geolocate');
	}
	
	var xhr = Ti.Network.createHTTPClient();
	
	xhr.timeout = glb.config.xhrTimeout;
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 200) {
			var errorCode = null;
			var response = null;
			try {
				response = this.responseText;
				response = {itemId: itemIds[index], response: true};
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
						
			// data.push({itemId: itemIds[index], response: true});
			
			if(index < webserver_api.length-1) {
				// launch next service
				index++;
				processXhrList(index);
			}else {
				clearTimeout(timeoutId);
				// content load finished
				fn_callback(data);
			}
		}else {
			data.push({
				'errorCode': this.status, // status error
				'response': {itemId: itemIds[index], response: false},
			});
				
			// data.push({itemId: itemIds[index], response: false});
			
			if (index < webserver_api.length-1) {
				// launch next service
				index++;
				processXhrList(index);
			}else {
				clearTimeout(timeoutId);
				// content load finished
				fn_callback(data);
			}
		}
	};
	
	xhr.onerror = function(e) {
		trace('geolocate :: error');
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': {itemId: itemIds[index], response: null},
		});
			
		// data.push({itemId: itemIds[index], response: false});
		
		if (index < webserver_api.length-1) {
			// launch next service
			index++;
			processXhrList(index);
		}else {
			clearTimeout(timeoutId);
			// content load finished
			fn_callback(data);
		}
	};
	
	processXhrList(index);
	
	var timeoutId = setTimeout(function(){
		trace('geolocate :: timeout');
		xhr=null;
		data.push({
			'errorCode': 2, // security timeout error
			'response': null,
		});
		fn_callback(data);
	}, glb.config.xhrSecurityTimeout);
};