// API
// method getFilesAndFolders() 
exports.getFilesAndFolders = function(folderId, start, limit, sort, dir, filters, fn_callback) {
	// xhr
    var xhr = Titanium.Network.createHTTPClient({
		timeout: glb.config.xhrTimeout
	});
	
	data = []; //response object
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 200) {
			var errorCode = null;
			var response = null;
			try {
				response = JSON.parse(this.responseText);
			} catch(exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
			
			fn_callback(data);
		}else {
			data.push({
				'errorCode': this.status, // status error
				'response': false,
			});
			
			fn_callback(data);
		}
	};
	xhr.onerror = function(e) {
		trace('getFilesAndFolders :: error','error');
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': null,
		});
		
		fn_callback(data);
	};
	
	trace(glb.config.getApiServerPath('BoxChildren',folderId)+'?start='+start+'&limit='+limit+'&sort='+sort+'&dir='+dir+'&'+filters);
	xhr.open('GET',glb.config.getApiServerPath('BoxChildren',folderId)+'?start='+start+'&limit='+limit+'&sort='+sort+'&dir='+dir+'&'+filters);
	
	Ti.API.info('send XHR (getFilesAndFolders): '+ timestamp());
	
	var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
	xhr_oAuth.send(xhr);
};