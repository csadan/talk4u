// API
// method share() 
exports.share = function(itemId, share, fn_callback) {
	// xhr
    var xhr = Titanium.Network.createHTTPClient({
		timeout: glb.config.xhrTimeout
	});
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 200) {
			var errorCode = null;
			var response = null;
			try {
				response = this.responseText;
				response = {itemId: itemId, itemExternalLink: response, response: true};
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
			
			// data.push({itemId: itemId, itemExternalLink: response, response: true});
			
			fn_callback(data);
		}else {
			data.push({
				'errorCode': this.status, // status error
				'response': {itemId: itemId, itemExternalLink: null, response: false},
			});
			
			// data.push({itemId: itemId, itemExternalLink: null, response: false});
			fn_callback(data);
		}
	};
	xhr.onerror = function(e) {
		trace('share :: error','error');
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': null,
		});
		fn_callback(data);
	};
	// var http_protocol;
	// if(share)
		// http_protocol='POST';
	// else
		// http_protocol='DELETE';
		
	xhr.open('POST',glb.config.getApiServerPath('BoxElementsActions', itemId)+'/Share?share='+share);
	trace(glb.config.getApiServerPath('BoxElementsActions', itemId)+'/Share');
	
	var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
	xhr_oAuth.send(xhr);
};