exports.send = function(xhr, params) {
	
	var userName = Ti.App.Properties.getString('talk4u:displayName');
	var password = Ti.App.Properties.getString('talk4u:password');
	var authString = 'Basic ' +Titanium.Utils.base64encode(userName+':'+password);

	xhr.setRequestHeader('Authorization', authString);
	
	if(params){
		xhr.send(params);
	}
	else{
		xhr.send();
	}
}