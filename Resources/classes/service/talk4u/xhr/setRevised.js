// API
// method setRevised() 
exports.setRevised = function(itemIds, revised, fn_callback) {
	function processXhrList(cont){
		var http_protocol;
		if(revised[cont])
			http_protocol='DELETE';
		else
			http_protocol='POST';
		
        xhr.open(http_protocol, webserver_api[cont]);
        trace(http_protocol + ' = webserver_api['+cont+']='+webserver_api[cont]);
        
        var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
		xhr_oAuth.send(xhr);
	}
	
	var data = []; //response object
	var index=0;
	var webserver_api = [];
	var users=[];
	for (var i in itemIds){
		webserver_api.push(glb.config.getApiServerPath('BoxUserPersonalizations')+'/Revised/'+itemIds[i]);
	}
	
	var xhr = Ti.Network.createHTTPClient();
	
	xhr.timeout = glb.config.xhrTimeout;
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 200) {
			var errorCode = null;
			var response = null;
			try {
				response = this.responseText;
				response = {itemId: itemIds[index], response: true};
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
			
			// if(!response)
				// data.push(itemIds[index]);
			
			if(index < webserver_api.length-1) {
				// launch next service
				index++;
				processXhrList(index);
			}else {
				clearTimeout(timeoutId);
				// content load finished
				fn_callback(data);
			}
		}else {
			data.push({
				'errorCode': this.status, // status error
				'response': {itemId: itemIds[index], response: false},
			});
				
			// data.push(itemIds[index]);
			
			if (index < webserver_api.length-1) {
				// launch next service
				index++;
				processXhrList(index);
			}else {
				clearTimeout(timeoutId);
				// content load finished
				fn_callback(data);
			}
		}
	};
	
	xhr.onerror = function(e) {
		trace('setRevised :: error');
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': {itemId: itemIds[index], response: null},
		});
		
		//data.push(itemIds[index]);
		if (index < webserver_api.length-1) {
			// launch next service
			index++;
			processXhrList(index);
		}else {
			clearTimeout(timeoutId);
			// content load finished
			fn_callback(data);
		}
	};
	
	processXhrList(index);
	
	var timeoutId = setTimeout(function(){
		trace('setRevised :: timeout');
		xhr=null;
		data.push({
			'errorCode': 2, // security timeout error
			'response': null,
		});
		fn_callback(data);
	}, glb.config.xhrSecurityTimeout);
};