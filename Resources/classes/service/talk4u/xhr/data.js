// API
// method data() 
exports.data = function(itemId, localFilePath, progBar, fn_callback) {
	// xhr
    var xhr = Titanium.Network.createHTTPClient({
		timeout: glb.config.xhrFilesTimeout
	});
	
	var data = []; //response object
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 200) {
			// var response=this.responseData;
			// response.localFilePath=localFilePath;
			// fn_callback(response);
			var errorCode = null;
			var response = null;
			try {
				var response=this.responseData;
				response.localFilePath=localFilePath;
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
			
			fn_callback(data);
		} else {
		
			data.push({
				'errorCode': this.status, // status error
				'response': false,
			});
			
			fn_callback(data);
			// fn_callback(false);
		}
	};
	xhr.ondatastream = function(e) {
		if (progBar)
			progBar.progress.value = e.progress;
	};
	xhr.onerror = function(e) {
		trace('data :: error','error');
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': null,
		});
		
		fn_callback(data);
	};
	
	trace(glb.config.getApiServerPath('BoxElementsActions', itemId)+'/Data');
	xhr.open('GET',glb.config.getApiServerPath('BoxElementsActions', itemId)+'/Data');
	
	if(localFilePath && (glb.osname == 'iphone' || glb.osname == 'ipad')) {
		xhr.file = Titanium.Filesystem.getFile(localFilePath);
	}

	var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
	xhr_oAuth.send(xhr);
};