// API
// method setFavourite() 
exports.setFavourite = function(itemIds, favourites, fn_callback) {
	function processXhrList(cont){
		if(favourites[cont])
			http_protocol='POST';
		else
			http_protocol='DELETE';
        xhr.open(http_protocol, webserver_api[cont]);
        
        trace(webserver_api[cont]);
        trace(http_protocol);
        var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
		xhr_oAuth.send(xhr);
	}
	
	var data = []; //response object
	var index=0;
	var webserver_api = [];
	for (var i in itemIds){
		webserver_api.push(glb.config.getApiServerPath('BoxUserPersonalizations')+'/Favourite/'+itemIds[i]);
	}
	
	var xhr = Ti.Network.createHTTPClient();
	
	xhr.timeout = glb.config.xhrTimeout;
	
	xhr.onload = function() {
		if(data === null)
			data=[];
		if(this.readyState == xhr.DONE /*&& this.status == 200*/) {
			var errorCode = null;
			var response = null;
			try {
				response = this.responseText;
				response = {itemId: itemIds[index], favourite: favourites[index], response: true};
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
			
			// data.push({itemId: itemIds[index], favourite: favourites[index], response: true});
			
			if(index < webserver_api.length-1) {
				// launch next service
				index++;
				processXhrList(index);
			}else {
				clearTimeout(timeoutId);
				// content load finished
				fn_callback(data);
			}
		}else {
			data.push({
				'errorCode': this.status, // status error
				'response': {itemId: itemIds[index], favourite: favourites[index], response: false},
			});
				
			// data.push({itemId: itemIds[index], favourite: favourites[index], response: false});
			
			if (index < webserver_api.length-1) {
				// launch next service
				index++;
				processXhrList(index);
			}else {
				clearTimeout(timeoutId);
				// content load finished
				fn_callback(data);
			}
		}
	};
	
	xhr.onerror = function(e) {
		trace('setFavourite :: error');
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': {itemId: itemIds[index], favourite: favourites[index], response: null},
		});
			
		// data.push({itemId: itemIds[index], favourite: favourites[index], response: false});
		
		if (index < webserver_api.length-1) {
			// launch next service
			index++;
			processXhrList(index);
		}else {
			clearTimeout(timeoutId);
			// content load finished
			fn_callback(data);
		}
	};
	
	processXhrList(index);
	
	var timeoutId = setTimeout(function(){
		trace('send :: timeout');
		xhr=null;
		data.push({
			'errorCode': 2, // security timeout error
			'response': null,
		});
		fn_callback(data);
	}, glb.config.xhrSecurityTimeout);
  
};