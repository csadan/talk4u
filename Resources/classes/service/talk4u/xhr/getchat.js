// API
// method getEffectivePermissions() 
exports.getChat = function(params) {
	
	
	
	// xhr
    var xhr = Titanium.Network.createHTTPClient({
		timeout: glb.config.xhrTimeout
	});
	
	var data = []; //response object
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 200) {
			var errorCode = null;
			var response = null;
			try {
				response = JSON.parse(this.responseText);
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
			
			params.fn_callback(data);
			// fn_callback(response);
		}else {
			data.push({
				'errorCode': this.status, // status error
				'response': false,
			});
			
			params.fn_callback(data);
			// fn_callback(null);
		}
	};
	xhr.onerror = function(e) {
		trace('getChat :: error','error');
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': null,
		});
		
		params.fn_callback(data);
	};
	
	trace(glb.config.getApiServerPath('TalkChat',params.id));//TODO: quitar 1
	xhr.open('GET',glb.config.getApiServerPath('TalkChat',params.id));
	
	var xhr_oAuth = require('classes/service/talk4u/xhr/oAuth');
	xhr_oAuth.send(xhr);
	
};