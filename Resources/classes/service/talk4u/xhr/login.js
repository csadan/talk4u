// API
// method login()
exports.login = function(user,pass,fn_callback) {
	// xhr
    var xhr = Titanium.Network.createHTTPClient({
		timeout: glb.config.xhrTimeout
	});
	
	var data = []; //response object
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 200) {
			data.push({
				'errorCode': null,
				'response': {
					'user': user,
					'pass': pass,
				},
			});
			fn_callback(data);
		}else {
			data.push({
				'errorCode': this.status, // status error
				'response': false,
			});
			fn_callback(data);
		}
	};
	xhr.onerror = function(e) {
		trace('login :: error','error');
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': null,
		});
		
		fn_callback(data);
	};
	
	xhr.open('GET',glb.config.getApiServerPath('TalkUserPersonalizations')+'/Contexts');
	
	var xhr_oAuth = require('classes/service/talk4u/xhr/oAuth');
	xhr_oAuth.send(xhr);
};