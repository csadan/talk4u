// API
// method deleteRelations() 
exports.deleteRelations = function(itemId, parentIds, childrenIds, fn_callback) {
	
	function processXhrList(cont){		
        xhr.open('DELETE', webserver_api[cont]);
        
        trace(webserver_api[cont]);
        
        var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
		xhr_oAuth.send(xhr);
	}
	
	var data = []; //response object
	var index=0;
	var webserver_api = [];
	var parentsAndChildren=(parentIds)? parentIds.concat(childrenIds) : childrenIds.concat();
	
	if(parentIds && parentIds.length>0){
		for (var i in parentIds){
			webserver_api.push(glb.config.getApiServerPath('BoxParents', itemId)+'?parents='+parentIds[i]);
		}		
	}
	
	if(childrenIds && childrenIds.length>0){
		for (var i in childrenIds){
			webserver_api.push(glb.config.getApiServerPath('BoxChildren', itemId)+'?children='+childrenIds[i]);
		}
	}
	
	var xhr = Ti.Network.createHTTPClient();
	
	xhr.timeout = glb.config.xhrTimeout;
	
	xhr.onload = function() {
		if (parentIds && parentIds.length >0 && index < parentIds.length){
			var id = itemId;
			var parentId = parentsAndChildren[index];
			var childId = itemId;
		} else {
			var id = parentsAndChildren[index];
			var parentId = itemId;
			var childId = parentsAndChildren[index];
		}
		if(this.readyState == xhr.DONE && (this.status == 200 || this.status == 204)) {						
			var errorCode = null;
			var response = null;
			try {
				response = this.responseText;
				response = {itemId: id, parentId: parentId, childId: childId, response: response};
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
			
						
			// data.push({itemId: id, parentId: parentId, childId: childId, response: response});
			
			if(index < webserver_api.length-1) {
				// launch next service
				index++;
				processXhrList(index);
			}else {
				clearTimeout(timeoutId);
				// content load finished
				fn_callback(data);
			}
		} else {
			data.push({
				'errorCode': this.status, // status error
				'response': {itemId: id, parentId: parentId, childId: childId, response: false},
			});
				
			// data.push({itemId: id, parentId: parentId, childId: childId, response: false});
			
			if (index < webserver_api.length-1) {
				// launch next service
				index++;
				processXhrList(index);
			}else {
				clearTimeout(timeoutId);
				// content load finished
				fn_callback(data);
			}
		}
	};
	
	xhr.onerror = function(e) {
		trace('deleteRelations :: error');
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': {itemId: id, parentId: parentId, childId: childId, response: null},
		});
			
		// data.push({itemId: id, parentId: parentId, childId: childId, response: false});
		
		if (index < webserver_api.length-1) {
			// launch next service
			index++;
			processXhrList(index);
		}else {
			clearTimeout(timeoutId);
			// content load finished
			fn_callback(data);
		}
	};
	
	processXhrList(index);
	
	var timeoutId = setTimeout(function(){
		trace('deleteRelations :: timeout');
		xhr=null;
		data.push({
			'errorCode': 2, // security timeout error
			'response': null,
		});
		fn_callback(data);
	}, glb.config.xhrSecurityTimeout);
	
};