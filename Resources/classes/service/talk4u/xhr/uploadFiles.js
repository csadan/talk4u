// API
// method uploadFiles() 
exports.uploadFiles = function(itemId, fileName, file, progBar, fn_callback) {
	// xhr
    var xhr = Titanium.Network.createHTTPClient({
		timeout: glb.config.xhrFilesTimeout,
		enableKeepAlive: false
	});
	
	var data = []; // resonse object
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 201) {
			var errorCode = null;
			var response = null;
			try {
				response = JSON.parse(this.responseText);
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'errorMessage': null,
				'response': response,
			});
			
			fn_callback(data);
		}else {
			data.push({
				'errorCode': errorCode,
				'errorMessage': L('pb_server'),
				'response': false,
			});
			
			fn_callback(data);
			// fn_callback({res: false, errorMessage: L('pb_server')});
		}
	};
	xhr.onsendstream = function(e) {
		progBar.progress.value = e.progress;
	}
	xhr.onerror = function(e) {
		trace('uploadFiles :: error','error');
		var message = L('uploadFiles_error');
		if(e.error){
			var match_arr = e.error.match(/\"([^\"]+)\"/);
			if (e.error.indexOf("ASIHTTPRequestError") > -1 && match_arr.length > 0) { // Catches iPhone
			    message = match_arr[1];
			}
		}
		data.push({
				'errorCode': errorCode,
				'errorMessage': message,
				'response': false,
			});
			
		fn_callback(data);
		//fn_callback({res: null, errorMessage: message});
	};
	
	trace(glb.config.getApiServerPath('BoxElementsActions', itemId)+'/Content?fileName='+encodeURIComponent(fileName));
	xhr.open('POST', glb.config.getApiServerPath('BoxElementsActions', itemId)+'/Content?fileName='+encodeURIComponent(fileName));
	
	var params = {
		'upfile': file
	};
	
	var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
	xhr_oAuth.send(xhr, params);

};