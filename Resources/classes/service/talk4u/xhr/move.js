// API
// method move() 
exports.move = function(itemIds, fromFolder, toFolder, /*name,*/ fn_callback) {
	function processXhrList(cont){
        xhr.open('POST', webserver_api[cont]);
        
        trace(webserver_api[cont]);
        
        var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
		xhr_oAuth.send(xhr);
	}
	
	var data = []; //response object
	var index=0;
	var webserver_api = [];
	for (var i in itemIds){
		webserver_api.push(glb.config.getApiServerPath('BoxElementsActions', itemIds[i])+'/Move?fromFolder='+fromFolder+'&toFolder='+toFolder);
	}
	
	var xhr = Ti.Network.createHTTPClient();
	
	xhr.timeout = glb.config.xhrTimeout;
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE /*&& this.status == 200*/) {
			var errorCode = null;
			var response = null;
			try {
				response = JSON.parse(this.responseText);
				response = {itemId: itemIds[index], response: true};
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
			
			// data.push({itemId: itemIds[index], response: true});
			
			if(index < webserver_api.length-1) {
				// launch next service
				index++;
				processXhrList(index);
			}else {
				clearTimeout(timeoutId);
				// content load finished
				fn_callback(data);
			}
		}else {
			data.push({
				'errorCode': this.status, // status error
				'response': {itemId: itemIds[index], response: false},
			});
				
			// data.push({itemId: itemIds[index], response: false});
			
			if (index < webserver_api.length-1) {
				// launch next service
				index++;
				processXhrList(index);
			}else {
				clearTimeout(timeoutId);
				// content load finished
				fn_callback(data);
			}
		}
	};
	
	xhr.onerror = function(e) {
		trace('move :: error');
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': {itemId: itemIds[index], response: null},
		});
			
		// data.push({itemId: itemIds[index], response: false});
		
		if (index < webserver_api.length-1) {
			// launch next service
			index++;
			processXhrList(index);
		}else {
			clearTimeout(timeoutId);
			// content load finished
			fn_callback(data);
		}
	};
	
	processXhrList(index);
	
	var timeoutId = setTimeout(function(){
		trace('move :: timeout');
		xhr=null;
		data.push({
			'errorCode': 2, // security timeout error
			'response': null,
		});
		fn_callback(data);
	}, glb.config.xhrSecurityTimeout);
};