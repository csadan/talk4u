exports.initialConfigData = function(fn_callback) {
	
	var xhr = Titanium.Network.createHTTPClient({
		timeout: glb.config.xhrTimeout
	});
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 200) {
			var response;
			try{
				response = JSON.parse(this.responseText);
			}catch(exception){
				response=null;
			}
			fn_callback(response);
		}else {
			fn_callback(null);
		}
	};
	
	xhr.onerror = function(e) {
		trace('initialConfigData :: error','error');
		traceObject(e,'error getGroup');
		fn_callback(null);
	};
	
	trace(glb.config.getApiServerPath('BoxUserPersonalizations')+'/Contexts');
	xhr.open('GET',glb.config.getApiServerPath('BoxUserPersonalizations')+'/Contexts');
	
	var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
	xhr_oAuth.send(xhr);
};