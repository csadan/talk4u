// API
// method getChats()
exports.getChats = function(param, fn_callback) {
	// xhr

	// var xhr = Titanium.Network.createHTTPClient({
	// timeout : glb.config.xhrTimeout
	// });
	//
	// xhr.onload = function() {
	// if (this.readyState == xhr.DONE && this.status == 200) {
	// var response;
	// try {
	// response = JSON.parse(this.responseText);
	// } catch(exception) {
	// response = null;
	// }
	// fn_callback(response);
	// } else {
	// fn_callback(null);
	// }
	// };
	// xhr.onerror = function(e) {
	// trace('getChats :: error', 'error');
	// fn_callback(null);
	// };
	// xhr.open('GET', glb.config.getApiServerPath('BoxElementsActions', parentId) + '/AddLink?title=' + title + '&url=' + url);
	//
	// var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
	// xhr_oAuth.send(xhr);
	//trace(glb.config.getApiServerPath('TalkChat',params.id));//TODO: quitar 1
	response = {
		"items" : [{
			"id" : 1,
			"topic" : "trabajador1@adminuser/Gajim",
			"created" : "2013-05-13T20:08:25.883452+02:00",
			"userRelatedInfo" : {
				"important" : true,
				"revised" : true,
				"followed" : true
			},
			"lastActive" : "2013-05-13T20:08:25.8844521+02:00",
			"participants" : [{
				"id" : 1,
				"name" : "sample string 2"
			}, {
				"id" : 1,
				"name" : "sample string 2"
			}, {
				"id" : 1,
				"name" : "sample string 2"
			}],
			"xmppjid" : "trabajador1@adminuser/Gajim"
		}, {
			"id" : 1,
			"topic" : "trabajador2@adminuser/Gajim",
			"created" : "2013-05-13T20:08:25.883452+02:00",
			"userRelatedInfo" : {
				"important" : true,
				"revised" : true,
				"followed" : true
			},
			"lastActive" : "2013-05-13T20:08:25.8844521+02:00",
			"participants" : [{
				"id" : 1,
				"name" : "sample string 2"
			}, {
				"id" : 1,
				"name" : "sample string 2"
			}, {
				"id" : 1,
				"name" : "sample string 2"
			}],
			"xmppjid" : "trabajador2@adminuser/Gajim"
		}, {
			"id" : 1,
			"topic" : "admin@adminuser/Gajim",
			"created" : "2013-05-13T20:08:25.883452+02:00",
			"userRelatedInfo" : {
				"important" : true,
				"revised" : true,
				"followed" : true
			},
			"lastActive" : "2013-05-13T20:08:25.8844521+02:00",
			"participants" : [{
				"id" : 1,
				"name" : "sample string 2"
			}, {
				"id" : 1,
				"name" : "sample string 2"
			}, {
				"id" : 1,
				"name" : "sample string 2"
			}],
			"xmppjid" : "admin@adminuser/Gajim"
		}],
		"totalCount" : 1
	}
	fn_callback(response);
};
