// API
// method getPermissions() 
exports.getAllPermissions = function(itemId, fn_callback) {

	function processXhrList(cont){
        trace(webserver_api[cont]);
		
        xhr.open('GET', webserver_api[cont]);
        
        var xhr_oAuth = require('classes/service/box4u/xhr/oAuth');
		xhr_oAuth.send(xhr);
	}
	
	var data = []; //response object
	var index=0;
	var webserver_api = [
		glb.config.getApiServerPath('BoxPermissions',itemId),//It will be data[0]
		glb.config.getApiServerPath('BoxEffectivePermissions',itemId),//It will be data[1]
	];
	
	var xhr = Ti.Network.createHTTPClient();
	
	xhr.timeout = glb.config.xhrTimeout;
	
	xhr.onload = function() {
		if(this.readyState == xhr.DONE && this.status == 200) {
			var errorCode = null;
			var response = null;
			try {
				response = JSON.parse(this.responseText);
			} catch (exception) {
				errorCode = 3; // response parse error
			}
				
			data.push({
				'errorCode': errorCode,
				'response': response,
			});
			
			if (index < webserver_api.length-1) {
				// launch next service
				index++;
				processXhrList(index);
			} else {
				clearTimeout(timeoutId);
				// content load finished
				fn_callback(data);
			}
		}else {
			// launch same service
			processXhrList(index);
		}
	};
	
	xhr.onerror = function(e) {
		trace('getAllPermissions :: error','error');
		clearTimeout(timeoutId);
		data.push({
			'errorCode': (this.status) ? this.status : 1, // general error
			'response': null,
		});
		
		fn_callback(data);
	};
	
	processXhrList(index);
	
	var timeoutId = setTimeout(function(){
		trace(xhr.statusText);
		xhr=null;
		data.push({
			'errorCode': 2, // security timeout error
			'response': null,
		});
		fn_callback(data);
	}, glb.config.xhrSecurityTimeout);
};