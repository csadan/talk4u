var xmppservice = function() {

}

xmppservice.prototype.joinChat = function(params) {
	conn.send(xmpp.presence({
		xmlns : "jabber:client",
		to : params.xmppjid,
		from : params.jid
	}));

}
xmppservice.prototype.sendMessage = function(params) {
	trace("[SEND] from: "+params.from +" to: "+params.to+" message: "+params.message);
	talk4u.chatList.newMessageSent(params.from,params.to,params.message);
	conn.send(xmpp.message({
		from : params.from,     //yo
		to : params.to,  		//el
		type : "chat"
	}).c("body").t(params.message));
	
}

module.exports = xmppservice;
