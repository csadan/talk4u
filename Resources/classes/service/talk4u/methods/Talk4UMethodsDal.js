var addLink = function(params){
	
	return false;
	
};

var addPermissionsForUser = function(params/*itemId, userOrGroupId, canRead, canWrite, canAddFather, canAddChilds, fn_callback*/){
	
	return false;
	
};

var advancedSearch = function(params){
	
	return false;
	
};

var convertInVersionOf = function(params/*candidateId, itemId, comments, fn_callback*/){
	
	return false;
	
};

var copy = function(params/*itemIds, folder, fn_callback*/){
	
	return false;
	
};

var createElements = function(dataXHR){
	
	if (dataXHR && isArray(dataXHR) && dataXHR.length > 0) {
		var statements = [];
		for (var i in dataXHR){
			if (dataXHR[i]) {
				// parse data to the correct format in sqlite (boolean type doesn't exist)
				var revised = (dataXHR[i].userRelatedInfo.revised)? 1 : 0;
				var important = (dataXHR[i].userRelatedInfo.important)? 1 : 0;
				var followed = (dataXHR[i].userRelatedInfo.followed)? 1 : 0;
				var deleted = (dataXHR[i].userRelatedInfo.deleted)? 1 : 0;
				
				// call from an element
				if (dataXHR[i].typeSpecificProperties && dataXHR[i].typeSpecificProperties.length>0) {
					var completeName;
					if (dataXHR[i].typeId === 50) {
						completeName = (dataXHR[i].typeSpecificProperties && dataXHR[i].typeSpecificProperties.length>0 && dataXHR[i].typeSpecificProperties[0].value) ? dataXHR[i].typeSpecificProperties[0].value : null;
					} else {
						for (var j in dataXHR[i].typeSpecificProperties) {
							if (dataXHR[i].typeSpecificProperties[j].name === 'NombreCompleto'){
								completeName = dataXHR[i].typeSpecificProperties[0].value;
								break;
							}
						}
					}
					var extension = null;
					var fileExtension = null;
					if (completeName){
						var a_extension = completeName.split(".");
						if (a_extension.length > 1)
							fileExtension = '.' + a_extension[a_extension.length-1];
						else
							extension = '.html';
					} else {
						extension = '.html';
					}
					// Check results
					if (typeof completeName == "undefined") {
						// It is a special element (mail, note,...)
						var completeNameFormatted = dataXHR[i].description + extension;
					} else if (completeName == null) {
						// We haven't cached before
						var completeNameFormatted = null;
					} else {
						// Set returned value
						var completeNameFormatted = (fileExtension)? completeName : completeName + extension;
					}
				} else {
					// call from a list
					var completeNameFormatted = null;
				}
				var description = (dataXHR[i].description) ? "'"+ dataXHR[i].description.replace(/'/g, "\'") +"'" : null;
				
				var completeNameField = (completeNameFormatted)? 'completeName, ' : '';
				var completeNameValue = (completeNameFormatted)? "'"+ completeNameFormatted.replace(/'/g, "\'") +"', " : '';
				var completeNameUpdate = (completeNameFormatted)? "completeName='"+ completeNameFormatted.replace(/'/g, "\'") +"', " : '';
				
				var lastModificationDateStatment = dataXHR[i].auditInfo.lastModificationDate ? "'" + dataXHR[i].auditInfo.lastModificationDate + "'" : null;
				
				var table = "INSERT OR IGNORE INTO items";
				var fields = " (id, revised, important, followed, deleted, size, description, "+ completeNameField +" date, lastModificationDate, typeId, ownerId, longitude, latitude)";				
				var values = " VALUES ("+ dataXHR[i].id +", "+ revised +", "+ important +", "+ followed +", "+ deleted +", '"+ dataXHR[i].size 
							+"', "+ description +", "+ completeNameValue +"'"+ dataXHR[i].date +"', "+ lastModificationDateStatment
							+", "+ dataXHR[i].typeId +", "+ dataXHR[i].owner.ownerId	+", "+ dataXHR[i].location.long +", "+ dataXHR[i].location.lat +")";
				
				var statement = table + fields + values;
				trace(statement);
				statements.push(statement);
			 
				var table = "UPDATE items";
				var fields = "";
				var values = " SET revised="+ revised +", important="+ important +", followed="+ followed +", deleted="+ deleted +", size='"+ dataXHR[i].size 
							+"', description="+ description +", "+ completeNameUpdate +"date='"+ dataXHR[i].date +"', lastModificationDate="+ lastModificationDateStatment
							+", typeId="+ dataXHR[i].typeId +", ownerId="+ dataXHR[i].owner.ownerId	+", longitude="+ dataXHR[i].location.long +", latitude="+ dataXHR[i].location.lat;
				var where = " WHERE id="+ dataXHR[i].id;
				
				var statement = table + fields + values + where;
				trace(statement);
				statements.push(statement);
			}
		}
		return statements;
	} else {
		return null;
	}
	
};

var createFolder = function(params/*parentId, name, fn_callback*/){
	
	return false;
	
};

var createGroups = function(dataXHR/*id, name, favourite*/){
	
	if (dataXHR && isArray(dataXHR) && dataXHR.length > 0) {
		var statements = [];
		for (var i in dataXHR){
			var name = ( dataXHR[i].name) ? "'"+ dataXHR[i].name.replace(/'/g, "\'") +"'" : null;
			var favourite = (dataXHR[i].favourite)? 1 : 0;
			
			var table = "INSERT OR REPLACE INTO usersOrGroups";
			var fields = " (id, name, favourite, isGroup)";
			var values = " VALUES ("+ dataXHR[i].id +", "+ name +", "+ favourite +", 1)";
			var statement = table + fields + values;
			statements.push(statement);
		}
		return statements;
		
	} else {
		return null;
	}
	
};

var createRelations = function(dataXHR /*parentId, childIds[]*/){
	
	if (dataXHR && dataXHR.parentId && dataXHR.childIds && dataXHR.childIds.length > 0) {
		var statements = [];
		if (isArray(dataXHR.childIds)){
			for (var i in dataXHR.childIds){
				if (dataXHR.childIds[i]){
					var table = "INSERT INTO relations";
					var fields = " (parentId, childId)";
					var values = " VALUES ("+ dataXHR.parentId +", "+ dataXHR.childIds[i] +")";
					var statement = table + fields + values;
					statements.push(statement);
				}
			}
		} else {
			var table = "INSERT INTO relations";
			var fields = " (parentId, childId)";
			var values = " VALUES ("+ dataXHR.parentId +", "+ dataXHR.childIds +")";
			var statement = table + fields + values;
			statements.push(statement);
		}
		return statements;
		
	} else {
		return null;
	}
		
};

var createUsers = function(dataXHR/*id, userName, email, name, favourite*/){
	
	if (dataXHR && isArray(dataXHR) && dataXHR.length > 0) {
		var statements = [];
		for (var i in dataXHR){
			var userName = (dataXHR[i].userName) ? "'"+ dataXHR[i].userName.replace(/'/g, "\'") +"'" : null;
			var email = (dataXHR[i].email) ? "'"+ dataXHR[i].email.replace(/'/g, "\'") +"'" : null;
			var name = (dataXHR[i].name) ? "'"+ dataXHR[i].name.replace(/'/g, "\'") +"'" : null;
			
			var favourite = (dataXHR[i].favourite)? 1 : 0;
			
			var table = "INSERT OR REPLACE INTO usersOrGroups";
			var fields = " (id, userName, email, name, favourite, isGroup)";
			var values = " VALUES ("+ dataXHR[i].id +", "+ userName +", "+ email +", "+ name +", "+ favourite +", 0)";
			var statement = table + fields + values;
			statements.push(statement);
		}
		return statements;
		
	} else {
		return null;
	}
	
};

var createVersion = function(params/*itemId, version, candidateId, fn_callback*/){
	
	return false;
	
};

// var data = function(params/*itemId, localFilePath, fn_callback*/){
// 	
// 
// 	
// };

var _delete = function(params/*itemId, recursive, fn_callback*/){
	
	return false;
	
};

var deleteGroups = function(){
	
	var tables = "DELETE FROM usersOrGroups";
	var where = " WHERE isGroup=1";
	var statement = tables + where;
		
	return statement;
	
};

var deleteItemPermission = function(params/*itemId, userOrGroupId,fn_callback*/){
	
	return false;
	
};

var deleteRelations = function(dataXHR/*parentId, childIds[itemId]*/){
	
	if (dataXHR && dataXHR.parentId) {
		var tables = "DELETE FROM relations";
		var where = " WHERE parentId="+ dataXHR.parentId;
		
		if (dataXHR.childIds && isArray(dataXHR.childIds) && dataXHR.childIds.length > 0)
			where += " AND childId IN ("+ dataXHR.childIds.toString() +")";
		else if (dataXHR.childIds && dataXHR.childIds.length > 0)
			where += " AND childId IN ("+ dataXHR.childIds +")";
		
		var statement = tables + where;
		
		return statement;
	} else {
		return null;
	}	
	
};

var deleteUsers = function(){
	
	var tables = "DELETE FROM usersOrGroups";
	var where = " WHERE isGroup=0";
	var statement = tables + where;
		
	return statement;
	
};

var deleteVersion = function(params/*itemId, versionId, version, fn_callback*/){
	
	return false;
	
};

var emptyTrashBin = function(params/*fn_callback*/){
	
	return false;
	
};

var extractVersion = function(params/*itemId, versionId, fn_callback*/){
	
	return false;
	
};

var geolocate = function(params/*itemIds, long, lat, fn_callback*/){
	
	return false;
	
};

var getAccount = function(params/*fn_callback*/){
	
	return false;
	
};

var getEffectivePermissions = function(params/*itemId, fn_callback*/){
	
	return false;
	
};

var getElement = function(params/*itemId, fn_callback*/){
	
	var fields = "SELECT id, revised, important, followed, deleted, size, description, completeName, date, lastModificationDate, typeId, ownerId, longitude, latitude";
	var tables = " FROM items";
	var where = " WHERE id="+ params.itemId;
	var order_by = "";
	var limit = "";
	
	var statement = fields + tables + where + order_by + limit;

	return statement;
	
};

var getFavouriteGroups = function(params/*start, limit, sort, fn_callback*/){
	
	var fields = "SELECT id, email, name, favourite";
	var tables = " FROM usersOrGroups";
	var where = " WHERE isGroup=1 AND favourite=1";
	var order_by = " ORDER BY lower(name) ASC"/*+ params.sort*/;
	var limit = "";
	
	var statement = fields + tables + where + order_by + limit;

	return statement;
	
};

var getFavouriteUsers = function(params/*start, limit, sort, fn_callback*/){
	
	var fields = "SELECT id, userName, email, name, favourite";
	var tables = " FROM usersOrGroups";
	var where = " WHERE isGroup=0 AND favourite=1";
	var order_by = " ORDER BY lower(name) ASC"/*+ params.sort*/;
	var limit = " ";
	
	var statement = fields + tables + where + order_by + limit;

	return statement;
	
};

var getFilesAndFolders = function(params/*itemId, start, limit, sort, dir, filters, fn_callback*/){
	
		var fields = "SELECT id, revised, important, followed, deleted, size, description, completeName, date, lastModificationDate, typeId, ownerId, longitude, latitude";
		var tables = " FROM items JOIN relations ON (items.id=relations.childId)";
		var where = " WHERE relations.parentId="+ params.itemId +" AND items.deleted=0";
		var order_by = (params.sort)? " ORDER BY lower("+ params.sort +") "+ params.dir : "";
		var limit = (params.limit !== -1)? " LIMIT "+ (params.limit+1) +' OFFSET '+ params.start : "";
		
		var statement = fields + tables + where + order_by + limit;

		return statement;
		
};

var getFilesAndFoldersTotalCount = function(params/*itemId*/){
	
		var fields = "SELECT count(*) as totalCount";
		var tables = " FROM items JOIN relations ON (items.id=relations.childId)";
		var where = " WHERE relations.parentId="+ params.itemId +" AND items.deleted=0";
		var order_by = "";
		var limit = "";
		
		var statement = fields + tables + where + order_by + limit;

		return statement;
		
};

var getGroups = function(params/*start, limit, sort*/){
	
	var fields = "SELECT id, email, name, favourite";
	var tables = " FROM usersOrGroups";
	var where = " WHERE isGroup=1";
	var order_by = " ORDER BY lower(name) ASC"/*+ params.sort*/;
	var limit = (params.limit !== -1)? " LIMIT "+ (params.limit+1) +' OFFSET '+ params.start : "";
	
	var statement = fields + tables + where + order_by + limit;

	return statement;
	
};

var getGroupsTotalCount = function(){
	
	var fields = "SELECT count(*) as totalCount";
	var tables = " FROM usersOrGroups";
	var where = " WHERE isGroup=1";
	var order_by = "";
	var limit = "";
	
	var statement = fields + tables + where + order_by + limit;

	return statement;
	
};

var getParentsAndChilds = function(params/*itemId, start, limit, sort, fn_callback*/){
	
	return false;
	
};

var getAllPermissions = function(params/*itemId, fn_callback*/){
	
	return false;
	
};

var getPermissions = function(params/*itemId, fn_callback*/){
	
	return false;
	
};

var getPermissionsForUser = function(params/*itemId, userOrGroupId, fn_callback*/){
	
	return false;
	
};

var getUser = function(params/*userId, fn_callback*/){
	
	return false;
	
};

var getGroup = function(params/*groupId, fn_callback*/){
	
	return false;
	
};

var getUserPermits = function(params/*itemId, text, fn_callback*/){
	
	return false;
	
};

var getUsers = function(params/*start, limit, sort*/){
	
	var fields = "SELECT id, userName, email, name, favourite";
	var tables = " FROM usersOrGroups";
	var where = " WHERE isGroup=0";
	var order_by = " ORDER BY lower(name) ASC"/*+ params.sort*/;
	var limit = (params.limit !== -1)? " LIMIT "+ (params.limit+1) +' OFFSET '+ params.start : "";
	
	var statement = fields + tables + where + order_by + limit;

	return statement;
	
};

var getUsersTotalCount = function(){
	
	var fields = "SELECT count(*) as totalCount";
	var tables = " FROM usersOrGroups";
	var where = " WHERE isGroup=0";
	var order_by = "";
	var limit = "";
	
	var statement = fields + tables + where + order_by + limit;

	return statement;
	
};

var getVersion = function(params/*itemId, versionId, fn_callback*/){
	
	return false;
	
};

var getVersions = function(params/*itemId, fn_callback*/){
	
	return false;
	
};

var home = function(params/*fn_callback*/){
	
	return false;
	
};

var initialConfigData = function(params/*fn_callback*/){
	
	return false;
	
};

var listElements=function(params/*listType, referenceId, start, limit, sort, dir, filters, propertyNames, fn_callback*/){

	if (params.listType === 'Favourites') {
		var fields = "SELECT i.id, i.revised, i.important, i.followed, i.deleted, i.size, i.description, i.completeName, i.date, i.lastModificationDate, i.typeId, i.ownerId, u.name, i.longitude, i.latitude";
		var tables = " FROM items i, usersOrGroups u";
		var where = " WHERE i.ownerId=u.id AND i.important=1";
		var order_by = (params.sort)? " ORDER BY lower(i."+ params.sort +") "+ params.dir : "";
		var limit = (params.limit !== -1)? " LIMIT "+ (params.limit+1) +' OFFSET '+ params.start : "";
		
		var statement = fields + tables + where + order_by + limit;

		return statement;
	} else {
		return false;
	}
	
};

var listElementsFavouritesTotalCount = function(){
	
	var fields = "SELECT count(*) as totalCount";
	var tables = " FROM items";
	var where = " WHERE important=1";
	var order_by = "";
	var limit = "";
	
	var statement = fields + tables + where + order_by + limit;

	return statement;
	
};

var login = function(params/*userName, password, fn_callback*/){
	
	return false;
	
};

var logout = function(params/*fn_callback*/){
	
	return false;
	
};

var move = function(params/*itemIds, fromFolder, toFolder, fn_callback*/){
	
	return false;
	
};

// var photo = function(params/*itemId, localFilePath, fn_callback*/){
// 	
// 
// 	
// };

var restore = function(params/*itemIds, recursive, fn_callback*/){
	
	return false;
	
};

var search = function(params/*text, start, limit, sort, dir, filters, fn_callback*/){
	
	return false;
	
};

var searchGroup = function(params/*searchText, start, limit, fn_callback*/){
	
	return false;
	
};

var searchUser = function(params/*searchText, start, limit, fn_callback*/){
	
	return false;
	
};

var send = function(params/*itemIds, userOrGroupIds, fn_callback*/){
	
	return false;
	
};

var setFavourite = function(params/*itemIds, favourites, fn_callback*/){
	
	return false;
	
};

var setRevised = function(params/*itemId, revised, fn_callback*/){
	
	return false;
	
};

var share = function(params/*itemId, share, fn_callback*/){
	
	return false;
	
};

var trash = function(params/*itemIds, recursive, fn_callback*/){
	
	return false;
	
};

var updateAccount = function(params/*account, fn_callback*/){
	
	return false;
	
};

var updatePermissions = function(params/*itemId, permissions, fn_callback*/){
	
	return false;
	
};

var updatePermissionsForUserGroup = function(params/*itemId, userOrGroupId, userGroupPermissions, fn_callback*/){
	
	return false;
	
};

var updateVersion = function(params/*itemId, versionId, version, fn_callback*/){
	
	return false;
	
};

// var uploadFiles = function(params/*itemId, fileName, file, progBar, fn_callback*/){
// 	
// 
// 	
// };