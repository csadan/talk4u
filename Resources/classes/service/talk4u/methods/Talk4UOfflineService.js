var addLink = function(params)
{
	params.fn_callback(false);
};

var addPermissionsForUser = function(params/*itemId, userOrGroupId, canRead, canWrite, canAddFather, canAddChilds, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var advancedSearch = function(params){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var convertInVersionOf = function(params/*candidateId, itemId, comments, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var copy = function(params/*itemIds, folder, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var createFolder = function(params/*parentId, name, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var createVersion = function(params/*itemId, version, candidateId, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var data = function(params/*itemId, localFilePath, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var _delete = function(params/*itemId, recursive, fn_callback*/){
	
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var deleteItemPermission = function(params/*itemId, userOrGroupId,fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var deleteRelations = function(params/*itemId, parentIds, childrenIds, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var deleteVersion = function(params/*itemId, versionId, version, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var emptyTrashBin = function(params/*fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var extractVersion = function(params/*itemId, versionId, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var geolocate = function(params/*itemIds, long, lat, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var getAccount = function(params/*fn_callback*/){
	
	var displayName = Ti.App.Properties.getString('talk4u:displayUserName', null);
	
	var dataPropertiesParsed = {
		'displayName': (displayName)? displayName : L('no_conecction'),
	};
	
	params.fn_callback(dataPropertiesParsed);
	
};

var getEffectivePermissions = function(params/*itemId, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var getElement = function(params/*itemId, fn_callback*/){
	
	// CRUDO response callback
	function CRUDOResponse_callback(dataCRUDO){
		var dataCRUDOParsed = null;
		if (dataCRUDO && dataCRUDO.length > 0) {
			dataCRUDOParsed = [
				{
					'errorCode': null,
					'response': 
						{
							'id': dataCRUDO[0].id,
							'userRelatedInfo': {
								'revised': dataCRUDO[0].revised,
								'important': dataCRUDO[0].important,
								'followed': dataCRUDO[0].followed,
							},
							'size': dataCRUDO[0].size,
							'description': dataCRUDO[0].description,
							"typeSpecificProperties": [
						        {
						            "typeName": "String",
						            "name": "NombreCompleto",
						            "value": dataCRUDO[0].completeName,
						        },
						    ],
							'date': dataCRUDO[0].date,
							'auditInfo': {
								'deleted': dataCRUDO[0].deleted,
								'lastModificationDate': dataCRUDO[0].lastModificationDate,
							},
							'typeId': dataCRUDO[0].typeId,
							"origin": {
				                "text": dataCRUDO[0].name,
				            },
							'owner': {
								'ownerId': dataCRUDO[0].ownerId,
							},
							'location': {
								'long': dataCRUDO[0].longitude,
								'lat': dataCRUDO[0].latitude,
							},
						},
				},
			];
		}
		params.fn_callback(dataCRUDOParsed);
	};
	
	// CRUDO async call
	var statementParams = {
		'itemId': params.itemId,
		'fn_callback': CRUDOResponse_callback,
	};
	talk4u.service.CRUDOperation('getElement', statementParams);
	
};

var getFavouriteUsersGroups = function(params/*start, limit, sort, fn_callback*/){
	
	// // CRUDO response callback
	// function CRUDOResponse_callback(dataCRUDO){
		// if (dataCRUDO && dataCRUDO) {
			// params.fn_callback(data);
		// } else {
			// var dataCRUDOParsed = [
				// {'items': [], 'totalCount': 0, 'update': false},
				// {'items': [], 'totalCount': 0, 'update': false}
			// ];
			// params.fn_callback(dataCRUDOParsed);
		// }
	// };
	
	// CRUDO sync call
	var favouriteUsers = talk4u.service.CRUDOperation('getFavouriteUsers');
	var favouriteGroups = talk4u.service.CRUDOperation('getFavouriteGroups');
	var dataCRUDOParsed = [
		{
			'errorCode': null,
			'response': 
				{
					'items': favouriteUsers,
					'totalCount': (favouriteUsers.length > 0)? favouriteUsers.length : 0,
				}
		},
		{
			'errorCode': null,
			'response': 
				{
					'items': favouriteGroups,
					'totalCount': (favouriteGroups.length)? favouriteGroups.length : 0,
				}
		},
	];
	params.fn_callback(dataCRUDOParsed);
	
};

var getFilesAndFolders = function(params/*itemId, start, limit, sort, dir, filters, fn_callback*/){
	
	Ti.API.info('getFilesAndFolders OFFLINE');
	if (params.start !== -1 && typeof params.filters == "undefined") {
		// CRUDO response callback
		function CRUDOResponse_callback(dataCRUDO){
			if (dataCRUDO && dataCRUDO.length > 0) {
				var itemsTotalCount = talk4u.service.CRUDOperation('getFilesAndFoldersTotalCount', {itemId: params.itemId});
				var items = dataCRUDO.map(function(e){
					return {
						'id': e.id,
						'userRelatedInfo': {
							'revised': e.revised,
							'important': e.important,
							'followed': e.followed,
						},
						'size': e.size,
						'description': e.description,
						"typeSpecificProperties": [
					        {
					            "typeName": "String",
					            "name": "NombreCompleto",
					            "value": e.completeName,
					        },
					    ],
						'date': e.date,
						'auditInfo': {
							'deleted': e.deleted,
							'lastModificationDate': e.lastModificationDate,
						},
						'typeId': e.typeId,
						'owner': {
							'ownerId': e.ownerId,
						},
						'location': {
							'long': e.longitude,
							'lat': e.latitude,
						},
					};
				});
				var dataCRUDOParsed = [
					{
						'errorCode': null,
						'response':
							{
								'items': items,
								'totalCount': itemsTotalCount[0].totalCount,
								'update': (params.start = 0 && params.limit > glb.config.limit)? true : false,
							},
					},
				];
				params.fn_callback(dataCRUDOParsed);
			} else {
				// var itemsTotalCount = talk4u.service.CRUDOperation('getFilesAndFoldersTotalCount', {itemId: params.itemId});
				var dataCRUDOParsed = [
					{
						'errorCode': null,
						'response':
							{
								'items': [],
								'totalCount': -1/*itemsTotalCount[0].totalCount*/,
								'update': false,
							},
					},
				];
				params.fn_callback(dataCRUDOParsed);
			}
			// Ti.API.info('finished CRUDO (getFilesAndFolders): '+ timestamp());
		};
		
		// CRUDO call
		var statementParams = {
			'itemId': params.itemId,
			'start': params.start,
			'limit': params.limit,
			'sort': params.sort,
			'dir': params.dir,
			'filters': params.filters,
			'fn_callback': CRUDOResponse_callback
		};
		talk4u.service.CRUDOperation('getFilesAndFolders', statementParams);
	} else if (typeof params.filters != "undefined") {
		var dataCRUDO = [];
		dataCRUDO.push({
			'errorCode': 4,
			'response': false,
		});
		params.fn_callback(dataCRUDO);
	}
	
};

var getGroups = function(params/*start, limit, sort, fn_callback*/){
	
	if (params.start !== -1) {
		// CRUDO response callback
		function CRUDOResponse_callback(dataCRUDO){
			if (dataCRUDO && dataCRUDO.length > 0){
				var res = talk4u.service.CRUDOperation('getGroupsTotalCount');
				var items = dataCRUDO.map(function(e){
					return {
						'id': e.id,
						'email': e.email,
						'name': e.name,
						'favourite': e.favourite,
					};
				});
				var dataCRUDOParsed = [
					{
						'errorCode': null,
						'response':
							{
								'items': items,
								'totalCount': res[0].totalCount,
							}
					}
				];
				params.fn_callback(dataCRUDOParsed);
			} else {
				var res = talk4u.service.CRUDOperation('getGroupsTotalCount');
				var dataCRUDOParsed = [
					{
						'errorCode': null,
						'response':
							{
								'items': [],
								'totalCount': -1,
								'update': false,
							}
					}
				];
				params.fn_callback(dataCRUDOParsed);
			}
		};
		
		// CRUDO async call
		var statementParams = {
			'start': params.start,
			'limit': params.limit,
			'sort': params.sort,
			'fn_callback': CRUDOResponse_callback
		};
		talk4u.service.CRUDOperation('getGroups', statementParams);
	}
	
};

var getParentsAndChilds = function(params/*itemId, start, limit, sort, fn_callback*/){
	
	// CRUDO response callback
	function CRUDOResponse_callback(data){
		params.fn_callback(data);
	};
	
	// CRUDO call
	talk4u.service.CRUDOperation('getParentsAndChilds', params);
	
};

var getAllPermissions = function(params/*itemId, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var getPermissions = function(params/*itemId, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var getPermissionsForUser = function(params/*itemId, userOrGroupId, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var getUser = function(params/*userId, fn_callback*/){
	
	// CRUDO response callback
	function CRUDOResponse_callback(data){
		params.fn_callback(data);
	};
	
	// CRUDO call
	talk4u.service.CRUDOperation('getUser', params);
	
};

var getGroup = function(params/*groupId, fn_callback*/){
	
	// CRUDO response callback
	function CRUDOResponse_callback(data){
		params.fn_callback(data);
	};
	
	// CRUDO call
	talk4u.service.CRUDOperation('getGroup', params);
	
};

var getUserPermits = function(params/*itemId, text, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var getUsers = function(params/*start, limit, sort, fn_callback*/){
	
	if (params.start !== -1) {
		// CRUDO response callback
		function CRUDOResponse_callback(dataCRUDO){
			if (dataCRUDO && dataCRUDO.length > 0){
				var res = talk4u.service.CRUDOperation('getUsersTotalCount');
				var items = dataCRUDO.map(function(e){
					return {
						'id': e.id,
						'userName': e.userName,
						'email': e.email,
						'name': e.name,
						'favourite': e.favourite,
					};
				});
				var dataCRUDOParsed = [
					{
						'errorCode': null,
						'response':
							{
								'items': items,
								'totalCount': res[0].totalCount,
							}
					}
				];
				params.fn_callback(dataCRUDOParsed);
			} else {
				var res = talk4u.service.CRUDOperation('getUsersTotalCount');
				var dataCRUDOParsed = 
				[
					{
						'errorCode': null,
						'response':
							{
								'items': [],
								'totalCount': -1,
								'update': false,
							}
					}
				];
				params.fn_callback(dataCRUDOParsed);
			}
		};
		
		var statementParams = {
			'start': params.start,
			'limit': params.limit,
			'sort': params.sort,
			'fn_callback': CRUDOResponse_callback
		};
		talk4u.service.CRUDOperation('getUsers', statementParams);
	}
	
};

var getVersion = function(params/*itemId, versionId, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var getVersions = function(params/*itemId, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var home = function(params/*fn_callback*/){
	
	// Read data from the PROPERTIES
	var data = JSON.parse(Ti.App.Properties.getString('talk4u:offline:home'), null);
	if (data === null) {
		var dataParsed = [
			{
				'errorCode': 1,
				'response': null,
			}
		];
	}
	params.fn_callback(data);
	
};

var initialConfigData = function(params/*fn_callback*/){
	// Read data from the PROPERTIES
	var data = JSON.parse(Ti.App.Properties.getString('talk4u:offline:initialConfigData'), null);
	params.fn_callback(data);
};

var listElements=function(params/*listType, referenceId, start, limit, sort, dir, filters, propertyNames, fn_callback*/){

	if (params.listType === 'Favourites') {
		// CRUDO response callback
		function CRUDOResponse_callback(data){
			if (data){
				var res = talk4u.service.CRUDOperation('listElementsFavouritesTotalCount');
				var dataCRUDOParsed = [
					{
						'errorCode': null,
						'response':
							{
								'items': data.map(function(e){
									return {
										'id': e.id,
										'userRelatedInfo': {
											'revised': e.revised,
											'important': e.important,
											'followed': e.followed,
										},
										'size': e.size,
										'description': e.description,
										"typeSpecificProperties": [
									        {
									            "typeName": "String",
									            "name": "NombreCompleto",
									            "value": e.completeName,
									        },
									    ],
										'date': e.date,
										'auditInfo': {
											'deleted': e.deleted,
											'lastModificationDate': e.lastModificationDate,
										},
										'typeId': e.typeId,
										"origin": {
							                "text": e.name,
							            },
										'owner': {
											'ownerId': e.ownerId,
										},
										'location': {
											'long': e.longitude,
											'lat': e.latitude,
										},
									};
								}),
								'totalCount': res[0].totalCount,
							}
					}
				];
				params.fn_callback(dataCRUDOParsed);
			} else {
				var dataCRUDOParsed = [
					{
						'errorCode': null,
						'response':
							{
								'items': [],
								'totalCount': -1,
							}
					}
				];
				params.fn_callback(dataCRUDOParsed);
			}
			
		};
		
		// CRUDO call
		var statementParams = {
			'listType': params.listType,
			'levels': params.levels,
			'start': params.start,
			'limit': params.limit,
			'sort': params.sort,
			'dir': params.dir,
			'filters': params.filters,
			'propertyNames': params.propertyNames,
			'referenceId': params.referenceId,
			'fn_callback': CRUDOResponse_callback
		}
		talk4u.service.CRUDOperation('listElements', statementParams);
	} else {
		var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	}
	
};

var login = function(params/*userName, password, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var logout = function(params/*fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var move = function(params/*itemIds, fromFolder, toFolder, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var notificationsCallback = function(params/*fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var notificationsInitialization = function(params/*fn_callback*/){

	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var notificationsUnsubscribe = function(params/*fn_callback*/){

	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var photo = function(params/*itemId, localFilePath, fn_callback*/){
	
	return null;
	
};

var restore = function(params/*itemIds, recursive, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var search = function(params/*text, start, limit, sort, dir, filters, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var searchGroup = function(params/*searchText, start, limit, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var searchUser = function(params/*searchText, start, limit, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var send = function(params/*itemIds, userOrGroupIds, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var setFavourite = function(params/*itemIds, favourites, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var setRevised = function(params/*itemId, revised, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var share = function(params/*itemId, share, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var trash = function(params/*itemIds, recursive, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var updateAccount = function(params/*account, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var updatePermissions = function(params/*itemId, permissions, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var updatePermissionsForUserGroup = function(params/*itemId, userOrGroupId, userGroupPermissions, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var updateVersion = function(params/*itemId, versionId, version, fn_callback*/){
	
	var dataCRUDO = [];
	dataCRUDO.push({
		'errorCode': 4,
		'response': false,
	});
	params.fn_callback(dataCRUDO);
	
};

var uploadFiles = function(params/*itemId, fileName, file, progBar, fn_callback*/){
	
	// NO OPERATIONS for pending state
	
};