var login = function(params/*userName, password, fn_callback*/) {

	// webservice response callback
	function xhrResponse_callback(dataXHR) {
		params.fn_callback(dataXHR);
	};

	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/login');
	xhr.login(params.userName, params.password, xhrResponse_callback);

};
var getChats = function(params) {
	// webservice response callback
	function xhrResponse_callback(dataXHR) {
		params.fn_callback(dataXHR);
	};

	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/getChats');
	var param = null;
	xhr.getChats(param, xhrResponse_callback);
};
var getChat = function(params,fn_callback) {
	// webservice response callback
	function xhrResponse_callback(dataXHR) {
		fn_callback(dataXHR);
	};

	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/getChat');
	xhr.getChat(params, xhrResponse_callback);
};
var advancedSearch = function(params) {

	// webservice response callback
	function xhrResponse_callback(dataXHR) {
		params.fn_callback(dataXHR);
	};

	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/advancedSearch');
	// xhr.advancedSearch(params.searchText, params.start, params.limit, params.sort, params.dir, params.filters, params.from, params.to, params.field, params.types, params.searchInVersions, params.searchInBody, xhrResponse_callback);
	xhr.advancedSearch(params, xhrResponse_callback);

};
var getAccount = function(params/*fn_callback*/) {

	// webservice response callback
	function xhrResponse_callback(dataXHR) {
		var dataParsed = null;
		if (dataXHR[0].response) {
			Ti.App.Properties.setString('talk4u:displayUserName', dataXHR[0].response.displayName);
			dataParsed = dataXHR;
		} else {
			var displayName = Ti.App.Properties.getString('talk4u:displayUserName', null);

			var dataParsed = {
				'displayName' : (displayName) ? displayName : '',
			};
		}
		params.fn_callback(dataParsed);
	};

	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/getAccount');
	xhr.getAccount(xhrResponse_callback);

};
var getFavouriteUsersGroups = function(params/*start, limit, sort, fn_callback*/) {

	var xhrReceivedOK = null;

	//if (params.start === 0 || params.start === -1) {
	// webservice response callback
	function xhrResponse_callback(dataXHR) {/*
		 if (dataXHR[0].response){
		 xhrReceived = true;
		 params.fn_callback(dataXHR);
		 if (dataXHR[0].response.totalCount > 0) {
		 var users = dataXHR[0].response.items.map(function(e) {
		 return {
		 'id': e.id,
		 'userName': e.userName,
		 'email': e.email,
		 'name': e.name,
		 'favourite': e.favourite,
		 }
		 });
		 talk4u.service.CRUDOperation('createUsers', users);
		 }
		 } else{
		 xhrReceivedOK = false;
		 if (crudoReceivedOK === false)
		 params.fn_callback(null);
		 }*/
		params.fn_callback(dataXHR);
	};

	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/getFavouriteUsersGroups');
	xhr.getFavouriteUsersGroups(params, xhrResponse_callback);
	//}

};
var getAllUsers = function(params/*start, limit, sort, fn_callback*/) {

	var xhrReceivedOK = null;

	//if (params.start === 0 || params.start === -1) {
	// webservice response callback
	function xhrResponse_callback(dataXHR) {/*
		 if (dataXHR[0].response){
		 xhrReceived = true;
		 params.fn_callback(dataXHR);
		 if (dataXHR[0].response.totalCount > 0) {
		 var users = dataXHR[0].response.items.map(function(e) {
		 return {
		 'id': e.id,
		 'userName': e.userName,
		 'email': e.email,
		 'name': e.name,
		 'favourite': e.favourite,
		 }
		 });
		 talk4u.service.CRUDOperation('createUsers', users);
		 }
		 } else{
		 xhrReceivedOK = false;
		 if (crudoReceivedOK === false)
		 params.fn_callback(null);
		 }*/
		params.fn_callback(dataXHR);
	};

	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/getAllUsers');
	xhr.getAllUsers(params, xhrResponse_callback);
	//}

};
var getOnlineUsers = function(params/*start, limit, sort, fn_callback*/) {

	var xhrReceivedOK = null;

	//if (params.start === 0 || params.start === -1) {
	// webservice response callback
	function xhrResponse_callback(dataXHR) {/*
		 if (dataXHR[0].response){
		 xhrReceived = true;
		 params.fn_callback(dataXHR);
		 if (dataXHR[0].response.totalCount > 0) {
		 var users = dataXHR[0].response.items.map(function(e) {
		 return {
		 'id': e.id,
		 'userName': e.userName,
		 'email': e.email,
		 'name': e.name,
		 'favourite': e.favourite,
		 }
		 });
		 talk4u.service.CRUDOperation('createUsers', users);
		 }
		 } else{
		 xhrReceivedOK = false;
		 if (crudoReceivedOK === false)
		 params.fn_callback(null);
		 }*/
		params.fn_callback(dataXHR);
	};

	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/getOnlineUsers');
	xhr.getOnlineUsers(null, xhrResponse_callback);
	//}

};
var getUser = function(params) {
	// webservice response callback
	function xhrResponse_callback(dataXHR) {
		params.fn_callback(dataXHR);
	};
	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/getUser');
	xhr.getUser(params.id, xhrResponse_callback);
};
var getUserChats = function(params) {
	// webservice response callback
	function xhrResponse_callback(dataXHR) {
		params.fn_callback(dataXHR);
	};
	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/getUserChats');
	xhr.getUserChats(params, xhrResponse_callback);
}
var home = function(params/*fn_callback*/) {

	// HOME FORM DATA
	// webservice response callback
	function xhrResponse_callback(dataXHR) {
		if (dataXHR[0].errorCode === null && dataXHR[0].response && dataXHR[1].errorCode === null && dataXHR[1].response/*&& dataXHR[2].response === null*/) {
			params.fn_callback(dataXHR);
			// Write data to the PROPERTIES
			Ti.App.Properties.setString('talk4u:offline:home', JSON.stringify(dataXHR));
		} else {
			// Read data from the PROPERTIES
			var dataParsed = JSON.parse(Ti.App.Properties.getString('talk4u:offline:home'), dataXHR);
			params.fn_callback(dataParsed);
		}
	};
	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/home');
	xhr.home(xhrResponse_callback);

};
var initialConfigData = function(params/*fn_callback*/) {

	// HOME FORM DATA
	// webservice response callback
	function xhrResponse_callback(dataXHR) {
		if (dataXHR) {
			params.fn_callback(dataXHR);
			// Write data to the PROPERTIES
			Ti.App.Properties.setString('talk4u:offline:initialConfigData', JSON.stringify(dataXHR));
		} else {
			// Read data from the PROPERTIES
			var data = JSON.parse(Ti.App.Properties.getString('talk4u:offline:initialConfigData'));
			params.fn_callback(data);
		}
	};
	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/initialConfigData');
	xhr.initialConfigData(xhrResponse_callback);
};
var logout = function(params/*fn_callback*/) {

	// webservice response callback
	function xhrResponse_callback(dataXHR) {
		params.fn_callback(dataXHR);
	};

	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/logout');
	xhr.logout(xhrResponse_callback);

};
var photo = function(params/*itemId, localFilePath, fn_callback*/) {

	// webservice response callback
	function xhrResponse_callback(dataXHR) {
		params.fn_callback(dataXHR);
	};

	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/photo');
	xhr.photo(params.itemId, params.width, params.height, params.localFilePath, xhrResponse_callback);

};
var search = function(params/*text, start, limit, sort, dir, filters, fn_callback*/) {

	// webservice response callback
	function xhrResponse_callback(dataXHR) {
		params.fn_callback(dataXHR);
	};

	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/search');
	xhr.search(params.searchText, params.start, params.limit, params.sort, params.dir, params.filters, xhrResponse_callback);

};
var searchGroup = function(params/*searchText, start, limit, fn_callback*/) {

	// webservice response callback
	function xhrResponse_callback(dataXHR) {
		params.fn_callback(dataXHR);
	};

	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/searchGroup');
	xhr.searchGroup(params.searchText, params.start, params.limit, xhrResponse_callback);

};
var searchUser = function(params/*searchText, start, limit, fn_callback*/) {

	// webservice response callback
	function xhrResponse_callback(dataXHR) {
		params.fn_callback(dataXHR);
	};

	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/searchUser');
	xhr.searchUser(params.searchText, params.start, params.limit, xhrResponse_callback);

};
var updateAccount = function(params/*account, fn_callback*/) {

	// webservice response callback
	function xhrResponse_callback(dataXHR) {
		params.fn_callback(dataXHR);
	};

	// webservice async call
	var xhr = require('classes/service/talk4u/xhr/updateAccount');
	xhr.updateAccount(params.account, xhrResponse_callback);

};
