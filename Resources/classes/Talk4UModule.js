// ERROR CODES:
// 1: xhr general error
// 2: timeout error on xhr multiple calls
// 3: parse error when parsing the xhr response
// 4: Internet connection required

var I4UModule = require('classes/I4UModule');
function Talk4UModule() {
	//I4UModule.apply(this);
}

Talk4UModule.prototype = new I4UModule();
Talk4UModule.prototype.loginSubmit = function(userName, password) {

	function service_callback(data) {
		if (data[0].response) {
			glb.Talk4ULogin.close();
			glb.Talk4ULogin = null;
			talk4u.startApp();
		} else if (data[0].response === false) {
			// No connection Error
			var connectionDialog = Ti.UI.createAlertDialog({
				title : L('attention'),
				message : L('connection_error'),
				ok : L('accept'),
			}).show();
		} else {
			// Login error
			Ti.App.Properties.removeProperty('talk4u:displayName');
			Ti.App.Properties.removeProperty('talk4u:password');

			var loginErrorAlert = Ti.UI.createAlertDialog({
				title : L('error'),
				message : L('loginError'),
			}).show();
		}
	};

	Ti.App.Properties.setString('talk4u:displayName', userName);
	Ti.App.Properties.setString('talk4u:password', password);

	talk4u.service.execService('login', {
		'userName' : userName,
		'password' : password,
		'fn_callback' : service_callback
	});

};
Talk4UModule.prototype.getState = function() {
	//Ti.App.Properties.getString('talk4u:estado');
	return 1;
}


Talk4UModule.prototype.getChats = function(form) {

	function service_callback(data) {
		var dataParsed = null;
		if (data[0].errorCode === null) {
			dataParsed = data[0].response;
		} else if (data[0].errorCode === 4) {
			dataParsed = false;
		} else if (data[0].errorCode === 401) {
			talk4u.module.logoutSubmit();
			return;
		}
		var chats = {};
		chats.items = [];
		var _chat = require("classes/models/chat");
		for (var i = 0; i < data.items.length; i++) {
			chats.items[i] = new _chat(data.items[i]);
			talk4u.chatList.addChat(chats.items[i]);
		}
		form.fireEvent('talk4uchatsform:getData', {
			'data' : chats
		});
	};
	talk4u.service.execService('getChats', {
		'fn_callback' : service_callback
	});
};


Talk4UModule.prototype.getChat = function(form) {

	function service_callback(data) {
		var dataParsed = null;
		if (data[0].errorCode === null) {
			var _chat = require("classes/models/chat");
			var dataParsed = new _chat(data[0].response);
		} else if (data[0].errorCode === 4) {
			dataParsed = false;
		} else if (data[0].errorCode === 401) {
			talk4u.module.logoutSubmit();
			return;
		}
		form.fireEvent('talk4uchatform:getData', {
			'data' : dataParsed
		});
	}


	talk4u.service.execService('getChat', {
		'id' : form.chat,
		'fn_callback' : service_callback
	});

};
Talk4UModule.prototype.getAllUsers = function(params) {

	//get the data for the form
	function service_callback(data) {
		var dataParsed = null;
		if (data[0].errorCode === null) {
			dataParsed = data[0].response;
		} else if (data[0].errorCode === 4) {
			dataParsed = false;
		} else if (data[0].errorCode === 401) {
			talk4u.module.logoutSubmit();
			return;
		}
		params.form.fireEvent('talk4uusersform:getData', {
			'data' : dataParsed
		});
	};

	talk4u.service.execService('getAllUsers', {
		'start' : (params) ? params.start : 0,
		'limit' : (params) ? params.limit : 100,
		'sort' : (params) ? params.sort : "",
		'fn_callback' : service_callback
	});

	return data;
};
Talk4UModule.prototype.getOnlineUsers = function(form) {

	function service_callback(data) {
		/*var dataParsed = null;
		 if (data[0].errorCode === null) {
		 dataParsed = data[0].response;
		 } else if (data[0].errorCode === 4) {
		 dataParsed = false;
		 } else if (data[0].errorCode === 401) {
		 talk4u.module.logoutSubmit();
		 return;
		 }*/

		form.fireEvent('talk4uusersform:getData', {
			'data' : data
		});
	};

	talk4u.service.execService('getOnlineUsers', {
		'fn_callback' : service_callback
	});

};
Talk4UModule.prototype.getFavouritesUsers = function(params) {

	//get the data for the form
	function service_callback(data) {
		var dataParsed = null;
		if (data[0].errorCode === null) {
			dataParsed = data[0].response;
		} else if (data[0].errorCode === 4) {
			dataParsed = false;
		} else if (data[0].errorCode === 401) {
			talk4u.module.logoutSubmit();
			return;
		}
		params.form.fireEvent('talk4uusersform:getData', {
			'data' : dataParsed
		});
	};

	talk4u.service.execService('getFavouriteUsersGroups', {
		'start' : (params) ? params.start : 0,
		'limit' : (params) ? params.limit : 100,
		'sort' : (params) ? params.sort : "",
		'fn_callback' : service_callback
	});

	return data;
};
Talk4UModule.prototype.getUser = function(params) {
	var that = this;
	//get the data for the form
	function service_callback(data) {

		var _user = require("classes/models/user");
		var usuario = new _user(data[0].response);
		params.form.fireEvent("talk4uuserform:getData", {
			'datos' : usuario
		});

	};
	talk4u.service.execService('getUser', {
		'id' : params.params.itemId,
		'fn_callback' : service_callback
	});

};
Talk4UModule.prototype.getUserChats = function(params) {

	//get the data for the form
	function service_callback(data) {
		var dataParsed = null;
		if (data[0].errorCode === null) {
			dataParsed = data[0].response;
		} else if (data[0].errorCode === 4) {
			dataParsed = false;
		} else if (data[0].errorCode === 401) {
			talk4u.module.logoutSubmit();
			return;
		}

		params.sectionChats.fireEvent('downloaded', {
			'data' : dataParsed
		});
	};

	talk4u.service.execService('getUserChats', {
		'id' : params.id,
		'start' : params.start,
		'limit' : params.limit,
		'sort' : params.sort,
		'fn_callback' : service_callback
	});
}

Talk4UModule.prototype.getAccount = function(params) {
	var data = [];

	data = [{}]

	//get the data for the form
	/*
	function service_callback(data){
	var dataParsed = null;
	if (data[0].errorCode === null) {
	dataParsed = data[0].response;
	} else if (data[0].errorCode === 4) {
	dataParsed = false;
	} else if (data[0].errorCode === 401) {
	talk4u.module.logoutSubmit();
	return;
	}
	form.fireEvent('Box4UAccountForm:getData',{ 'data' : dataParsed });
	};
	*/

	// talk4u.service.execService('getAccount', {
	// 'fn_callback': service_callback
	// });
};
Talk4UModule.prototype.logoutSubmit = function() {
	// Delete talk4u database
	//var db = Ti.Database.install(glb.config.dbpath, glb.config.dbname);
	//var dbFile = db.getFile();
	//db.close();
	//dbFile.deleteFile();
	// Delete talk4u cache folder
	//var talk4uDir = Ti.Filesystem.getFile(glb.config.talk4u_path);
	//if (talk4uDir.exists()){
	//	talk4uDir.deleteDirectory(true);
	//}
	// Delete talk4u properties
	var properties = Titanium.App.Properties.listProperties();
	for (var i = 0, len = properties.length; i < len; i++) {
		if (properties[i].match('talk4u:'))
			//trace(properties[i]);
			Ti.App.Properties.removeProperty(properties[i]);
	}
	if (glb.tabgroup) {
		glb.tabgroup.close();
		glb.tabgroup = null;
	}
	glb.mainWindow.close();
	glb.mainWindow = null;
	talk4u.startApp();
};
/**
 * Download the photo of the user or group
 * @param Object obj // obj with the information for preview
 */
Talk4UModule.prototype.photo = function(obj) {
	//get the data for the form
	function service_callback(data) {
		if (data[0].errorCode === null && data[0].response) {
			// set the file downloaded
			obj.fireEvent('downloaded', {
				'localFilePath' : data[0].response.localFilePath
			});
		} else if (data[0].errorCode === 401) {
			talk4u.module.loginoutSubmit();
			return;
		} else {
			var file = Ti.Filesystem.getFile(filesPath + Ti.Filesystem.separator + fileName);
			if (file.exists) {
				file.deleteFile();
				file = null;
			}
		}
	};

	// Set photo size
	var width = (obj.itemType === 'user' || obj.itemType === 'group') ? Math.round(_P(glb.config.thumbImageSize)) : Math.round(_P(glb.config.photoSize));
	var height = width;
	// Check what files we have to really download
	var talk4uDir = Ti.Filesystem.getFile(glb.config.talk4u_path);
	if (!talk4uDir.exists()) {
		talk4uDir.createDirectory();
	}
	if (glb.device === 'iphone' || glb.device === 'ipad') {
		talk4uDir.remoteBackup = false;
	}
	var filesPath = (obj.type === 'photo') ? glb.config.preview_path : glb.config.list_path;
	var filesDir = Ti.Filesystem.getFile(filesPath);
	if (!filesDir.exists()) {
		filesDir.createDirectory();
	}
	var fileName = obj.itemId + glb.config.photoExtension;
	var dirList = filesDir.getDirectoryListing();
	var finded = false;
	for (var local_index in dirList) {
		if (dirList[local_index] === fileName) {
			finded = true;
			if (obj.photoId) {
				// use downloaded file in obj
				obj.fireEvent('downloaded', {
					'localFilePath' : filesPath + Ti.Filesystem.separator + fileName
				});
			} else {
				var fileToDelete = Ti.Filesystem.getFile(filesPath + Ti.Filesystem.separator + fileName);
				fileToDelete.deleteFile();
			}
			break;
		}
	}

	// request the file
	if (!finded && obj.photoId) {
		talk4u.service.execService('photo', {
			itemId : obj.itemId,
			width : width,
			height : height,
			localFilePath : filesPath + Ti.Filesystem.separator + fileName,
			fn_callback : service_callback
		});
	}
};

Talk4UModule.prototype.initialiceXMPPevents = function(conn) {
	//trace("initalice message handler","INFO");
	function onMessage(message) {
		if (message.getChild("body") !== null) {
			trace("from: " + message.getAttribute("from") + " message: " + message.getChild("body").getText(), "info")
			talk4u.chatList.newMessage(message.getAttribute("from"), message.getAttribute("to"), message.getChild("body").getText());
		}
	}


	conn.addHandler(onMessage, null, 'message', null, null, null);

}

module.exports = Talk4UModule;

