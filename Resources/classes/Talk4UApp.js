var I4UApp = require('classes/I4UApp');
function Talk4UApp() {
	I4UApp.apply(this);
	var Talk4UService = require('classes/Talk4UService');
	this.service = new Talk4UService();
	var Talk4UModule = require('classes/Talk4UModule');
	this.module = new Talk4UModule();
	var Talk4UFormFactory = require('classes/Talk4UFormFactory');
	this.formFactory = new Talk4UFormFactory();
	var _chatList = require('classes/models/chatList')
	this.chatList= new _chatList(); // chats[nombreDelChat@muc.adminuser].msjs
	
	
};
Talk4UApp.prototype = new I4UApp();
Talk4UApp.prototype.startApp = function() {
	var that = this;
	Ti.App.Properties.setString('talk4u:displayName', "administrador");
	Ti.App.Properties.setString('talk4u:password', "123456");

	var userName = Ti.App.Properties.getString('talk4u:displayName', null);
	var password = Ti.App.Properties.getString('talk4u:password', null);

	if (userName && password) {
		//this.module.appInitialization();

		//open xmpp conection
		conn = new xmpp.Connection(glb.config.xmppServerPath,"5222");
		xmppservice = null;
		//TODO: sustituir por ip

		jid = userName+"@"+glb.config.xmppDomain;
		var passxmpp = "administrador";
		conn.connect(jid, passxmpp, function(status, condition) {
			if (status == xmpp.Status.CONNECTED) {
				//CREACIÓN DEL RECURSO TALK4U
				/*
				 <iq type="set" id="83">
				 <bind xmlns="urn:ietf:params:xml:ns:xmpp-bind">
				 <resource>Gajim</resource>
				 </bind>
				 </iq>
				 */
				var iqResource = xmpp.iq({
					type : 'set'
				}).c("bind", {
					xmlns : "urn:ietf:params:xml:ns:xmpp-bind"
				}).c("resource").t(glb.config.xmppSource);
				
				var iq_Callback = function(reply) {
					// open Home window
					glb.mainWindow = that.formFactory.createForm('Talk4UMainForm');
					glb.mainWindow.open();
					xmppservice = new _xmppservice();
					// var _ventanaChat = require("/ventanaChat");
					// var ventanaChat = new _ventanaChat(conn, xmpp);
					// ventanaChat.open();
				}
				conn.sendIQ(iqResource, iq_Callback, function(){
					alert("iq no recibido")
				});

				//DECIR QUE ESTOY CONECTADO AL SERVICIO
				/*
				 <presence xmlns="jabber:client" id="94">
				 <priority>50</priority>
				 */
				var presenceONLINE = xmpp.presence({
					xmlns : 'jabber:client'
				}).c("priority").t('50');
				conn.send(presenceONLINE);

			} else {
				conn.log(xmpp.LogLevel.DEBUG, "New connection status: " + status + ( condition ? (" (" + condition + ")") : ""));
			}

		});
		this.module.initialiceXMPPevents(conn);
		// execute app initialization functions
		//this.module.backgroundInitialization();

	} else {
		glb.Talk4ULogin = this.formFactory.createForm('Talk4ULoginForm');
		this.formFactory.openForm(glb.Talk4ULogin);
	}
	return;
};
 module.exports = Talk4UApp;

