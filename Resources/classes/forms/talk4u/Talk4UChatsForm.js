/*
 * crea la ventana principal de la pestaña Talk4U
 *
 */
var Talk4UForm = require('classes/Talk4UForm');
function Talk4UChatsForm(params) {
	var _this = this;
	var data = [];
	var start = 0;
	var totalCount;
	var updating = true;
	var moreRow = null;
	var params = data;

	function refreshTimeChats() {
		for (index in listaChats) {
			setRowChat(index);
		}
		setTimeout(refreshTimeChats, 1000);
	}

	//obtiene la posición de la conversación por la propiedad xmppjid de la row dentro del tableView
	//devuelve -1 si no existe en la tableView
	function indexXmppJid(xmppjid) {
		var i = 0;
		var esta = false;
		while (!esta && i < tableView.data[0].rows.length) {
			if (tableView.data[0].rows[i].xmppjid === xmppjid) {
				esta = true;
			} else {
				i++;
			}
		}
		return (esta) ? i : -1;
		//no existe
	}

	//modificar una row
	function setRowChat(from) {
		if ( typeof listaChats[from] !== "undefined") {
			var l = listaChats[from].messages.length - 1;
			var row = _this.createChatsTableViewRow({
				topLeftText : getUserFromJid(from),
				topRightText : moment(listaChats[from].dateLastMessage, "YYYYMMDDhhmmss").fromNow(),
				bottomRightText : (listaChats[from].nMessagesNotRead === 0) ? null : listaChats[from].nMessagesNotRead,
				bottomLeftText : listaChats[from].messages[l].content,
				leftImage : "images/talk4u/fakePNG/user.png",
				hasChild : true,
				itemId : 1,
				itemType : 'user',
				xmppjid : from,
			});
			if ( typeof tableView.data[0] !== "undefined") {
				var index = indexXmppJid(from);
				if (index < 0) {
					tableView.insertRowBefore(0, row);
				} else {
					tableView.updateRow(index, row);
				}
			} else {
				tableView.data = [row]
			}
		}
	}

	//create a new chat and add to tableVieww
	function append_chat(message) {
		var from = message.getAttribute("from");
		if ( typeof listaChats[from] !== "undefined") {//ha llegado algun mensaje de ese from
			//por eso existe en la lista de chats messages >0
			var row = _this.createChatsTableViewRow({
				topLeftText : getUserFromJid(from),
				topRightText : moment(listaChats[from].dateLastMessage, "YYYYMMDDhhmmss").fromNow(),
				bottomRightText : (listaChats[from].nMessagesNotRead === 0) ? null : listaChats[from].nMessagesNotRead,
				bottomLeftText : message.getChild("body").getText(),
				leftImage : "images/talk4u/fakePNG/user.png",
				hasChild : true,
				itemId : 1,
				itemType : 'user',
				xmppjid : from,
			});
			if (tableView.data.length === 0) {//no hay ninguna
				tableView.data = [row]
			} else {
				var index = indexXmppJid(from);
				if (index < 0)//no existe en la ventana chats
					tableView.insertRowBefore(0, row);
				else {//existe en la ventana de chats
					if (index === 0) {//depende que sea el primero o no se sustituye de una forma u otra
						tableView.insertRowBefore(0, row);
						tableView.deleteRow(1);
					} else {
						tableView.deleteRow(index);
						tableView.insertRowBefore(0, row);
					}
				}
			}
		}
	}

	function getData(callback) {
		var that_talk4u = talk4u;
		var newData = [];

		for (var i = 0; i < callback.data.items.length; i++) {
			var row = _this.createChatsTableViewRow({
				topLeftText : callback.data.items[i].topic,
				topRightText : dateWithoutHours(callback.data.items[i].lastActive),
				bottomLeftText : callback.data.items[i].created,
				leftImage : "images/talk4u/fakePNG/user.png",
				hasChild : true,
				itemId : callback.data.items[i].id,
				itemType : 'user',
				xmppjid : callback.data.items[i].xmppJid,
				participants : callback.data.items[i].participants
			});
			newData.push(row);
		};
		tableView.data = newData;
	}

	function fn_open() {
		editButton.setEnabled(true);
		data = listaChats;
		//talk4u.module.getChats(self);
		trace("Add handle on message from new chat")
		conn.addHandler(append_chat, null, 'message', null, null, null);
		refreshTimeChats();
	}

	function fn_windowClose() {
		if (self.openStyle === 'inNavGroup')
			glb.navGroup[glb.navGroup.length - 1].close(self);
		else if (self.openStyle === 'inTab')
			glb.tabgroup.activeTab.close(self);
		else if (self.openStyle === 'inDetail')
			glb.mainWindow.detailView.fn_close();
		else
			self.close();
	}

	function fn_close(form) {
		// Delete from navigation
		glb.contextsNavigation.pop();
		// If still have any changedItem it is because we have trashed a previous navigated folder
		var changedItemPos = glb.changedItemIds.indexOf(getItemId());
		if (changedItemPos === -1 /*&& glb.contextsNavigation.length > 0*/)
			//params.fn_parentActionResponse(true, 'refresh');
			if (tableView.editionMode)
				glb.tabgroup.animate({
					bottom : 0,
					duration : 0
				});
		self.removeEventListener('open', fn_open);
		self.removeEventListener('getData', getData);
		self.removeEventListener('close', fn_close);
		self = null;
		params.bgTasksWindow.close();
		params.bgTasksWindow = null;
	}

	// private function to edit current window
	function editSelf() {
		editButton.setEnabled(false);
		search.left = 0;
		if (self.openStyle === 'inTab')
			glb.tabgroup.animate({
				bottom : -50,
				duration : 500
			});
		editButton.setTitle(L('cancel'));
		self.add(toolBar);
		var unBlock = [editButton];
		tableView.fireEvent('TableView:editSelf', {
			unBlock : unBlock,
			toolBar : toolBar
		});
	};
	// private function to cancel edit
	function editSelfCancel() {
		editButton.setEnabled(false);
		search.left = 30;
		editButton.setTitle(L('edit'));
		self.remove(toolBar);
		if (self.openStyle === 'inTab')
			glb.tabgroup.animate({
				bottom : 0,
				duration : 500
			});
		var unBlock = [editButton];
		tableView.fireEvent('TableView:editSelfCancel', {
			unBlock : unBlock,
			tableViewData : data,
			toolBar : toolBar
		});
	};
	function getParentId() {
		return params.parentId;
	}

	function getItemId() {
		return params.itemId;
	}

	function getItemsChecked() {
		var itemIds = [];
		for (var i = 0, len = data.length; i < len; i++) {
			if (data[i].isChecked)
				itemIds.push(data[i].itemId);
		}
		return itemIds;
	}

	function getItemObjectChecked() {
		var items = [];
		for (var i = 0, len = data.length; i < len; i++) {
			if (data[i].isChecked)
				items.push({
					'itemId' : data[i].itemId,
					'itemName' : data[i].itemName,
					'pos' : i
				});
		}
		return items;
	}

	function getItemCheckedLocation() {
		var location = {};
		for (var i = 0, len = data.length; i < len; i++) {
			if (data[i].isChecked) {
				location = data[i].location;
				break;
			}
		}
		return location;
	}

	function isInFront() {
		if (glb.contextsNavigation.length > 0 && glb.contextsNavigation[glb.contextsNavigation.length - 1].itemId === getItemId())
			return true;
		else
			return false;
	}

	// CREATE NAVBAR BUTTONS
	var editButton = this.createButton({
		title : L('edit')
	});
	editButton.addEventListener('click', function() {
		if (this.getTitle() === L('edit')) {
			editSelf();
		} else {
			editSelfCancel();
		}
	});
	editButton.setEnabled(true);
	var newChatButton = this.createButton({
		title : "Nuevo chat"
	});
	newChatButton.addEventListener('click', function() {
		var form = talk4u.formFactory.createForm('Talk4UNewChatUsersForm', {
			openStyle : (glb.device==='iphone')?'modal':'inDetail',

		});
		talk4u.formFactory.openForm(form);
	});
	newChatButton.setEnabled(true);

	// CREATE WINDOW
	var self = this.createWindow({
		title : 'Chats',
		rightNavButton : editButton,
		leftNavButton : newChatButton
	});
	self.visible = true;
	self.fn_close = fn_close;
	self.addEventListener('open', fn_open);
	self.addEventListener('talk4uchatsform:getData', getData);
	self.addEventListener('close', fn_close);

	// CREATE VIEW

	var tableView = this.createTableView({
		top : 0,
		editionMode : false,
		dinamicEdit : true
	});

	tableView.addEventListener('click', function(e) {
		var form = talk4u.formFactory.createForm('Talk4UChatForm', {
			openStyle : 'modal',
			xmppjid : e.row.xmppjid,
		});
		var l = listaChats[e.row.xmppjid].messages.length - 1;
		listaChats[e.row.xmppjid].nMessagesNotRead=0;
		setRowChat(e.row.xmppjid);
		talk4u.formFactory.openForm(form);
	});
	self.add(tableView);

	// CREATE TOOLBAR

	var toolBar = this.createToolBar({
		items : [],
		winOpenStyle : self.openStyle
	});

	return self;
}

Talk4UChatsForm.prototype = new Talk4UForm();
module.exports = Talk4UChatsForm;
