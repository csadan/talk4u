/*
 * crea la ventana que permite buscar elementos por su nombre y además muestra el histórico de búsquedas
 * 
 */
var Talk4UForm = require('classes/Talk4UForm');
var Talk4USearchForm = function(params) {
	var _this=this;
	var numRecentSearches = 0;
	typesRow = null;
	date_typeRow = null;
	searchInBodyRow = null;
	searchInVersionRow = null;
	date_fromRow = null;
	function fn_open(){
		if(typeof fn_open_extra === 'function') fn_open_extra();
	}
	function fn_close(){
		if(typeof fn_close_extra === 'function') fn_close_extra();
		self.removeEventListener('open',fn_open);
		self.removeEventListener('close',fn_close);
		self=null;
	}
	function initializeSearchParams(){
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
        var curHour = today.getHours() > 12 ? today.getHours() - 12 : (today.getHours() < 10 ? "0" + today.getHours() : today.getHours());
        var curMinute = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
        var curSeconds = today.getSeconds() < 10 ? "0" + today.getSeconds() : today.getSeconds();

		var current_date = mm + '/' + dd + '/' + yyyy + ' ' + curHour + ':' + curMinute + ':' + curSeconds;
		glb.searchParams = {
			searchText: '',
			limit: glb.config.limit,
			sort: 'date',
			dir: glb.config.sortType,
			filters: null,
			propertyNames: null,
			from: '',
			to: '',
			field: 'Fecha',
			Idsorigin: null,
			typeIds: new Array(),
			searchInVersions: false,
			searchInBody: false,
			advancedSearch: false,
			dateOfSearch: new Date()
		};
		
		typesRow = _this.createSearchTableViewRow({ win: self, title: L('types'), hasChild: true, itemType: 'types', itemId: 'typeIds', editable: true });
		date_typeRow = _this.createSearchTableViewRow({ win: self, title: L('date_type'), rightText: glb.searchParams.field, hasChild: true, itemType: 'radio_list', itemId: 'field_types', editable: true });
		searchInBodyRow = _this.createSearchTableViewRow({ win: self, title: L('Nombre usuario'), value: glb.searchParams.searchInBody, hasChild: false, selectionStyle: 'none', itemId: 'searchInBody', itemValue: glb.searchParams.searchInBody, switchValue: glb.searchParams.searchInBody });
		searchInVersionRow = _this.createSearchTableViewRow({ win: self, title: L('JID'), value: glb.searchParams.searchInVersions, hasChild: false, selectionStyle: 'none', itemId: 'searchInVersions', itemValue: glb.searchParams.searchInVersions, switchValue: glb.searchParams.searchInVersions });
		date_fromRow = _this.createText22TableViewRow({ win: self, topLeftText : L('Desde'), bottomLeftText : L('hasta'), topRightText: glb.searchParams.from, bottomRightText : glb.searchParams.to, itemType:'dates', hasChild:true, editable:true });
	}
	
	var populateTableView=function(){
		tableView.setData();
		
		var recentSearches = JSON.parse(Ti.App.Properties.getString('talk4u:recentSearches'));
		if(recentSearches && recentSearches.length > 0){
			var sectionRecents = Ti.UI.createTableViewSection({
				headerTitle : L('Última busqueda')
			});
			for(var i in recentSearches){
				var row = _this.createSearchTableViewRow({ win: self, title: recentSearches[i].searchText, rightText: dateComplete(recentSearches[i].dateOfSearch), hasChild: true, itemType: 'recent_search' });
				row.searchParams = recentSearches[i];
				sectionRecents.add(row);
			}
			numRecentSearches = recentSearches.length;
		}
		
		var sectionBusquedaAvanzada = Ti.UI.createTableViewSection(); 
		advancedSearchRow = _this.createSearchTableViewRow({ win: self, title: L('Busqueda avanzada'), value: glb.searchParams.advancedSearch, hasChild: false, selectionStyle: 'none', itemId: 'advancedSearch', itemValue: glb.searchParams.advancedSearch, switchValue: glb.searchParams.advancedSearch });
		advancedSearchRow.addEventListener('row:switchChanged',function(e){
			if(e.value){
				tableView.appendRow(searchInVersionRow);
				tableView.appendRow(searchInBodyRow);
				//tableView.appendRow(date_typeRow);
				// tableView.appendRow(date_fromRow);
				// tableView.appendRow(typesRow);
			}else{
				tableView.deleteRow(numRecentSearches+5);
				tableView.deleteRow(numRecentSearches+4);
				tableView.deleteRow(numRecentSearches+3);
				tableView.deleteRow(numRecentSearches+2);
				tableView.deleteRow(numRecentSearches+1);
				initializeSearchParams();
			}
		});
		if (glb.searchParams.advancedSearch)
			advancedSearchRow.fireEvent('row:switchChanged', {'value': true});
		sectionBusquedaAvanzada.add(advancedSearchRow);
		if(sectionRecents!==undefined){
			tableView.setData([sectionRecents,sectionBusquedaAvanzada]);
		}else{
			tableView.setData([sectionBusquedaAvanzada]);
		}
	}
		
	function searchClick(){
		if(search.value){
			glb.searchParams.filters = null;
			glb.searchParams.sort = 'date';
			glb.searchParams.dir = glb.config.sortType;
			glb.searchParams.searchText = search.value;
			// keep the last search in local properties.
			function replaceLastSearchProperty(searchParams){
				// 1. list all search properties kept in db
				var recentSearches = JSON.parse(Ti.App.Properties.getString('box4u:recentSearches'));
				if(!recentSearches)
					recentSearches=[];
				if (recentSearches.length > 2)
					recentSearches.pop();
				recentSearches.unshift(searchParams);
				numRecentSearches = recentSearches.length;
				Ti.App.Properties.setString('talk4u:recentSearches',JSON.stringify(recentSearches));
			}
			replaceLastSearchProperty(glb.searchParams);
			populateTableView();
		}else{
			search.focus();
		}
	}
	// CREATE NAVBAR BUTTONS
	var search = Ti.UI.createSearchBar({
		showCancel: true,
		height: 43,
		top: 0,
		right: 0,
		width: 310,
		barColor:(glb.config.theme.color1)?glb.config.theme.color1:null
	});
	search.addEventListener('cancel',function(){
		search.blur();
		search.value = null;
	});
	search.addEventListener('return',searchClick);
	// CREATE WINDOW
	var self = this.createWindow({
		openStyle: (params.openStyle)?params.openStyle:null,
		rightNavButton: search,
	});
	self.addEventListener('open', fn_open);
	self.addEventListener('close',fn_close);
	self.addEventListener('click',function(){
		search.blur();
		trace('click');
	});
	// CREATE VIEW
	var actInd = this.createActivityIndicator();
	self.add(actInd);
	var tableView = this.createTableView({
		style: 'grouped',
	});
	tableView.addEventListener('click',function(e){

	});
	var footerView = Ti.UI.createView({
		width: Ti.UI.FILL,
		height: 60
	});
	var searchBtn = _this.createButton({
		title : L('search'),
		borderRadius : 10,
		backgroundImage : (glb.config.theme.buttonLoginColor)?'images/talk4u/buttonColor/'+glb.config.theme.buttonLoginColor+'.png':'images/talk4u/buttonColor/gray.png',
		height : 40,
		bottom : 10,
		width : 300,
		font: {fontSize: 16, fontWeight: 'bold'}
	}); 
	searchBtn.addEventListener('click',searchClick);
	footerView.add(searchBtn);
	tableView.footerView = footerView;
	self.add(tableView);

	initializeSearchParams();
	populateTableView();
	
	return self;
};
Talk4USearchForm.prototype = new Talk4UForm();
module.exports = Talk4USearchForm;