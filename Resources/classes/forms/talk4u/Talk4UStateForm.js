var Talk4UForm = require('classes/Talk4UForm');
var Talk4UStateForm = function(params) {
	var _this = this;
	var rowChecked;
	var data = [];

	function fn_open() {
		data = [
		//_this.createText11TableViewRow({leftImage:"images/talk4u/status/disconected.png", title: "Desconectado"}),
		_this.createText11TableViewRow({
			leftImage : "images/talk4u/status/online.png",
			title : "Conectado"
		}), _this.createText11TableViewRow({
			leftImage : "images/talk4u/status/inactive.png",
			title : "Ausente"
		}), _this.createText11TableViewRow({
			leftImage : "images/talk4u/status/busy.png",
			title : "Inactivo"
		})];
		rowChecked = 0;
		data[rowChecked].hasCheck = true;
		tableView.setData(data);
	}

	function fn_close() {
	}

	// CREATE WINDOW
	var self = this.createWindow({
		title : "Carlos Tirado"
	});
	self.addEventListener('open', fn_open);
	self.addEventListener('close', fn_close);

	//Create View

	var tableView = this.createTableView({
		style : 'grouped',
		scrollable : false
	});
	tableView.addEventListener('click', function(e) {
		Ti.App.Properties.setString('talk4u:estado', e.index);
		data[rowChecked].hasCheck = false;
		e.row.hasCheck = true;
		rowChecked = e.index;
		switch(e.index) {
			case 0:
				alert("conectado")
				if (rowChecked !== 0)
					conn.send("presence xmlns='jabber:client' id='" + this.getUniqueId() + "'><priority>50</priority><x xmlns='vcard-temp:x:update'><photo/></x></presence>'");
				conn.send("<iq xmlns='jabber:client' type='set' id='" + this.getUniqueId() + "'><pubsub xmlns='http://jabber.org/protocol/pubsub'><publish node='http://jabber.org/protocol/activity'><item id='0'><activity xmlns='http://jabber.org/protocol/activity' /></item></publish></pubsub></iq>");
				conn.send("<iq xmlns='jabber:client' type='set' id='" + this.getUniqueId() + "'><pubsub xmlns='http://jabber.org/protocol/pubsub'><publish node='http://jabber.org/protocol/mood'><item id='0'><mood xmlns='http://jabber.org/protocol/mood' /></item></publish></pubsub></iq>");

				break;
			case 1:
				alert("away")
				if (rowChecked !== 1)
					conn.send("presence xmlns='jabber:client' id='" + this.getUniqueId() + "'><priority>40</priority><show>away</show><x xmlns='vcard-temp:x:update'><photo/></x></presence>'");
				conn.send("<iq xmlns='jabber:client' type='set' id='" + this.getUniqueId() + "'><pubsub xmlns='http://jabber.org/protocol/pubsub'><publish node='http://jabber.org/protocol/activity'><item id='0'><activity xmlns='http://jabber.org/protocol/activity' /></item></publish></pubsub></iq>");
				conn.send("<iq xmlns='jabber:client' type='set' id='" + this.getUniqueId() + "'><pubsub xmlns='http://jabber.org/protocol/pubsub'><publish node='http://jabber.org/protocol/mood'><item id='0'><mood xmlns='http://jabber.org/protocol/mood' /></item></publish></pubsub></iq>");

				break;

			case 2:
				alert("dnd")

				if (rowChecked !== 2)
					conn.send("presence xmlns='jabber:client' id='" + this.getUniqueId() + "'><priority>40</priority><show>dnd</show><x xmlns='vcard-temp:x:update'><photo/></x></presence>'");
				conn.send("<iq xmlns='jabber:client' type='set' id='" + this.getUniqueId() + "'><pubsub xmlns='http://jabber.org/protocol/pubsub'><publish node='http://jabber.org/protocol/activity'><item id='0'><activity xmlns='http://jabber.org/protocol/activity' /></item></publish></pubsub></iq>");
				conn.send("<iq xmlns='jabber:client' type='set' id='" + this.getUniqueId() + "'><pubsub xmlns='http://jabber.org/protocol/pubsub'><publish node='http://jabber.org/protocol/mood'><item id='0'><mood xmlns='http://jabber.org/protocol/mood' /></item></publish></pubsub></iq>");
				break;
			case 3:
				alert("xa")

				conn.send("presence xmlns='jabber:client' id='" + this.getUniqueId() + "'><priority>40</priority><show>xa</show><x xmlns='vcard-temp:x:update'><photo/></x></presence>'");
				break;
			default:
			//Android
		}

	});
	self.add(tableView);

	return self;
}

Talk4UStateForm.prototype = new Talk4UForm();
module.exports = Talk4UStateForm;

