/*
 * crea la ventana que muestra los ajustes de la cuenta de usuario logeada
 * 
 */
var Talk4UForm = require('classes/Talk4UForm');
var Talk4UAccountForm = function(params) {
	var _this=this;
	var _data=[];
	var storageRow;
	var geolocalizationRow;
	var notificationsRow;
	var notificationChangeByError = false;
	function fn_open(){
		data = talk4u.module.getAccount();
	}

	function fn_close(){
		self.removeEventListener('open',fn_open);

		self.removeEventListener('close',fn_close);
		self=null;
	}
	

	// CREATE WINDOW
	var self = this.createWindow({
		title: L('account'),
		openStyle: (params.openStyle)?params.openStyle:null
 	});
 	
	self.addEventListener('open', fn_open);
	self.addEventListener('close',fn_close);
	
	// CREATE VIEW
	var tableView = this.createTableView({
		style:'grouped'
	});
	tableView.addEventListener('click',function(e){
		tableView.setTouchEnabled(false);
		tableView.setTouchEnabled(true);
	});
	var footerView = Ti.UI.createView({
		width: Ti.UI.FILL,
		height: Ti.UI.SIZE,
		layout: 'vertical'
	});
	var logOutButton = this.createButton({
		title:L('log_out'),
		//style: 'bordered',
		width: 300,
		height: 40,
		top: 20,
		bottom: 20,
		//borderWidth:1,
		backgroundImage: 'images/talk4u/buttonColor/darkred.png',
		borderRadius: 10,
		font: {fontSize: 16, fontWeight: 'bold'}
 	});
 	
 	
 	logOutButton.addEventListener('click',function(){
		talk4u.module.logoutSubmit();
	});
	
	//footerView.add(saveButton);
	footerView.add(logOutButton);
	tableView.footerView=footerView;
	
	self.add(tableView);
	
	return self;
}
Talk4UAccountForm.prototype = new Talk4UForm();
module.exports = Talk4UAccountForm;