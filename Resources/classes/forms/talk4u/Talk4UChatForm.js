var Talk4UForm = require('classes/Talk4UForm');
var Talk4UChatForm = function(params) {
	var xmppjid = params.xmppjid;

	function append_chat_message(message) {
		if (xmppjid === message.getAttribute("from")) {
			textArea.recieveMessage(message.getChild("body").getText());
		}
	}

	function sendChatMessage(message, dest) {
		xmppservice.sendMessage({
			from : jid + "/talk4u",
			to : dest,
			message : message
		});
	}

	function fn_open() {
		textArea.blur();
		if (params.chat) {//un chat que ya existe
			win.chat = params.chat;
			listaChats[params.xmppjid].nMessagesNotRead = 0
			talk4u.module.getChat(win);
		} else {
			win.chat = "1";
			//TODO:quitarlo xq no lo voy a necesitar
			talk4u.module.getChat(win);
			//talk4u.module.createChat()
		}
	}

	function fn_close() {
		listaChats[params.xmppjid].nMessagesNotRead = 0
		win.close();
	}

	function getData(callback) {
		if (callback.data === null) {
			textArea.addLabel(L("API_Not_conected"));
		}
			//xmppjid = //callback.data.xmppjid; TODO: cuando reciba uno bueno ponerlo aqui de momento el k pasa al construir
			conn.addHandler(append_chat_message, null, 'message', null, null, null);
			if ( typeof listaChats[params.xmppjid] !== "undefined") {
				for (var i = 0; i < listaChats[params.xmppjid].messages.length; i++) {
					var contenido = listaChats[params.xmppjid].messages[i].content;
					if (!listaChats[params.xmppjid].messages[i].dest) {
						textArea.recieveMessage(contenido);
					} else {
						textArea.sendMessage(contenido);
					}
				}
			}
			win.title = (callback.data.topic)?callback.data.topic:"Not chat";
	}

	var infoButton = this.createButton({
		title : "Info"
	});
	var win = this.createWindow({
		title : "Chat",
		tabBarHidden : true,
		rightNavButton : infoButton,
		openStyle : (glb.device==='iphone')?'modal':'inDetail'
	});
	win.addEventListener('open', fn_open);
	win.addEventListener('talk4uchatform:getData', getData)
	win.addEventListener('close', fn_close);

	var textArea = Ti.SMSView.createView({
		//maxLines:6,				// <--- Defaults to 4
		//minLines:2,				// <--- Defaults to 1
		backgroundColor : '#dae1eb', // <--- Defaults to #dae1eb
		assets : 'images/talk4u/assets', // <--- Defauls to nothing, smsview.bundle can be placed in the Resources dir
		// sendColor: 'Green',		// <--- Defaults to "Green"
		// recieveColor: 'White',	// <--- Defaults to "White"
		// selectedColor: 'Blue',	// <--- Defaults to "Blue"
		// editable: true,			// <--- Defautls to true, do no change it
		// animated: false,			// <--- Defaults to true
		buttonTitle: 'Enviar',	// <--- Defaults to "Send"
		// font: { fontSize: 12 ... },	// <--- Defaults to... can't remember
		// autocorrect: false,		// <--- Defaults to true
		textAlignment : 'left', // <--- Defaulst to left
		// textColor: 'blue',		// <--- Defaults to "black"
		returnType : Ti.SMSView.RETURNKEY_SEND, // <---- Defaults to Ti.SMSView.RETURNKEY_DEFAULT
		camButton : true,	// <--- Defaults to false
		//hasTab:true				// <--- Defaults to false

	});
	win.add(textArea);

	textArea.addEventListener('click', function(e) {
		if (e.scrollView) {
			textArea.blur();
		}
		// fires when clicked on the scroll view
		Ti.API.info('Clicked on the scrollview');
	});
	textArea.addEventListener('buttonClicked', function(e) {
		// fires when clicked on the send button
		textArea.addLabel(moment().format('LLL'));
		textArea.sendMessage(e.value);
		sendChatMessage(e.value, xmppjid)

	});
	textArea.addEventListener('camButtonClicked', function() {
		// fires when clicked on the camera button

		var options = Ti.UI.createOptionDialog({
			options : ['Galeria', 'Cancel'],
			cancel : 1,
			title : 'Enviar foto'
		});
		options.show();
		options.addEventListener('click', function(e) {
			if (e.index == 0) {
				// --------------- open the photo gallery and send an image ------------------
				Titanium.Media.openPhotoGallery({
					success : function(event) {
						// uncomment to set a specific width, in this case 100
						// var image = Ti.UI.createImageView({image:event.media});
						// image.width = 100;
						// image.height = (100/event.media.width)*event.media.height
						//textArea.sendMessage(image.toBlob());
						textArea.sendMessage(event.media);
						sendChatMessage(event.media, xmppjid)
					},
					mediaTypes : [Ti.Media.MEDIA_TYPE_PHOTO]
				});
				// ---------------------------------------------------------------------------
			}
		});
	});

	textArea.addEventListener('change', function(e) {
		Ti.API.info(e.value);
	});

	textArea.addEventListener('messageClicked', function(e) {
		// fires when clicked on a message
		if (e.text) {
			Ti.API.info('Text: ' + e.text);
		}
		if (e.image) {
			Ti.API.info('Image: ' + e.image);
		}
		Ti.API.info('Index: ' + e.index);
	});

	return win;
}
Talk4UChatForm.prototype = new Talk4UForm();
module.exports = Talk4UChatForm;
