var Talk4UForm = require('classes/Talk4UForm');
var Talk4UChatForm = function(params) {
	var _this = this;
	var data = [];
	var msjSelected;
	var xmppjid = {};
	var participants = [];

	function append_chat_message(message) {
		if (xmppjid === message.getAttribute("from")) {
			var msjRow = _this.createChatTableViewRowOwner({
				userImage : "images/talk4u/fakePNG/user.png",
				itemName : message.getChild("body").getText(),
				propietario : message.getAttribute("from"),
				date : moment().fromNow(),
			});
			tableView.insertRowBefore(0, msjRow);
		}
	}

	function new_send_message(message) {
		var msjRow = _this.createChatTableViewRow({
			userImage : "images/talk4u/fakePNG/user.png",
			itemName : message,
			propietario : xmppjid,
			date : moment().fromNow(),
		});
		tableView.insertRowBefore(0, msjRow);
	}

	function fn_open() {
		self.chat = params.chat;
		listaChats[params.xmppjid].nMessagesNotRead = 0
		talk4u.module.getChat(self);
	}

	function fn_close() {
		listaChats[params.xmppjid].nMessagesNotRead = 0
		navigation.back(self);

	}

	function getData(callback) {

		if (callback === null) {
			self.title = "Chat";
			var mensajes = []

			var msjRow = _this.createChatTableViewRowOwner({
				userImage : "images/talk4u/fakePNG/user.png",
				itemName : "No se ha podido recoger ningun mensaje",
				propietario : "nadie",
				date : moment().fromNow(),
			});
			mensajes.push(msjRow);
			tableView.data = mensajes;

		} else {
			xmppjid = params.xmppjid//callback.data.xmppjid; TODO: cuando reciba uno bueno ponerlo aqui de momento el k pasa al construir
			conn.addHandler(append_chat_message, null, 'message', null, null, null);
			self.title = callback.data.topic;
			participants = callback.data.participants;
			var mensajes = []
			for (var i = listaChats[params.xmppjid].messages.length - 1; i >= 0; i--) {
				if (params.xmppjid === listaChats[params.xmppjid].messages[i].dest) {
					var msjRow = _this.createChatTableViewRowOwner({
						userImage : "images/talk4u/fakePNG/user.png",
						itemName : listaChats[params.xmppjid].messages[i].content,
						propietario : getUserFromJid(params.xmppjid),
						date : moment(listaChats[params.xmppjid].messages[i].date, "YYYYMMDDhhmmss").fromNow(),//"callback.data.mensajes[i].date"
					});
					mensajes.push(msjRow);
				} else {
					var msjRow = _this.createChatTableViewRow({
						userImage : "images/talk4u/fakePNG/user.png",
						itemName : listaChats[params.xmppjid].messages[i].content,
						propietario : getUserFromJid(params.xmppjid),
						date : moment(listaChats[params.xmppjid].messages[i].date, "YYYYMMDDhhmmss").fromNow(),//"callback.data.mensajes[i].date"
					});
					mensajes.push(msjRow);
				}

				// } else {
				// var msjRow = _this.createChatTableViewRowOwner({
				// userImage : "images/talk4u/fakePNG/user.png",
				// itemName : callback.data.mensajes[i].contenido,
				// propietario : callback.data.mensajes[i].propietario,
				// date : callback.data.mensajes[i].date
				// })
				// }
			};
			tableView.data = mensajes;
			//xmppservice.joinChat(callback.data);
		};
	}

	//create NAVBar elements
	var infoButton = this.createButton({
		title : "Info"
	});

	// CREATE WINDOW
	var self = this.createWindow({
		title : "Chat",
		rightNavButton : infoButton,
		openStyle : 'modal'
	});
	self.addEventListener('open', fn_open);
	self.addEventListener('talk4uchatform:getData', getData)
	self.addEventListener('close', fn_close);

	//Create View
	var tableView = this.createTableView({
		borderWidth : 0,
		borderColor : 'transparent',
		backgroundColor : 'transparent'
	});

	tableView.addEventListener('longpress', function(e) {
		var actionOptionDialog = Ti.UI.createOptionDialog({
			options : [L('Copiar'), L('Reenviar'), L('Eliminar'), L('Cancelar')],
			destructive : 2,
			cancel : 3
		});
		actionOptionDialog.addEventListener('click', function(e) {
			if (e.index === 0) {
				//Ti.UI.Clipboard.setText(msjSelected);
				var infoDialog = Ti.UI.createAlertDialog({
					title : '',
					message : msjSelected,
					ok : L('accept'),
				}).show();
			} else if (e.index === 1) {
				//reenviar
			} else if (e.index === 2) {
				//eliminar
			}
		});

		msjSelected = e.row.itemName;
		actionOptionDialog.show({
			animated : true,
		});
	});
	self.add(tableView);

	//create toolbar
	/*
	var actionItem = {
		title : "action",
		fn_click : function() {
			var actionOptionDialog = Ti.UI.createOptionDialog({
				options : [L('Tomar foto o video'), L('Seleccionar existente'), L('Compartir contacto'), L('Compartir ubicación'), L('Cancelar')],
				cancel : 4
			});

			actionOptionDialog.addEventListener('click', function(e) {
				if (e.index === 0) {

				} else if (e.index === 1) {
					//reenviar
				} else if (e.index === 2) {
					//eliminar
				} else if (e.index === 3) {
					//eliminar
				}
			});
			actionOptionDialog.show({
				animated : true,
			});
		}
	};
	var sendItem = {
		title : "Enviar",
		fn_click : function() {
			new_send_message("hola tio que tal, esto funcaaaaaa");
			xmppservice.sendMessage({
				from : jid + "/talk4u",
				to : xmppjid,
				message : "hola tio que tal, esto funcaaaaaa"
			});
		}
	};

	var keyBoardToolbar = this.createToolBar({
		items : [actionItem, sendItem]
	});

	var barcodeField = {
		borderStyle : Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
		hintText : 'Focus',
		keyboardToolbar : keyBoardToolbar, // [actionItem, sendItem],
		keyboardToolbarColor : '#999',
		keyboardToolbarHeight : 40,
		top : 10,
		height : 32,
		width : 175
	};

	var toolbar = this.createToolBar({
		items : [actionItem, barcodeField, sendItem],
	});

	self.add(toolbar);

	/*var toolBar = Titanium.UI.createToolBar({
	 items : [textfield],
	 pos : "top"
	 });

	if (self.openStyle === 'inTab') {
		self.add(toolBar);
		var fn_open_extra = function() {
			tableView.bottom = toolBar.toImage().height;
			glb.tabgroup.animate({
				bottom : -50,
				duration : 0
			});
		}
		var fn_close_extra = function() {
			glb.tabgroup.animate({
				bottom : 0,
				duration : 0
			});
		}
	} else if (self.openStyle === 'inNavGroup' || self.openStyle === 'modal') {
		if (toolbar.pos === 'bottom') {
			self.add(toolbar);
		} else
			self.setRightNavButton(toolbar);
	}
	*/
 
	var flexSpace = Titanium.UI.createButton({
	    systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
	});
	 
	var bb1 = Titanium.UI.createButtonBar({
	    labels:['Front', 'Back'],
	    //backgroundColor:'#336699',
	    top:50,
	    style:Titanium.UI.iPhone.SystemButtonStyle.BAR,
	    height:30,
	    width:100
	});
	
	var bb2 = Titanium.UI.createButtonBar({
	    labels:['Save', 'Add another'],
	//  backgroundColor:'#336699',
	    top:50,
	    style:Titanium.UI.iPhone.SystemButtonStyle.BAR,
	    height:30,
	    width:170
	});
	
	var bb3 = Titanium.UI.createButtonBar({
	    labels:['Button Here'],
	//  backgroundColor:'#336699',
	    top:50,
	    style:Titanium.UI.iPhone.SystemButtonStyle.BAR,
	    height:30,
	    width:170
	});
	 
	var front = Titanium.UI.createTextField({
	    color:'#336699',
	    font:{fontSize:20,fontFamily:'Marker Felt', fontWeight:'bold'},
	    left: 15,
	    height:35,
	    width:300,
	    top:10,
	    borderStyle:'none',
	    keyboardToolbar:[bb1, flexSpace, bb3],
	    keyboardToolbarColor: 'black',  
	    keyboardToolbarHeight: 35,
	    hintText: 'Here text'
	});
	 
	var back = Titanium.UI.createTextArea({
	    color:'black',
	    height:160,
	    width:300,
	    top:40,
	    left: 10,
	    backgroundColor: 'none',
	    font:{fontSize:17,fontFamily:'Marker Felt', fontWeight:'bold'},
	    keyboardToolbar:[bb1, flexSpace, bb2],
	    keyboardToolbarColor: 'black',  
	    keyboardToolbarHeight: 35
	});
	 
	 
	self.add(front);
	self.add(back);	
	return self;
}

Talk4UChatForm.prototype = new Talk4UForm();
module.exports = Talk4UChatForm;

