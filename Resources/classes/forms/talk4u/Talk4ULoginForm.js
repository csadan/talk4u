/*
 * Crea la venta de login
 * 
 */
var Talk4UForm = require('classes/Talk4UForm');
function Talk4ULoginForm(){
	var _this=this;
	function fn_close(){
		if(typeof fn_close_extra === 'function') fn_close_extra();
		self.removeEventListener('close',fn_close);
		self=null;
	}
	// CREATE WINDOW
	var self = this.createWindow({
		title: L('login'),
		backgroundImage: 'Default.png',
		backgroundImageOrientation: true
	});
	self.addEventListener('close',fn_close);
	self.addEventListener('click',function(){
		// unfocus textfields
		username.blur();
		password.blur();
	});		
	// CREATE VIEW

	var scrollView = Ti.UI.createScrollView({
		height: Ti.UI.FILL,
		width: Ti.UI.FILL,
		contentHeight: 'auto',
	});
	self.add(scrollView);
	var loginView=Ti.UI.createView({
		layout: 'vertical',
		left: 20,
		right: 20,
		top: '50%',
		height: Ti.UI.FILL,
	});
	
	// Username
	var username = this.createTextField({
		top: 50,
		width: Ti.UI.FILL,
		height: 40,
		hintText: L('username'),
		returnKeyType: Ti.UI.RETURNKEY_NEXT 
	});
	username.addEventListener('return',function(){
		// focus password
		password.focus();
	});
	loginView.add(username);
	 
	// Password
	var password = this.createTextField({
	    top: 20,
	    width: Ti.UI.FILL,
	    height: 40,
	    hintText: L('password'),
	    passwordMask:true,
	    returnKeyType: Ti.UI.RETURNKEY_GO
	});
	loginView.add(password);
	password.addEventListener('return',function(){
		// login submit.
		if(password.value!='' && username.value!=''){
			talk4u.module.loginSubmit(username.getValue(), password.getValue());
		}else
			password.focus();
	});

	var button = this.createButton({
		height:40,
		width:Ti.UI.FILL, //300, no se como quedará en ipad... y otros formatos de pantalla
		title:L('conect'),
		//borderWidth: 1,
		borderRadius: 10,
		top:20,
		backgroundImage: (glb.config.theme.buttonLoginColor)?'images/talk4u/buttonColor/'+glb.config.theme.buttonLoginColor+'.png':'images/talk4u/buttonColor/gray.png',
		font: {fontSize: 16, fontWeight: 'bold'}
		//style: 'bordered'
	});

	button.addEventListener('click', function() {
		this.setTuchEnabled(false);
		if(password.value!='' && username.value!='')
			talk4u.module.loginSubmit(username.getValue(), password.getValue());
		else {
			username.focus();
		}
		this.setTouchEnabled(true);
	});
	loginView.add(button);
	scrollView.add(loginView);
	
	return self;
}
Talk4ULoginForm.prototype=new Talk4UForm();
module.exports = Talk4ULoginForm;