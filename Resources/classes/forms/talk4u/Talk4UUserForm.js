var Talk4UForm = require('classes/Talk4UForm');

var Talk4UUserForm = function(params) {
	var _this = this;
	var data = [];
	function fn_open() {
		talk4u.module.getUser({
			'form' : self,
			'params' : params
		});
	}

	function fn_close() {

	}

	function getData(callback) {

		var sectionChats = Ti.UI.createTableViewSection({
			headerTitle : 'Chats'
		});
		sectionChats.addEventListener('downloaded', function(e) {
			for (var i = 0; i < e.data.totalCount; i++) {
				var chatRow = _this.createText11TableViewRow({
					id : e.data.items[i].id,
					title : e.data.items[i].topic,
					rightText : dateComplete(e.data.items[i].lastActive),
					hasChild : false
				});
				sectionChats.add(chatRow);
			};
			tableView.setData([sectionChats]);
		});

		talk4u.module.getUserChats({
			'sectionChats' : sectionChats,
			'id' : callback.datos.id,
			'start' : 0,
			'limit' : 5,
			'sort' : "",
		});

		var tableView = _this.createTableView({
			style : 'grouped'
		});

		self.title = callback.datos.name;
		var userInfoView = _this.createUserInfoView({
			itemName : callback.datos.name,
			userJid : (callback.datos.jid)?callback.datos.jid:L("no user jid"),
			userImage : 'images/talk4u/fakePNG/user.png',
			itemId : callback.datos.id,
			photoId : callback.datos.photoId,
			itemType : "user"
		});
		userInfoView.userImg.addEventListener('downloaded', function(e) {
			this.image = e.localFilePath;
		});
		talk4u.module.photo(userInfoView.userImg);

		tableView.headerView = userInfoView;
		self.add(tableView);
		var sendItem = {
			image : "images/talk4u/icoToolbar/icons-white/enviar.png",
			fn_click : function() {
				var form = talk4u.formFactory.createForm('Talk4UChatForm', {
					openStyle : 'modal',
					xmppjid : getUserFromJid(callback.datos.jid)+"@"+glb.config.xmppDomain+"/"+glb.config.xmppSource,
				});
				talk4u.formFactory.openForm(form);
			}
		};
		var toolBar = _this.createToolBar({
			items : [sendItem],
		});
		self.add(toolBar);

	}

	// CREATE WINDOW

	var self = this.createWindow({
		title : "Información de contacto",
		openStyle : (glb.device==='iphone')?'modal':'inDetail',
		id : params.id
	});

	self.addEventListener('open', fn_open);
	self.addEventListener('talk4uuserform:getData', getData);
	self.addEventListener('close', fn_close);

	//create toolbar

	/*
	 if (self.openStyle === 'inTab') {
	 self.add(toolBar);
	 var fn_open_extra = function() {
	 tableView.bottom = toolBar.toImage().height;
	 glb.tabgroup.animate({
	 bottom : -50,
	 duration : 0
	 });
	 }
	 var fn_close_extra = function() {
	 glb.tabgroup.animate({
	 bottom : 0,
	 duration : 0
	 });
	 }
	 } else if (self.openStyle === 'inNavGroup' || self.openStyle === 'modal') {
	 if (toolBar.pos === 'bottom') {
	 self.add(toolBar);
	 } else
	 self.setRightNavButton(toolBar);
	 }
	 */
	return self;
}
Talk4UUserForm.prototype = new Talk4UForm();
module.exports = Talk4UUserForm;

