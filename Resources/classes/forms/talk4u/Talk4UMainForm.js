var Talk4UForm = require('classes/Talk4UForm');
function Talk4UMainForm(obj) {
	
	var obj = this.getMainWindows();
	
	var self = this.createMainWindow({
		forms: obj.forms,
		titles: obj.titles,
		icons: obj.icons,
	});
	
	return self;
};
Talk4UMainForm.prototype=new Talk4UForm();
module.exports = Talk4UMainForm;