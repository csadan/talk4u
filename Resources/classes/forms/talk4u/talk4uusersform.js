/*
 * crea la ventana que muestra la lista de usuarios
 */
var Talk4UForm = require('classes/Talk4UForm');
var Talk4UUsersForm = function(params){
	var _this=this;
	var data=[];
	var dataForm=[];
	var start=0;
	//var usersTotalCount;
	//var groupsTotalCount;
	var totalCount;
	var updating=true;
	var moreRow = null;
	
	function getData(callback){
		var rows = [];
		tableView.data = rows;//delete previos rows
		if(callback.data!==null){
			for (var i=0; i < callback.data.items.length; i++) {
			  var row = _this.createText11TableViewRow({
			  	leftImage: '/images/talk4u/fakePNG/user.png',
			  	title: callback.data.items[i].name,
			  	hasDetail: true,
			  	itemId: callback.data.items[i].id,
			  	itemType: 'user',
			  	type:'photo',
			  	photoId: callback.data.items[i].photo.photoId
			  });
			  talk4u.module.photo(row);
			  rows.push(row);
			};
			tableView.data=rows;
		}else{
			alert(L("Not_Connected"))
		}
		actInd.hide();
	}
	
	function fn_open(){
		actInd.show();
		talk4u.module.getAllUsers({'form':self, 
								   'start':0,
								   'limit':100});				   
	}
	
	function fn_close(){
		self.removeEventListener('open',fn_open);
		self.removeEventListener('close',fn_close);
		self=null;
	}

	function actionResponse(res, action){
		if (res) {
			var message='';
			var items=getItemObjectChecked();
			for (var i in items){
				if(res[i] && res[i].itemId===items[i].itemId){
					if(!res[i].response)
						message+=items[i].itemName+': '+L('error')+'.\n';
					else if (glb.mainWindow.detailView && glb.mainWindow.detailView.window.children[0] && glb.mainWindow.detailView.window.children[0].itemId===res[i].itemId){
						// child actions for inDetail
						if (action === 'addDeleteFavourites'){
							talk4u.module.usersGroupsTableClick(data[items[i].pos], actionResponse);
						}
					}
				}
			}
			if(action === 'addDeleteFavourites' || action === 'refresh'){
				refresh();
			}
			if(message!==''){
				var infoDialog = Ti.UI.createAlertDialog({
					title : L('info'),
					message: message,
					ok : L('accept'),
				}).show();
			} else if (tableView.editionMode === true){
				editSelfCancel();
			}
		} else if (res === false) {
			//No connection Error
			var connectionDialog = Ti.UI.createAlertDialog({
				title : L('attention'),
				message : L('connection_error'),
				ok : L('accept'),
			}).show();
		} else {
			var errorDialog = Ti.UI.createAlertDialog({
				title : L('error'),
				ok : L('accept'),
			})
			if(action === 'send')
				errorDialog.message=L('send_error');
			else if(action === 'addDeleteFavourites')
				errorDialog.message=L('favourites_error');
			errorDialog.show();
		}
		editButton.setEnabled(true);
	}
	
	// CREATE NAVBAR BUTTONS
	var connectedItem = {
		title: L('Conectados'),
		fn_click: function(){
			// make call
			data= talk4u.module.getAllUsers();
		}
	};
	var favouritesItem = {
		title:L('Favoritos'),
		fn_click: function(){
			// make call
			data= talk4u.module.getFavouritesUsers({'form':self, 
								   'start':0,
								   'limit':100});	
		}
	};
	var allItem = {
		title: L('Todos'),
		fn_click: function(){	
			data=talk4u.module.getAllUsers({'form':self, 
								   'start':0,
								   'limit':100});	
		}
	};
	var segmentedControl = this.createSegmentedControl({
		items:[allItem,favouritesItem],
	});
	
	// CREATE WINDOW
	var self = this.createWindow({
		formName: 'Talk4UUsersForm',
		leftNavButton: segmentedControl,
		//rightNavButton: editButton,
		openStyle: (params.openStyle)?params.openStyle:null,
		backgroundImage: 'none',
	});
	self.addEventListener('open', fn_open);
	self.addEventListener('talk4uusersform:getData',getData);
	self.addEventListener('close',fn_close);
	
	// CREATE VIEW
	var search = Ti.UI.createSearchBar({
		// barColor:'#000', 
		showCancel: true,
		height: 43,
		top: 0,
		right: 0,
		barColor:(glb.config.theme.color1)?glb.config.theme.color1:null
	});
	search.addEventListener('return',function(e){
		blockAndResetForm();
		// make call
		if(segmentedControl.getIndex()===0)
			talk4u.module.searchUser(self, e.value, start, glb.config.limit);
		else if(segmentedControl.getIndex()===1)
			talk4u.module.searchGroup(self, e.value, start, glb.config.limit);
		// if(segmentedControl.getIndex()===2)
			// talk4u.module.getFavouriteUsersGroups(self, 0, -1, glb.config.sortType);
		else {
			editButton.setEnabled(true);
			segmentedControl.setTouchEnabled(true);
		}
		this.blur();
	});
	search.addEventListener('cancel',function(e){
		blockAndResetForm();
		resetSearch();
		// make call
		if(segmentedControl.getIndex()===0)
			talk4u.module.getUsers(self, start, glb.config.limit, glb.config.sortType);
		else if(segmentedControl.getIndex()===1)
			talk4u.module.getGroups(self, start, glb.config.limit, glb.config.sortType);
		// if(segmentedControl.getIndex()===2)
			// talk4u.module.getFavouriteUsersGroups(self, 0, -1, glb.config.sortType);
		else {
			editButton.setEnabled(true);
			segmentedControl.setTouchEnabled(true);
		}
	});
	var searchView = Ti.UI.createView({
		height: 43,
		top: 0,
		width: Ti.UI.FILL
	});
	searchView.add(search);
	
	var actInd = this.createActivityIndicator();
	self.add(actInd);
	var tableView = this.createTableView({
		/*top : 0,
		editionMode : false,
		dinamicEdit : true,
		fn_dinamicScroll : fn_dinamicScroll,*/
	});
	tableView.addEventListener('click', function(e) {
			var userForm = talk4u.formFactory.createForm('Talk4UUserForm',e.row);
			talk4u.formFactory.openForm(userForm); 
	});	
	self.add(tableView);
	
	return self;
};
        
Talk4UUsersForm.prototype = new Talk4UForm();
module.exports = Talk4UUsersForm;