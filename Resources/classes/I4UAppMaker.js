function I4UAppMaker(){};
I4UAppMaker.prototype.getApp=function(appType){
	if(appType==='box'){
		var Box4UApp = require('classes/Box4UApp');
		return new Box4UApp();
	}else if(appType==='talk'){
		var Talk4UApp = require('classes/Talk4UApp');
		return new Talk4UApp();
	}else if(appType==='email'){
		var Email4UApp = require('classes/Email4UApp');
		return new Email4UApp();
	}
};
module.exports=I4UAppMaker;