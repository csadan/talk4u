var I4UService=require('classes/I4UService');
function Talk4UService(){
	//I4UService.apply(this);
	// this._Talk4UDAl = require('classes/Talk4UDal');
	
	//collection of all service's methods
	this.service = {}; 

	//state set of service
	//state 1: online
	//state 2: offline
	this.states = { 
		'online': {}, 
		'offline': {}
	};
	
	// this.prev_state = (Ti.Network.online)? 'online' : 'offline';
	
	this.states.online = (function(){
		Ti.include('/classes/service/talk4u/methods/Talk4UOnlineService.js');
		return {
			login: login,
			logout:logout,
			getChats: getChats,
			getChat: getChat,
			getAllUsers: getAllUsers,
			getFavouriteUsersGroups: getFavouriteUsersGroups,
			getUser: getUser,
			getUserChats: getUserChats,
			photo: photo
		}
	})();
	this.states.offline = (function(){
		Ti.include('/classes/service/talk4u/methods/Talk4UOfflineService.js');
		return {
			addLink: addLink,
			addPermissionsForUser: addPermissionsForUser,
			advancedSearch: advancedSearch,
			convertInVersionOf: convertInVersionOf,
			copy: copy,
			createFolder: createFolder,
			createVersion: createVersion,
			data: data,
			_delete: _delete,
			deleteItemPermission: deleteItemPermission,
			deleteRelations: deleteRelations,
			deleteVersion: deleteVersion,
			emptyTrashBin: emptyTrashBin,
			extractVersion: extractVersion,
			geolocate: geolocate,
			getAccount: getAccount,
			getEffectivePermissions: getEffectivePermissions,
			getElement: getElement,
			getFavouriteUsersGroups: getFavouriteUsersGroups,
			getFilesAndFolders: getFilesAndFolders,
			getGroup: getGroup,
			getGroups: getGroups,
			getParentsAndChilds: getParentsAndChilds,
			getAllPermissions: getAllPermissions,
			getPermissions: getPermissions,
			getPermissionsForUser: getPermissionsForUser,
			getUser: getUser,
			getUserPermits: getUserPermits,
			getUsers: getUsers,
			getVersion: getVersion,
			getVersions: getVersions,
			home: home,
			initialConfigData: initialConfigData,
			listElements: listElements,
			login: login,
			logout: logout,
			move: move,
			notificationsCallback: notificationsCallback,
			notificationsInitialization: notificationsInitialization,
			notificationsUnsubscribe: notificationsUnsubscribe,
			photo: photo,
			restore: restore,
			search: search,
			searchGroup: searchGroup,
			searchUser: searchUser,
			send: send,
			setFavourite: setFavourite,
			setRevised: setRevised,
			share: share,
			trash: trash,
			updateAccount: updateAccount,
			updatePermissions: updatePermissions,
			updatePermissionsForUserGroup: updatePermissionsForUserGroup,
			updateVersion: updateVersion,
			uploadFiles: uploadFiles,
		}
	})();	
};

Talk4UService.prototype = new I4UService();
Talk4UService.prototype.setState = function(state){
	this.service=this.states[state];
};
Talk4UService.prototype.sync = function(){
	var _this = this;
	if(Ti.Network.online){
		this.setState('online');
	}else{
		this.setState('offline');
	}
	Ti.Network.addEventListener('change', function(e){
		if (e.online) {
			_this.setState('online');
			Ti.App.fireEvent('talk4u:connectionStateChanged', {'state': 'online'});
		} else {
			_this.setState('offline');
		}
	});
};
Talk4UService.prototype.CRUDOperation = function(serviceName, params_data){
	/*
	var _Talk4UDAl = require('/classes/Talk4UDal');
	Talk4UDAl = new _Talk4UDAl();
	var statement = Talk4UDAl.getStatement(serviceName, params_data);
	if (params_data && params_data.fn_callback)
		Talk4UDAl.execStatement(statement, params_data.fn_callback);
	else
		return Talk4UDAl.execStatement(statement, null);
	*/
};
Talk4UService.prototype.execService = function(serviceName, params){
	
	try {
		this.setState('online');
		this.service[serviceName](params);
	} catch (exception) {
		
	}
	
};
module.exports=Talk4UService;






