function Box4UDal(){}
Box4UDal.prototype.getStatement = function(serviceName, params){
	Ti.include('/classes/service/box4u/methods/Box4UMethodsDal.js');
	try {
		return eval(serviceName)(params);
	} catch (exception) {
		return null;
	}
};
Box4UDal.prototype.execStatement = function(statement, fn_callback){
	// Ti.API.info('execStatement BEGIN: '+ timestamp());
	if (isArray(statement) && statement.length > 0) {
		// TRANSACTIONAL COMMIT
		try {
			// Open database
			var result=null;
			var db = Ti.Database.open(glb.config.dbname);
			// Ti.API.info('starting BEGIN: '+ timestamp());
			db.execute('BEGIN;');
			// Ti.API.info('starting INSERTS: '+ timestamp());
			for (var i in statement) {
				db.execute(statement[i]);
			}
			// Ti.API.info('finished INSERTS: '+ timestamp());
			// Ti.API.info('starting COMMIT: '+ timestamp());
			db.execute('COMMIT;');
			// Ti.API.info('finished COMMIT: '+ timestamp());
		} catch(e) {
			trace('Box4UDal transactional statement:'+e.message,'error');
		}
		finally {
			db.close();
		}
		
	} else if (statement && statement.length > 0) {
		var error = false;
		try {
			var isSelect = statement.indexOf("SELECT");
			// Open database
			var result=null;
			var db = Ti.Database.open(glb.config.dbname);
			if (isSelect !== -1){
				// Ti.API.info('starting SELECT: '+ timestamp());
				var resultTMP = [];
				var rows = db.execute(statement);
				var cont = 0;
				while (rows.isValidRow()){
					resultTMP[cont] = {};
					for (var i=0, len=rows.fieldCount(); i<len; i++){
						resultTMP[cont][rows.fieldName(i)] = rows.field(i);
					}
					rows.next();
					cont++;
				}
				rows.close();
				if (resultTMP.length >= 0){
					result = resultTMP.slice(0);
					resultTMP = null;
				}
			} else {
				db.execute(statement);
			}
		} catch(e) {
			trace('Box4UDal statement:'+e.message,'error');
			error = true;
		}
		finally {
			db.close();
			// Ti.API.info('finished SELECT: '+ timestamp());
			if (fn_callback && !error)
				fn_callback(result);
			else
				return result;
		}
	}
	return false;
	// Ti.API.info('execStatement END: '+ timestamp());
};
module.exports=Box4UDal;