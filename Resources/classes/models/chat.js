/**
 * @author Administrador
 */
function chat(data) {

	var conversacion= new Object();
	conversacion.id = (data.id) ? data.id : "";
	conversacion.lastActive = (data.lastActive) ? data.lastActive : "";
	conversacion.created = (data.created) ? data.created : "";
	conversacion.image = "";
	conversacion.topic = (data.topic) ? data.topic : "conversacion";
	conversacion.xmppjid= (data.xmppjid)? data.xmppjid:"";
	conversacion.jid= (data.jid)? data.jid:"";
	conversacion.participants= new Array();
	
	for (var i=0; i < data.participants.length; i++) {
	  conversacion.participants[i] = new Object();
	  conversacion.participants[i].id = data.participants[i].id;
	  conversacion.participants[i].name= data.participants[i].name;
	  conversacion.participants[i].jid= data.participants[i].jid;

	}
	
	conversacion.mensajes = new Array();
	
		/*
		var chat =[
		{propietario: "Carlos Tirado", fecha: "13.22", contenido: "Ei carlos! Como va talk4u"},
		{propietario: "Carlos Sáenz", fecha: "13.22", contenido: "Buenas carlos, muy bien aqui estoy dandole duro"},
		{propietario: "Carlos Tirado", fecha: "13.22", contenido: "muy bien me parece, a ver si hacemos una buena aplicacion"},
		{propietario: "Carlos Tirado", fecha: "13.22", contenido: "muy bien me parece, a ver si hacemos una buena aplicacion"},
		{propietario: "Carlos Sáenz", fecha: "13.22", contenido: "claro que si!!"}
		];
		*/
		
	return conversacion;
}

module.exports = chat; 