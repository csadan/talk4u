var I4UForm = require('classes/I4UForm');
function Talk4UForm() {};

Talk4UForm.prototype=new I4UForm();

Talk4UForm.prototype.getMainWindows = function(){
	var forms=[
		talk4u.formFactory.createForm('Talk4UChatsForm',{openStyle: 'inTab'}),
		talk4u.formFactory.createForm('Talk4UUsersForm',{openStyle: 'inTab'}),
		talk4u.formFactory.createForm('Talk4UStateForm',{openStyle: 'inTab'}),
		talk4u.formFactory.createForm('Talk4USearchForm',{openStyle: 'inTab'}),
		// talk4u.formFactory.createForm('Talk4UAccountForm',{openStyle: 'inTab'})
	];
	
	var titles=[
		L('Chats'),
		L('Usuarios'),
		L('Estado'),
		L('Buscar'),
		// L('Cuenta')
	];
	
	var icons=[
		'images/talk4u/iconos_tabbar/icons-gray/talk4u.png',
		'images/talk4u/iconos_tabbar/icons-gray/usuarios.png',
		'images/talk4u/iconos_tabbar/icons-gray/subir.png',
		'images/talk4u/iconos_tabbar/icons-gray/buscar.png',
		// 'images/talk4u/iconos_tabbar/icons-gray/ajustes.png',
	];
	
	return {
		'forms': forms,
		'titles': titles,
		'icons': icons
	};
	
};
Talk4UForm.prototype.createMainWindow = function(params) {
		switch(glb.device) {
		case 'iphone':
			glb.tabgroup = this.createTabgroup(params);
			var mainWindow = Ti.UI.createWindow();
			mainWindow.addEventListener('open',function() {
				glb.tabgroup.open();
			});
			mainWindow.add(glb.tabgroup);
		break;
		case 'androidTablet':
			
		break;
		case 'ipad':
			// WINDOWS
			glb.tabgroup = this.createTabgroup(params);
			var detailWindow = Ti.UI.createWindow({
				backgroundImage: (glb.config.theme.mainWindowBackgroundImage)?glb.config.theme.mainWindowBackgroundImage:null,
				barColor: (glb.config.theme.color1)?glb.config.theme.color1:null,
				barImage:(glb.config.theme.navBarImage)?glb.config.theme.navBarImage:null,
			});
			// MASTER NAV GROUP
			var masterNav = Ti.UI.iPhone.createNavigationGroup({
				window : glb.tabgroup
			});
			// DETAIL NAV GROUP
			var detailNav = Ti.UI.iPhone.createNavigationGroup({
				window : detailWindow
			});
			
			detailNav.fn_close = function () {
				if (mainWindow.detailView.window.children[0]) {
					if (typeof mainWindow.detailView.window.children[0].fn_close === 'function') {
						mainWindow.detailView.window.children[0].close();
						mainWindow.detailView.window.remove(mainWindow.detailView.window.children[0]);
					}
					mainWindow.detailView.window.setRightNavButton(null);
					mainWindow.detailView.window.title='';
				}
			};
			detailNav.fn_open = function (form) {
				mainWindow.detailView.window.title=form.title;
				if(form.toolBar){
					mainWindow.detailView.window.rightNavButton = form.toolBar;
				}
				form.open() ;
				mainWindow.detailView.window.add(form);
			};
			detailNav.fn_setNavBar = function (t) {
				mainWindow.detailView.window.rightNavButton = t;
			};
			
			// SPLIT VIEW
			var mainWindow = Titanium.UI.iPad.createSplitWindow({
				masterView : glb.tabgroup, //izq
				detailView : detailNav, //derecha
			});
			mainWindow.addEventListener('open',function() {
				//glb.Box4ULogin.close();
				//glb.Box4ULogin=null;
				glb.tabgroup.open();
			});
			
			// TODO: Workaround for Titanium bug
			Ti.App.addEventListener.addEventListener('app:rowClicked', function(e) {
				//Ti.API.log('setMasterPopupVisible');
				// see bug in lighthouse
				// <a href="https://appcelerator.lighthouseapp.com/projects/32238/tickets/2300-hide-master-popover-on-ipad" data-bitly-type="bitly_hover_card">https://appcelerator.lighthouseapp.com/projects/32238/tickets/2300-hide-master-popover-on-ipad</a>
				mainWindow.setMasterPopupVisible(false);
				mainWindow.setMasterPopupVisible(true);
			});
			
			// SPLIT VIEW EVENT LISTENER
			mainWindow.addEventListener('visible',function(e) {
				//trace('visible');
				if (e.view == 'detail' && !Ti.Gesture.isLandscape()) {
					// show master list when in detail mode via left nav button
					e.button.title = L('home');
					mainWindow.detailView.window.leftNavButton = e.button;
				} else if (e.view == 'master' || Ti.Gesture.isLandscape()) {
					mainWindow.detailView.window.leftNavButton = null;
				}
			});
		break;
		default://Android
			var mainWindow = Ti.UI.createWindow();
			mainWindow.addEventListener('open',function() {
				glb.Box4ULogin.close();
				glb.Box4ULogin=null;
				glb.tabgroup.open();
			});
			glb.tabgroup = this.createTabgroup(params);
			mainWindow.add(glb.tabgroup);
	}
	return mainWindow;
};
Talk4UForm.prototype.createTabgroup = function(params) {
	var self = Ti.UI.createTabGroup();
	//create app main windows
	var tabs = [];
	for (var i in params.forms) {
		var tab = Ti.UI.createTab({
			window: params.forms[i],
			title: params.titles[i],
			icon: params.icons[i],
		});
		self.addTab(tab);
	}
	switch(glb.device) {
		case 'iphone':
		
		break;
		case 'ipad':
			
		break;
		case 'androidTablet':
			
		break;
		default://Android
			
	}
	return self;
};
Talk4UForm.prototype.createTextField = function(params) {
	var textField = Ti.UI.createTextField({
	    color: (params && params.color)?params.color:'#000',
	    top: (params && params.top!==null && typeof params.top!=='undefined')?params.top:25,
	    left:(params && params.left)?params.left:null,
		right: (params && params.right)?params.right:null,
		bottom: (params && params.bottom!==null && typeof params.bottom!=='undefined')?params.bottom:null,
	   	width: (params && params.width)?params.width:Ti.UI.FILL,
	    height: (params && params.height)?params.height:50,
	    hintText: (params && params.hintText)?params.hintText:'',
	    passwordMask:(params && params.passwordMask)?params.passwordMask:false,
	    autocorrect:(params && params.autocorrect)?params.autocorrect:false,
	    keyboardType:(params && params.keyboardType)?params.keyboardType:Ti.UI.KEYBOARD_DEFAULT,
	    returnKeyType: (params && params.returnKeyType)?params.returnKeyType:Ti.UI.RETURNKEY_DEFAULT,
	    borderStyle: (params && params.borderStyle)?params.borderStyle:Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
	    itemId: (params && params.itemId)?params.itemId:undefined,
	    value: (params && params.value) ? params.value:null,
	});
	switch(glb.device) {
		case 'iphone':
		
		break;
		case 'ipad':
			
		break;
		case 'androidTablet':
			
		break;
		default://Android
			
	}
	return textField;
};
Talk4UForm.prototype.createNavigationGroup = function(params) {
	if (!glb.navGroup)
		glb.navGroup = [];
	glb.navGroup.push(Ti.UI.iPhone.createNavigationGroup({
		window : params.win
	}));
	switch(glb.device) {
		case 'iphone':
			
		break;
		case 'ipad':
			
		break;
		case 'androidTablet':
			
		break;
		default://Android
			
	}
	return glb.navGroup.length-1;
};
Talk4UForm.prototype.createButton = function(params) {
	var button = Ti.UI.createButton({
		image: (params && params.image)?params.image:undefined,
		width: (params && params.width)?params.width:Ti.UI.SIZE,
		height: (params && params.height)?params.height:Ti.UI.SIZE,
		title: (params && params.title)?params.title:undefined,
		top: (params && params.top!==null && typeof params.top!=='undefined')?params.top:undefined,
		left: (params && params.left)?params.left:undefined,
		right: (params && params.right)?params.right:undefined,
		bottom: (params && params.bottom!==null && typeof params.bottom!=='undefined')?params.bottom:undefined,
		borderWidth: (params && params.borderWidth)?params.borderWidth:undefined,
		borderRadius: (params && params.borderRadius)?params.borderRadius:undefined,
		backgroundColor: (params && params.backgroundColor)?params.backgroundColor:undefined,
		backgroundImage: (params && params.backgroundImage)?params.backgroundImage:undefined,
		font: (params && params.font)?params.font:undefined,
		enabled: (params && params.enabled)?params.enabled:undefined
	});

	switch(glb.device) {
		case 'iphone':
			if(params && params.title) {
				if(params.style==='bordered')
					button.style = Titanium.UI.iPhone.SystemButtonStyle.BORDERED
				else if(params.style==='plain')
					button.style = Titanium.UI.iPhone.SystemButtonStyle.PLAIN
			}
		break;
		case 'ipad':
			if(params && params.title) {
				if(params.style==='bordered')
					button.style = Titanium.UI.iPhone.SystemButtonStyle.BORDERED
				else if(params.style==='plain')
					button.style = Titanium.UI.iPhone.SystemButtonStyle.PLAIN
			}
		break;
		case 'androidTablet':
			
		break;
		default://Android
			
	}
	return button;
};
Talk4UForm.prototype.createTableView = function(params) {
	var tableView = Ti.UI.createTableView({
		width: (params && params.width)?params.width:undefined,
		height: (params && params.height)?params.height:undefined,
		editionMode: (params && typeof params.editionMode!='undefined')?params.editionMode:null,
		backgroundColor: (params && params.backgroundColor)?params.backgroundColor:undefined,
		top: (params && params.top!==null && typeof params.top!=='undefined')?params.top:undefined,
		bottom: (params && params.bottom!==null && typeof params.bottom!=='undefined')?params.bottom:undefined,
		borderWidth : params.borderWidth
	});
	switch (glb.device) {
		case 'iphone':
			tableView.scrollable = params.scrollable==null;
			if (params && params.style==='grouped') {
				tableView.style = Ti.UI.iPhone.TableViewStyle.GROUPED;
			} else {
				tableView.style = Ti.UI.iPhone.TableViewStyle.PLAIN;
				tableView.left = (tableView.editionMode===false)?-30:0;
				tableView.right = 0;
			}
			if (params && params.fn_dinamicScroll) {
				tableView.addEventListener('scroll',function(e){
					// If we're on android: our total number of rows is less than the first visible row plus the total number of visible
				    // rows plus 3 buffer rows, we need to load more rows!
				    if (e.contentOffset.y + e.size.height + 100 > e.contentSize.height) {
				    	// add the new rows
				    	setTimeout(function(){
				    		 params.fn_dinamicScroll();
				    	}, 1000);
				    }
				});
			}
			if (params && params.dinamicEdit) {
				tableView.addEventListener('TableView:editSelf', function(p) {
					tableView.setTouchEnabled(false);
					tableView.applyProperties({
						bottom: (p.toolBar)? p.toolBar.toImage().height: 0,
						editionMode: true,
					});
					var animation = Titanium.UI.createAnimation();
					animation.left = 0;
					animation.right = -35;
					animation.duration = 500;
					animation.addEventListener('complete',function() {
						if(p && p.unBlock) {
							p.unBlock.forEach(function(elem) {
								elem.setTouchEnabled(true);
								elem.setEnabled(true);
							});
						}
						tableView.setTouchEnabled(true);
						animation.removeEventListener('complete',function() {});
					});
					tableView.animate(animation);
				});
				tableView.addEventListener('TableView:editSelfCancel', function(p) {
					tableView.setTouchEnabled(false);
					tableView.applyProperties({
						bottom: 0,
						editionMode: false,
					});
					var animation = Titanium.UI.createAnimation();
					animation.left = -30;
					animation.right = 0;
					animation.duration = 500;
					animation.addEventListener('complete',function() {
						for (var i in p.tableViewData) {
							p.tableViewData[i].editImage.image = 'images/unchecked.png';
							p.tableViewData[i].isChecked = false;
						}
						if(p && p.unBlock) {
							p.unBlock.forEach(function(elem) {
								elem.setTouchEnabled(true);
								elem.setEnabled(true);
							});
						}
						tableView.setTouchEnabled(true);
						animation.removeEventListener('complete',function() {});
					});
					tableView.animate(animation);
				});
			}
		break;
		case 'ipad':
			tableView.scrollable = params.scrollable==null;
			if (params && params.style==='grouped') {
				tableView.style = Ti.UI.iPhone.TableViewStyle.GROUPED;
			} else {
				tableView.style = Ti.UI.iPhone.TableViewStyle.PLAIN;
				tableView.left = (tableView.editionMode===false)?-30:0;
				tableView.right = 0;
			}
			if (params && params.fn_dinamicScroll) {
				tableView.addEventListener('scroll',function(e){
					// If we're on android: our total number of rows is less than the first visible row plus the total number of visible
				    // rows plus 3 buffer rows, we need to load more rows!
				    if (e.contentOffset.y + e.size.height + 100 > e.contentSize.height) {
				    	// add the new rows
				        params.fn_dinamicScroll();
				    }
				});
			}
			if (params && params.dinamicEdit) {
				tableView.addEventListener('TableView:editSelf', function(p) {
					tableView.setTouchEnabled(false);
					var animation = Titanium.UI.createAnimation();
					animation.left = 0;
					animation.right = -35;
					animation.duration = 500;
					animation.addEventListener('complete',function() {
						if(p && p.unBlock) {
							p.unBlock.forEach(function(elem) {
								elem.setTouchEnabled(true);
								elem.setEnabled(true);
							});
						}
						tableView.setTouchEnabled(true);
						animation.removeEventListener('complete',function() {});
						tableView.applyProperties({
							bottom: (p.toolBar)? p.toolBar.toImage().height: 0,
							editionMode: true,
						});
					});
					tableView.animate(animation);
				});
				tableView.addEventListener('TableView:editSelfCancel', function(p) {
					tableView.setTouchEnabled(false);
					var animation = Titanium.UI.createAnimation();
					animation.left = -30;
					animation.right = 0;
					animation.duration = 500;
					animation.addEventListener('complete',function() {
						for (var i in p.tableViewData) {
							p.tableViewData[i].editImage.image = 'images/unchecked.png';
							p.tableViewData[i].isChecked = false;
						}
						if(p && p.unBlock) {
							p.unBlock.forEach(function(elem) {
								elem.setTouchEnabled(true);
								elem.setEnabled(true);
							});
						}
						tableView.setTouchEnabled(true);
						animation.removeEventListener('complete',function() {});
						tableView.applyProperties({
							bottom: 0,
							editionMode: false,
						});
					});
					tableView.animate(animation);
				});
			}
		break;
		case 'androidTablet':
			if (params && params.fn_dinamicScroll) {
				tableView.addEventListener('scroll',function(e){
					// If we're on android: our total number of rows is less than the first visible row plus the total number of visible
				    // rows plus 3 buffer rows, we need to load more rows!
				    if (e.totalItemCount < e.firstVisibleItem + e.visibleItemCount + 3) {
				       // add the new rows
				        params.fn_dinamicScroll();
				    }
				});
			}
		break;
		default://Android
			if (params && params.fn_dinamicScroll) {
				tableView.addEventListener('scroll',function(e){
					// If we're on android: our total number of rows is less than the first visible row plus the total number of visible
				    // rows plus 3 buffer rows, we need to load more rows!
				    if (e.totalItemCount < e.firstVisibleItem + e.visibleItemCount + 3) {
				        // tell our interval (above) to load more rows
				        //loadData = true;
				        // add the new rows
				        params.fn_dinamicScroll();
				    }
				});
			}
	}
	if (params && params.data)
		tableView.setData(params.data);
	
	return tableView;
};
Talk4UForm.prototype.createToolBar = function(params) {	
	var _this=this;
	var toolBar;
	
	switch (glb.device) {
		case 'iphone':
			// iPhone style toolBar at bottom
			var setItems = function(pItems) {
				var items=[];
				var len = pItems.length;  
				for(var i=0; i<len; i++) {
					var element;
					if (pItems[i].image || pItems[i].title) {//button
						pItems[i].style='bordered';
						element = _this.createButton(pItems[i]);
						if (pItems[i].fn_click)
							element.addEventListener('click',pItems[i].fn_click);
					} else if (pItems[i].label ) {//label
						element = Ti.UI.createLabel({
							text: pItems[i].label,
							textAlign: 'center',
							color: 'white',
							font: {
								fontSize: 12,
								fontWeight: 'bold'
							},
							width: Ti.UI.SIZE,
							height: Ti.UI.SIZE
						});
					}else{
						element= Ti.UI.createTextField({//textfield
											borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
										    hintText : pItems[i].hintText,
										    keyboardToolbar : pItems[i].keyboardToolbar,
										    keyboardToolbarColor : '#999',
										    keyboardToolbarHeight : 40,
										    top : 10,  
										    height: 32,
										    width: 175
						});		
					}
					items.push(element);
				}
				var flexSpace = Titanium.UI.createButton({
			    	systemButton: Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE,
				});
				
				var itemsWithFlex = [];
				var len = items.length;
				
				if (len!==1) {
					for (var i=0,len=items.length; i<len; i++) {
						if(i!==0) itemsWithFlex.push(flexSpace);
						itemsWithFlex.push(items[i]);
					}
				} else {
					itemsWithFlex.push(flexSpace);
					itemsWithFlex.push(items[0]);
					itemsWithFlex.push(flexSpace);
				}
			toolBar.items = itemsWithFlex;
			}
			
			toolBar = Titanium.UI.iOS.createToolbar({
				pos: (params.pos)?params.pos:"bottom",
				bottom: 0,
			    borderTop: (params.borderTop)?true:false,
			    borderBottom: (params.borderBottom)?true:false,
			    setCustomItems: setItems,
			   	barColor: (glb.config.theme.color1)? glb.config.theme.color1 : null
			});
			if (params.items){
					setItems(params.items);
			}				
		break;
		case 'ipad':
		if (params.winOpenStyle!=='inDetail' || params.pos==='bottom') {
						var setItems = function(pItems) {
							var items=[]; 
							for (var i=0,len=pItems.length; i<len; i++) {
								var element;
								if (pItems[i].image || pItems[i].title) {
									pItems[i].style= 'bordered';
									element = _this.createButton(pItems[i]);
									if (pItems[i].fn_click){
											element.addEventListener('click',pItems[i].fn_click);
									}
								} else if (pItems[i].label) {
									element = Ti.UI.createLabel({
										text: pItems[i].label,
										textAlign: 'center',
										color: 'white',
										font: {
											fontSize: 12,
											fontWeight: 'bold'
										},
										width: Ti.UI.SIZE,
										height: Ti.UI.SIZE
									});
								}
								items.push(element);
							}
							var flexSpace = Titanium.UI.createButton({
						    	systemButton: Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE,
							});
							
							var itemsWithFlex = [];
							var len = items.length;
							if(len!==1) {
								for(var i=0,len=items.length; i<len; i++) {
									if(i!==0) itemsWithFlex.push(flexSpace);
									itemsWithFlex.push(items[i]);
								}
							}else{
								itemsWithFlex.push(flexSpace);
								itemsWithFlex.push(items[0]);
								itemsWithFlex.push(flexSpace);
							}
							toolBar.items = itemsWithFlex;
						}
						toolBar = Titanium.UI.iOS.createToolbar({
							pos: 'bottom',
							bottom: 0,
						    borderTop: (params.borderTop)?true:false,
						    borderBottom: (params.borderBottom)?true:false,
						    barColor: (glb.config.theme.color1)?glb.config.theme.color1:null,
						    setCustomItems: setItems
						});
						if (params.items) setItems(params.items);
					
					}else {
						// It is inDetail so set Toolbar in NavBar
						var setItems = function(pItems) {
							toolBar = Ti.UI.createView({
							    layout: 'horizontal',
							    width: Ti.UI.SIZE,
							    height: Ti.UI.SIZE,
							    right: 0,
							});
							var items=[]; 
							for(var i=0,len=pItems.length; i<len; i++) {
								var element;
								if(pItems[i].image || pItems[i].title) {
									element = Ti.UI.createImageView({
										image: pItems[i].image,
										height: Ti.UI.SIZE,
										width: Ti.UI.SIZE,
										left: 40,
										opacity: (pItems[i].enabled===false)?0.5:1
									});
									if (pItems[i].fn_click)	element.addEventListener('click',pItems[i].fn_click);
								
								}else if (pItems[i].label) {
									element = Ti.UI.createLabel({
										text: pItems[i].label,
										textAlign: 'center',
										color: '#000',
										font: {
											fontSize: 12,
											fontWeight: 'normal'
										},
										width: Ti.UI.SIZE,
										height: Ti.UI.SIZE
									});
								}
								items.push(element);
							}
							for(var i=0,len=items.length; i<len; i++) {
								var buttonView = Ti.UI.createView({
									width: Ti.UI.SIZE,
							    	height: Ti.UI.SIZE,
								});
								buttonView.add(items[i]);
								toolBar.add(buttonView);
							}
							glb.mainWindow.detailView.fn_setNavBar(toolBar);
						}
						toolBar = Ti.UI.createView({
						    layout: 'horizontal',
						    width: Ti.UI.SIZE,
						    height: Ti.UI.SIZE,
						    right: 0,
						    setCustomItems: setItems
						});
						if (params.items) setItems(params.items);
				}
				break;
		case 'androidTablet':
			
		break;
		
		
		default://Android
		
	}
	return toolBar;
};
Talk4UForm.prototype.createText11TableViewRow = function(params) {
	
	var row = Ti.UI.createTableViewRow({
		itemId: (params.itemId)?params.itemId:null,
		title: (params.title!=undefined)?params.title:null,
		itemCode: (params.itemCode)?params.itemCode:null,
		leftImage: (params.leftImage)?params.leftImage:'',
		hasChild: (params.hasChild)?true:false,
		hasDetail: (params.hasDetail)?true:false,
		hasCheck: (params.hasCheck)?true:false,
		selectionStyle: (params.selectionStyle)?params.selectionStyle:Ti.UI.iPhone.TableViewCellSelectionStyle.BLUE,
		itemType: (params.itemType)?params.itemType:null,
		rightText: (params.rightText)?params.rightText:'',
		idTitle: (params.title)?params.title:'',
		origin: (params.origin)?params.origin:'',
		switchValue: (params.value!==undefined)?params.value:null,
		width: 320,
		height: 50,
		className: 'text11Row',
		font: {fontSize: 14, fontWeight: 'normal'},
	});
	row.photoId= (params.photoId)?params.photoId:'';
	row.addEventListener("downloaded",function(e){
		row.leftImage = e.localFilePath;
	});
	
	switch(glb.device) {
		case 'iphone':
			
		break;
		case 'ipad':
			
		break;
		case 'androidTablet':
			
		break;
		default://Android
	}

	return row;
};
Talk4UForm.prototype.createText22TableViewRow = function(params) {
	function checkResize(leftObject, rightObject) {
		var rightMargin = (params.hasChild)?20:0;
		var leftMargin = (params.leftImage)?50:0;
		var availableWidth = row.width - 15 - leftMargin - rightMargin - ((params.hasChild || params.hasDetail || params.hasCheck)?20:0);
		// trace('***********************************');
		// trace('availableWidth: '+availableWidth);
		var leftLabelWidth = leftObject.toImage().width;
		var rightLabelWidth = rightObject.toImage().width;
		// trace('leftLabelWidth: '+leftLabelWidth);
		// trace('rightLabelWidth: '+rightLabelWidth);
		var totalWidth = leftLabelWidth + rightLabelWidth;
		// trace('totalWidth: '+totalWidth);
		if(availableWidth<(totalWidth+10)) {
			var quarter = Math.round(availableWidth/4);
			// trace('quarter: '+quarter);
			if(rightLabelWidth <= (quarter-10)) {
				leftObject.width = availableWidth - (rightLabelWidth+10);
			}else if(leftLabelWidth > (quarter*3)) {
				leftObject.width = (quarter*3);
				rightObject.width = (quarter-10);
			}else {
				rightObject.width = availableWidth - (leftLabelWidth+10);
			}
		}
		// trace('leftObject.width: '+leftObject.width);
		// trace('rightObject.width: '+rightObject.width);
	}
	var row = Ti.UI.createTableViewRow({
		itemId: (params.itemId)?params.itemId:null,
		leftImage: (params.leftImage)?params.leftImage:'',
		hasChild: (params.hasChild)?true:false,
		hasDetail: (params.hasDetail)?true:false,
		hasCheck: (params.hasCheck)?true:false,
		// width: params.win.toImage().width,
		width: 320,
		height: params.height?params.height:50,
		itemType: params.itemType?params.itemType:null,
		className: 'text22Row',
	});
	if(params.editable) {
		row.addEventListener('row:valueChanged',function(data) {
			if(data.rowId==='from') {
				topRightTextLabel.text = data.value;
				row.dateFrom = data.value;
			}else if(data.rowId==='to') {
				bottomRightTextLabel.text = data.value;
				row.dateTo = data.value;
			}
		});
	}
	// Independent to all createXXXTableViewRow
	var rowContentView = Ti.UI.createView({
		layout: 'vertical',
		left: 10,
		right: 5,
		height: Ti.UI.SIZE
		// top: 20,
		// bottom: 20,
	});
	var topTextView = Ti.UI.createView({
		left: (params.leftImage)?50:0,
		width: Ti.UI.FILL,
		height: Ti.UI.SIZE,
	});
	var topLeftTextLabel = Ti.UI.createLabel({
		text: (params.topLeftText)?params.topLeftText:'',
		textAlign: 'left',
		color: (params.color)?params.color:'#000',
		font: {fontSize: 16, fontWeight: 'bold'},
		width: Ti.UI.SIZE,
		height: 24,
		left: 0
	});
	var topRightTextLabel = Ti.UI.createLabel({
		text: (params.topRightText)?params.topRightText:'',
		textAlign: 'right',
		color: (params.color)?params.color:'#000',
		font: {fontSize: 14, fontWeight: 'normal'},
		width: Ti.UI.SIZE,
		height: 24,
		right: 0
	});
	var bottomTextView = Ti.UI.createView({
		left: (params.leftImage)?50:0,
		width: Ti.UI.FILL,
		height: Ti.UI.SIZE,
	});
	var bottomLeftTextLabel = Ti.UI.createLabel({
		text: (params.bottomLeftText)?params.bottomLeftText:'',
		textAlign: 'left',
		color: (params.color)?params.color:'#385487',
		font: {fontSize: 16, fontWeight: 'bold'},
		width: Ti.UI.SIZE,
		height: 24,
		left: 0
	});
	var bottomRightTextLabel = Ti.UI.createLabel({
		text: (params.bottomRightText)?params.bottomRightText:'',
		textAlign: 'right',
		color: (params.color)?params.color:'#385487',
		font: {fontSize: 14, fontWeight: 'normal'},
		width: Ti.UI.SIZE,
		height: 24,
		right: 0
	});
	switch(glb.device) {
		case 'iphone':
		
		break;
		case 'ipad':
			
		break;
		case 'androidTablet':
			
		break;
		default://Android
	}
	topTextView.add(topLeftTextLabel);
	topTextView.add(topRightTextLabel);
	bottomTextView.add(bottomLeftTextLabel);
	bottomTextView.add(bottomRightTextLabel);
	rowContentView.add(topTextView);
	rowContentView.add(bottomTextView);
	row.add(rowContentView);
	
	checkResize(topLeftTextLabel, topRightTextLabel);
	checkResize(bottomLeftTextLabel, bottomRightTextLabel);
	
	return row;
};
Talk4UForm.prototype.createSearchTableViewRow = function(params) {
	function checkResize(leftObject, rightObject) {
		var rightMargin = (params.hasChild)?20:0;
		var leftMargin = (params.leftImage)?50:0;
		var availableWidth = row.width - 15 - leftMargin - rightMargin - ((params.hasChild || params.hasDetail || params.hasCheck)?20:0);
		//trace('***********************************');
		//trace('availableWidth: '+availableWidth);
		var leftLabelWidth = leftObject.toImage().width;
		var rightLabelWidth = rightObject.toImage().width;
		//trace('leftLabelWidth: '+leftLabelWidth);
		//trace('rightLabelWidth: '+rightLabelWidth);
		var totalWidth = leftLabelWidth + rightLabelWidth;
		//trace('totalWidth: '+totalWidth);
		if(availableWidth<(totalWidth+10)) {
			var quarter = Math.round(availableWidth/4);
			//trace('quarter: '+quarter);
			if(rightLabelWidth <= (quarter-10)) {
				leftObject.width = availableWidth - (rightLabelWidth+10);
			}else if(leftLabelWidth > (quarter*3)) {
				leftObject.width = (quarter*3);
				rightObject.width = (quarter-10);
			}else {
				rightObject.width = availableWidth - (leftLabelWidth+10);
			}
		}
		//trace('leftObject.width: '+leftObject.width);
		//trace('rightObject.width: '+rightObject.width);
	}
	var row = Ti.UI.createTableViewRow({
		itemId: (params.itemId)?params.itemId:null,
		itemValue: (params.itemValue!=undefined)?params.itemValue:null,
		itemCode: (params.itemCode)?params.itemCode:null,
		leftImage: (params.leftImage)?params.leftImage:'',
		hasChild: (params.hasChild)?true:false,
		hasDetail: (params.hasDetail)?true:false,
		hasCheck: (params.hasCheck)?true:false,
		selectionStyle: (params.selectionStyle)?params.selectionStyle:Ti.UI.iPhone.TableViewCellSelectionStyle.BLUE,
		itemType: (params.itemType)?params.itemType:null,
		rightText: (params.rightText)?params.rightText:'',
		idTitle: (params.title)?params.title:'',
		origin: (params.origin)?params.origin:'',
		switchValue: (params.value!==undefined)?params.value:null,
		// width: params.win.toImage().width,
		width: 320,
		height: 50,
		className: 'text11Row',
	});
	if(params.editable) {
		row.addEventListener('row:valueChanged',function(data) {
			if (row.itemValue)
				row.itemValue = data.value;
			else 
				rightElement.text = data.value;
			row.width = params.win.toImage().width;
			checkResize(titleLabel, rightElement);
		});
	}
	var rowContentView = Ti.UI.createView({
		left: 10,
		right: 5,
		height: Ti.UI.SIZE,
	});
	// Independent to all createXXXTableViewRow
	var titleLabel = Ti.UI.createLabel({
		text: (params.title)?params.title:'',
		color: (params.color)?params.color:'#000',
		font: {fontSize: 16, fontWeight: 'bold'},
		width: Ti.UI.SIZE,
		height: 24,
		left: (params.leftImage)?50:0
	});
	row.itemName=titleLabel.text;
	var rightElement;
	if(params.value!==undefined) {
		rightElement = Ti.UI.createSwitch({
			value: params.value,
			width: Ti.UI.SIZE,
			height: Ti.UI.SIZE,
			right: 0,
			enabled: (params.enabled===false)?false:true,
		});
		row.rightElement=rightElement;
		rightElement.addEventListener('change',function(e) {
			row.fireEvent('row:switchChanged',e);
			row.itemValue = e.value;
			row.switchValue = e.value;
			// TODO: This function about changing glb.searchParams should be somewhere in box4u.module..
			if(params.itemId==='searchInBody') {
				glb.searchParams.searchInBody=e.value;
			}else if(params.itemId==='searchInVersions') {
				glb.searchParams.searchInVersions=e.value;
			}else if(params.itemId==='advancedSearch') {
				glb.searchParams.advancedSearch=e.value;
			}
		});
	}else {
		rightElement = Ti.UI.createLabel({
			text: (row.rightText)?row.rightText:'',
			textAlign: 'right',
			color: (params.color)?params.color:'#385487',
			font: {fontSize: 15, fontWeight: 'normal'},
			width: Ti.UI.SIZE,
			height: 24,
			right: 0
		});
	}
	switch(glb.device) {
		case 'iphone':
			
		break;
		case 'ipad':
			
		break;
		case 'androidTablet':
			
		break;
		default://Android
	}
	rowContentView.add(titleLabel);
	rowContentView.add(rightElement);
	row.add(rowContentView);
	
	checkResize(titleLabel, rightElement);
	
	return row;
};

Talk4UForm.prototype.createSegmentedControl = function(params) {
	var segmentedControl;
	switch(glb.device) {
		case 'iphone':
			var labels=[];
			for (var i in params.items)
				labels.push(params.items[i]);
				segmentedControl = Titanium.UI.iOS.createTabbedBar({
					backgroundColor: (glb.config.theme.color2)?glb.config.theme.color2:null,
				    labels: labels,
			        style: (params.style) ? params.style : Titanium.UI.iPhone.SystemButtonStyle.BAR,
			        index: (params.index) ? params.index : 0
				});
			segmentedControl.addEventListener('click', function(e) {
				if(e.index>=0)
					params.items[e.index].fn_click();
			});
		break;
		case 'ipad':
			var labels=[];
			for (var i in params.items)
				labels.push(params.items[i]);
				segmentedControl = Titanium.UI.iOS.createTabbedBar({
					backgroundColor: (glb.config.theme.color2)?glb.config.theme.color2:null,
				    labels: labels,
			        style: (params.style) ? params.style : Titanium.UI.iPhone.SystemButtonStyle.BAR,
			        index: (params.index) ? params.index : 0
				});
				segmentedControl.addEventListener('click', function(e) {
					if(e.index>=0)
						params.items[e.index].fn_click();
				});
		break;
		case 'androidTablet':
			
		break;
		default://Android
			
	}

	return segmentedControl;
};
Talk4UForm.prototype.createUserInfoView = function(params) {
	var userInfoView = Ti.UI.createView({
		width: Ti.UI.FILL,
		height: Ti.UI.SIZE,
	});
	var leftView = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
	});
	var nameLb = Ti.UI.createLabel({
		height: 100,
		text: params.itemName+'\n'+params.userJid,
		textAlign: 'left'
	});

	var rightView = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
	});
	var imageWrapper = Ti.UI.createView({
		width: 100,
		height: 100
	});
	var userImg = Ti.UI.createImageView({
		itemId: params.itemId,
		itemType: (params.itemType)?params.itemType:'user',
		photoId: (params.photoId)?params.photoId:'',
		type: 'photo',
		width: 100,
		height: 100,
		image: (params.userImage)?params.userImage:'images/talk4u/fakePNG/user.png'
	});
	userInfoView.userImg = userImg;
	var favouriteImg = Ti.UI.createImageView({
		bottom: 0,
		right: 0,
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		image: (params.favouriteImage)?'':params.favouriteImage,
		visible: false,
	});
	userInfoView.favouriteImg = favouriteImg;
	switch(glb.device) {
		case 'iphone':
			userInfoView.top = 0;
			
			leftView.left = 10;
			leftView.top = 10;
			leftView.right = 120;
			leftView.bottom = 10;
			
			rightView.top = 10;
			rightView.right = 10;
			rightView.bottom = 10;
		break;
		case 'ipad':
			userInfoView.top = 30;
			
			leftView.left = 45;
			leftView.top = 30;
			leftView.right = 180;
			leftView.bottom = 30;
			
			rightView.top = 30;
			rightView.right = 30;
			rightView.bottom = 30;
		break;
		case 'androidTablet':
				
		break;
		default://Android
	}
	leftView.add(nameLb);
	imageWrapper.add(userImg);
	imageWrapper.add(favouriteImg);
	rightView.add(imageWrapper);
	userInfoView.add(leftView);
	userInfoView.add(rightView);
	
	return userInfoView;
};
Talk4UForm.prototype.createChatTableViewRow = function(params) {
	
	var chatRow = Ti.UI.createTableViewRow({
			width: Ti.UI.SIZE,
			height: Ti.UI.SIZE,
			backgroundColor: '#E0E0F8'
	});
	var leftView = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,

	});
	

	var nameLb = Ti.UI.createLabel({
		text: params.propietario + ": ",
		left:3,
		font: {
			fontWeight: 'bold'
		},
		color: '#336e95',
		top: 0
	});
	var chatLb = Ti.UI.createLabel({
		text: params.itemName,
		textAlign: 'left',
		top:25,
	});

	var rightView = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: 'vertical'
	});
	var imageWrapper = Ti.UI.createView({
		width: 50,
		height: 50,

	});
	var userImg = Ti.UI.createImageView({
		itemId: params.itemId,
		itemType: 'photo',
		photoId: params.photoId,
		type: 'photo',
		width: 50,
		height: 50,
		image: params.userImage
	});
	chatRow.userImg = userImg;

	var infoView = Ti.UI.createView({
		width: 50,
		height: Ti.UI.SIZE,
		layout: 'horizontal'
	});
	
	var dateLabel = Ti.UI.createLabel({
		text: params.date,
		font: {
			fontWeight: 'bold',
			fontSize:8
		},
		color: '#336e95',
	});
	
	var stateImage = Ti.UI.createImageView({
		image: 'images/talk4u/msgStatus/received.png'
	})

	switch(glb.device) {
		case 'iphone':
			chatRow.top = 0;
			
			leftView.left = 10;
			leftView.top = 10;
			leftView.right = 60;
			leftView.bottom = 10;
			
			rightView.top = 10;
			rightView.right = 10;
		break;
		case 'ipad':
			chatRow.top = 30;
			
			leftView.left = 45;
			leftView.top = 30;
			leftView.right = 180;
			leftView.bottom = 30;
			
			rightView.top = 30;
			rightView.right = 30;
			rightView.bottom = 30;
		break;
		case 'androidTablet':
				
		break;
		default://Android
	}
	
	imageWrapper.add(userImg);
	
	infoView.add(stateImage);
	infoView.add(dateLabel);
	
	rightView.add(imageWrapper);
	rightView.add(infoView);
	
	leftView.add(nameLb);
	leftView.add(chatLb);
	
	chatRow.add(leftView);
	chatRow.add(rightView);
	
	return chatRow;
};
Talk4UForm.prototype.createChatTableViewRowOwner = function(params) {
	var chatRow = Ti.UI.createTableViewRow({
			width: Ti.UI.SIZE,
			height: Ti.UI.SIZE,
			//backgroundColor: '#F3E2A9'
			backgroundColor: '#EFEFFB'
	});
	var leftView = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: 'vertical'
	});
	var nameLb = Ti.UI.createLabel({
		text: params.propietario + ":",
		left:3,
		font: {
			fontWeight: 'bold'
		},
		color: '#336e95',
		top: 0
	});
	var chatLb = Ti.UI.createLabel({
		text: params.itemName,
		//textAlign: 'left',
		top:25,
	});

	var rightView = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		textAlign: 'left'
	});
	var imageWrapper = Ti.UI.createView({
		width: 50,
		height: 50,

	});
	var userImg = Ti.UI.createImageView({
		itemId: params.itemId,
		itemType: 'photo',
		photoId: params.photoId,
		type: 'photo',
		width: 50,
		height: 50,
		image: params.userImage
	});
	chatRow.userImg = userImg;
	
	var infoView = Ti.UI.createView({
		width: 50,
		height: Ti.UI.SIZE,
		layout: 'horizontal'
	});
	
	var dateLabel = Ti.UI.createLabel({
		text: params.date,
		font: {
			fontWeight: 'bold',
			fontSize:8
		},
		color: '#336e95',
		textAlign: 'center'
	});

	switch(glb.device) {
		case 'iphone':
			chatRow.top = 0;
			
			leftView.top = 10;
			leftView.right = 10;
			leftView.left = 10;

			
			rightView.top = 10;
			rightView.right = 0;
			rightView.bottom = 10;
			rightView.left= 80;

		break;
		case 'ipad':
			chatRow.top = 30;
			
			leftView.left = 45;
			leftView.top = 30;
			leftView.right = 180;
			leftView.bottom = 30;
			
			rightView.top = 30;
			rightView.right = 30;
			rightView.bottom = 30;
		break;
		case 'androidTablet':
				
		break;
		default://Android
	}
	
	imageWrapper.add(userImg);
	
	rightView.add(chatLb);
	rightView.add(nameLb);
	
	infoView.add(dateLabel);
	
	leftView.add(imageWrapper);
	leftView.add(infoView);
	
	chatRow.add(leftView);
	chatRow.add(rightView);
	
	return chatRow;
};
Talk4UForm.prototype.createChatsTableViewRow = function(params) {
	var row = Ti.UI.createTableViewRow({
		parentId: (params.parentId) ? params.parentId : null,
		itemId: (params.itemId) ? params.itemId : null,
		itemTypeId: (params.itemTypeId) ? params.itemTypeId : null,
		itemType: (params.itemType) ? params.itemType : 'user',
		// completeName: (params.completeName) ? params.completeName : null,
		favourite: (params.favourite) ? true : false,
		location: (params.location) ? params.location : {'long': null, 'lat': null},
		hasChild: (params.hasChild) ? true : false,
		hasDetail: (params.hasDetail) ? true : false,
		// width: params.win.toImage().width,
		width: 320,
		height: 50,
		className: 'folderUserRow',
		xmppjid: (params.xmppjid) ? params.xmppjid : '',
		participant: (params.participants)? params.participants: ''
		
	});
	row.addEventListener('downloaded', function(e) {
		thumbImage.image=e.localFilePath;
	});
	var rowContentView = Ti.UI.createView({
		layout: 'horizontal',
		horizontalWrap: false,
		left: 0,
		right: 5,
		height: Ti.UI.SIZE,
	});
	var rowContentViewLeft= Ti.UI.createView({
		layout: 'vertical',
		horizontalWrap: false,
		left: 30,
		right: 10,
		height: Ti.UI.SIZE,
	});
	
	var leftView = Ti.UI.createView({
		layout: 'horizontal',
		horizontalWrap: false,
		left: 0,
		width:Ti.UI.SIZE,
		height:Ti.UI.SIZE,
	});
	var editImage = Ti.UI.createImageView({
		image: 'images/unchecked.png',
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		left: 5,
	});
	row.editImage=editImage;
	var thumbView = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		left: 10
	});
	var thumbImage_path=(params.thumbImage)?params.thumbImage:'';
	if(params.itemType==='folder') {
		thumbImage_path='images/talk4u/icoListas/carpeta.png';
		row.hasChild = true;
	} else if(params.itemType==='user') {
		thumbImage_path='images/talk4u/icoListas/user.png';
		//row.hasChild = true;
	} else if(params.itemType==='group') {
		thumbImage_path='images/talk4u/icoListas/group.png';
		//row.hasChild = true;
	}
	var thumbImage = Ti.UI.createImageView({
		image: thumbImage_path,
		width: 29,
		height: 29,
		left: 0
	});
	row.thumbImage=thumbImage;
	if(row.favourite) {
		var favouriteImage = Ti.UI.createImageView({
			image: 'images/talk4u/icoListas/favourite_mask.png',
			width: Ti.UI.SIZE,
			height: Ti.UI.SIZE,
			left: 0
		});
	}

	var topTextView = Ti.UI.createView({
		left: (params.leftImage)?50:0,
		width: Ti.UI.FILL,
		height: Ti.UI.SIZE,
	});
	var topLeftTextLabel = Ti.UI.createLabel({
		text: (params.topLeftText)?params.topLeftText:'',
		textAlign: 'left',
		color: (params.color)?params.color:'#000',
		font: {fontSize: 16, fontWeight: 'bold'},
		width: Ti.UI.SIZE,
		height: 24,
		left: 0
	});
	var topRightTextLabel = Ti.UI.createLabel({
		text: (params.topRightText)?params.topRightText:'',
		textAlign: 'right',
		color: (params.color)?params.color:'#000',
		font: {fontSize: 12, fontWeight: 'normal'},
		width: Ti.UI.SIZE,
		height: 24,
		right: 0
	});
	var bottomTextView = Ti.UI.createView({
		left: (params.leftImage)?50:0,
		width: Ti.UI.FILL,
		height: Ti.UI.FILL,
	});
	var bottomLeftTextLabel = Ti.UI.createLabel({
		text: (params.bottomLeftText)?params.bottomLeftText:'',
		textAlign: 'left',
		color: (params.color)?params.color:'#385487',
		font: {fontSize: 16},
		width: Ti.UI.SIZE,
		height: 24,
		left: 0
	});
	var bottomRightTextLabel = Ti.UI.createLabel({
		text: (params.bottomRightText)?params.bottomRightText:'',
		textAlign: 'center',
		color: (params.color)?params.color:'white',
		font: {fontSize: 14, fontWeight: 'bold'},
		backgroundColor: (params.bottomRightText===null)?null:"gray",
		width: 20,
		height: 20,
		right: 0
	});
	switch(glb.device) {
		case 'iphone':
		
		break;
		case 'ipad':
			
		break;
		case 'androidTablet':
			
		break;
		default://Android
	}
	leftView.add(editImage);
	thumbView.add(thumbImage);
	leftView.add(thumbView);
	topTextView.add(topLeftTextLabel);
	topTextView.add(topRightTextLabel);
	bottomTextView.add(bottomLeftTextLabel);
	bottomTextView.add(bottomRightTextLabel);
	
	rowContentView.add(leftView);
	
	rowContentViewLeft.add(topTextView);
	rowContentViewLeft.add(bottomTextView);	
	row.add(rowContentView);
	row.add(rowContentViewLeft);
	


	
	return row;
};
Talk4UForm.prototype.createActivityIndicator = function(message) {
	var view = Ti.UI.createView({
		backgroundColor: '#ccc',
		opacity: 0.75,
		width: Ti.UI.FILL,
		height: Ti.UI.FILL,
		zIndex: 1000,
		visible: false
	});
	var actInd = Ti.UI.createActivityIndicator({
		message: (message || message==='') ? message : L('loading'),
		color: '#000',
	});
	switch(glb.device) {
		case 'iphone':
			actInd.style = Ti.UI.iPhone.ActivityIndicatorStyle.DARK;
		break;
		case 'ipad':
			actInd.style = Ti.UI.iPhone.ActivityIndicatorStyle.DARK;
		break;
		case 'androidTablet':
				
		break;
		default://Android
	}
	view.add(actInd);
	actInd.show();
	
	return view;
};
Talk4UForm.prototype.createProgressBar = function(message) {
	var view = Ti.UI.createView({
		backgroundColor: '#ccc',
		opacity: 0.75,
		width: Ti.UI.FILL,
		height: Ti.UI.FILL,
		zIndex: 1000,
		visible: false
	});
	var pb=Titanium.UI.createProgressBar({
	    width:250,
	    height: Ti.UI.SIZE,
	    min:0,
	    max:1,
	    value:0,
	    color:'#fff',
	});
	view.progress=pb;
	switch(glb.device) {
		case 'iphone':
			pb.style = Titanium.UI.iPhone.ProgressBarStyle.PLAIN;
		break;
		case 'ipad':
			pb.style = Titanium.UI.iPhone.ProgressBarStyle.PLAIN;
		break;
		case 'androidTablet':
				
		break;
		default://Android
	}
	view.add(pb);
	pb.show();
	
	return view;
};
	
module.exports=Talk4UForm;


