/**
 * traceObject()
 * trace all key and value of a js object
 * @param Object object
 * @param String name
 */
function traceObject(object,name) {
	// name = (name == undefined) ? '' : name;
	// trace('traceObject '+name);
	// for (var i in object){
		// if(typeof(object[i])==='object'){
			// trace(i+' :: '+object[i]);
			// traceObject(object[i]);
		// } else {
			// trace(i+' :: '+object[i]);
		// }
	// }
};
/**
 * Trace logs of the app actions
 * @param String txt
 * @param String type
 */
function trace(msg,type) {
	var type = (type!=undefined) ? type = type : type = 'warn';
	switch(type){
		case 'info':
			Ti.API.info('***-'+msg+'-***');
			break;
			
		case 'debug':
			Ti.API.debug('***-'+msg+'-***');
			break;
		
		case 'warn':
			Ti.API.warn('***-'+msg+'-***');
			break;
		
		case 'error':
			Ti.API.error('***-'+msg+'-***');
			break;
	}
	//Ti.API.info("Available memory: " + Ti.Platform.availableMemory);
};
// returns current timestamp {int}
function timestamp(){
    return Math.round(new Date().getTime() / 1000);
};
function _D(ThePixels){
  return (ThePixels / (Titanium.Platform.displayCaps.getDpi() / 160));
};
function _P(TheDPUnits){
  return (TheDPUnits * (Titanium.Platform.displayCaps.getDpi() / 160));
};
/**
 * send_email()
 * Open the email dialog with the given params
 * @param String subject
 * @param EmailAddress toRecipients
 * @param String messageBody
 */
function send_email(subject, toRecipients, messageBody, win){
	var dEmailMessage = Ti.UI.createEmailDialog();
	dEmailMessage.subject = subject;
	dEmailMessage.toRecipients = [toRecipients];
	dEmailMessage.messageBody = messageBody;
	// dEmailMessage.html
	 
	dEmailMessage.addEventListener('complete',function(e)
	{
	    if (e.result == dEmailMessage.SENT){
	        //trace('message sent');
	        if (win)
		    	win.close();
	    }
	});
	dEmailMessage.open();
};
// parsea la fecha recibido desde el ws y devuelve un new Date()
function formatDate(d){
	if(d){
		// 2012-11-07T16:53:46.05
		var year = d.substr(0, 4);
		var month = d.substr(5,2);
		var day = d.substr(8,2);
		var hours = d.substr(11,2);
		var minutes = d.substr(14,2);
		var seconds = d.substr(17,2);
		var milliseconds = d.substr(20,2);
		var date = new Date(year, month-1, day, hours, minutes, seconds, milliseconds);
		return date;
	}else {
		return '';
	}
	
};
// parsea la fecha del ws y returns la fecha completa DD/MM/YYYY HH:MM:SS
function dateComplete(d){
	if(d){
		var date = formatDate(d);
		var day = (date.getDate()<10)?'0'+date.getDate():date.getDate();
		var month = ((date.getMonth()+1)<10)?'0'+(date.getMonth()+1):(date.getMonth()+1);
		
		var hours = (date.getHours()+1<10)?'0'+(date.getHours()+1):date.getHours()+1;
		var minutes = (date.getMinutes()<10)?'0'+(date.getMinutes()):(date.getMinutes());
		
		return day + '/' + month + '/' + date.getFullYear() + ' ' +hours + ':' +minutes;//+ ':' + date.getSeconds();
	}else {
		return '';
	}
};

// parsea la fecha del ws y returns la fecha sin las horas DD/MM/YYYY
function dateWithoutHours(d){
	if(d){
		var date = formatDate(d);
		var day = (date.getDate()<10)?'0'+date.getDate():date.getDate();
		var month = ((date.getMonth()+1)<10)?'0'+(date.getMonth()+1):(date.getMonth()+1);
		return day + '/' + month + '/' + date.getFullYear();
	}else {
		return '';
	}
};
function dateCompleteFromObjectDate(d){
	if(d){
		var dd = d.getDate();
		var mm = d.getMonth()+1; //January is 0!
		var yyyy = d.getFullYear();
	    var curHour =  d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
	    var curMinute = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
	    var curSeconds = d.getSeconds() < 10 ? "0" + d.getSeconds() : d.getSeconds();
	
		return dd + '/' + mm + '/' + yyyy + ' ' + curHour + ':' + curMinute + ':' + curSeconds;
	}else {
		return '';
	}
}

function readableFileSize(fileSize)
{
	if (fileSize > 1024*1024*1024)
		fileSize = Math.round(fileSize / (1024*1024*1024), 2) + ' GB';
	else if (fileSize > 1024*1024)
		fileSize = Math.round(fileSize / (1024*1024), 2) + ' MB';
	else if (fileSize > 1024)
		fileSize = Math.round(fileSize / 1024, 2) + ' KB';
	else 
		fileSize = fileSize + ' bytes';
	return fileSize;
}

function isArray(obj) {
	return obj && !!obj.push;
}

function getUserFromJid(s){
	return s.substring(0,s.indexOf('@'));
}
